<?php
defined('BASEPATH') OR exit('No direct script access allowed');

  function sesionLogueado($in)
  {
    $data['datos'] = $in;
    $CI= & get_instance();
    if(is_null($CI->session->userdata('_rendiapps')['usuario']))
    {
      if ($in == "POST") {
        echo $CI->load->view('Login/loginmodal',$data,true);
        exit;
      }else {
        redirect(base_url());
      }
    }
    else
    {
      return true;
    }
  }


if (!function_exists('sesionNoLogueado'))
{
  function sesionNoLogueado()
  {
    $CI= & get_instance();
    if(is_null($CI->session->userdata('_rendiapps')['usuario']))
    {
      return true;
    }
    else
    {
      return redirect('Home/Apps');
    }
  }
}
