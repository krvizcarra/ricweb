<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth
{

	protected $ci;

	//creamos una instancia del super objeto de codeigniter
	//en el constructor para poder tenerlo disponible las veces
	//que necesitemos sin repetir la misma línea
	public function __construct()
	{

		$this->ci =& get_instance();


	}

	//creamos una token para nuestros formularios
	public function token()
    {

        $token = md5(uniqid(rand(),true));
        $this->ci->session->set_userdata('token',$token);
        return $token;

    }

	//función para comprobar si el usuario está logueado
	public function is_logged()
	{

		return $this->ci->session->userdata('_rendiapps');

	}

	//función para comprobar si el usuario tiene permiso de acceder al sistema
	public function it_has_permission($sistema)
	{
		$id = $this->ci->session->userdata('id');

        $query = $this->ci->db->get_where('usuarios_sistemas',array('usuario_id' => $id, 'sistema_id' => $sistema));

        //si el usuario y el sistema coincide
        if($query->num_rows() == 1){
            return TRUE;
        }else{
            return FALSE;
        }
		//return $this->rendiapps_model->get_permission($sistema);
		//session->userdata('usuario') !== FALSE ? TRUE : FALSE;

	}

	//creamos un array con los campos del formulario




	//función para validar los formularios
	public function validate()
	{
		//si es el formulario de login validamos
		$this->ci->form_validation->set_rules('correo', 'correo', 'required|trim|min_length[3]|max_length[100]|xss_clean');
		$this->ci->form_validation->set_rules('contrasena', 'contraseña', 'required|trim|min_length[5]|max_length[100]|xss_clean');

    $this->ci->form_validation->set_message('required', 'El campo %s es requerido');
		//$this->ci->form_validation->set_message('trim', 'El %s no acepta espacios');
		$this->ci->form_validation->set_message('valid_email', 'El campo %s no es correcto');
    $this->ci->form_validation->set_message('min_length', 'El campo %s debe tener al menos %s carácteres');
    $this->ci->form_validation->set_message('max_length', 'El campo %s debe tener al menos %s carácteres');

		if($this->ci->form_validation->run() == FALSE){
			return FALSE;
		}
		return TRUE;
	}

	//función para registrar a los usuarios encriptando
	//los passwords con bcrypt
	public function register($nombre,$usuario,$email,$telefono,$password,$empresa){

		$query = $this->ci->db->get_where('users', array('usuario' => $usuario));

		if($query->num_rows() == 1){
			return TRUE;
		}else{
		  $hash = $this->ci->bcrypt->hash_password($password);
            //comprobamos si el password se ha encriptado
            if ($this->ci->bcrypt->check_password($password, $hash))
            {
            	$this->ci->db->trans_begin();

            	$data = array('nombre' => $nombre,'usuario' => $usuario, 'email' => $email,'telefono'=>$telefono, 'password' => $hash,'empresa'=>$empresa);
				$this->ci->db->insert('users',$data);

				$data = array('usuario_id' => $this->ci->db->insert_id(),'sistema_id' => 1, 'permisos' => 0);
				$data2 = array('usuario_id' => $this->ci->db->insert_id(),'sistema_id' => 2, 'permisos' => 0);
				$this->ci->db->insert('usuarios_sistemas',$data);
				$this->ci->db->insert('usuarios_sistemas',$data2);


				//comprobamos si se han llevado a cabo correctamente todas
			    //las consultas
			    if ($this->ci->db->trans_status() === FALSE)
			    {
			      //si ha habido algún error lo debemos mostrar aquí
			      $this->ci->db->trans_rollback();
			    }else{
			      //en otro caso todo ha ido bien
			      $this->ci->db->trans_commit();
			    }

			}

		}

	}

	//función para loguear a los usuarios
	public function login_user($correo,$contrasena)
	{
		// $this->ci->db->where('correo',$correo);
		$this->ci->db->where("(correo='$correo' OR usuario='$correo' ) ", NULL, FALSE);

    $query = $this->ci->db->get('usuarios');

    //si el nombre de usuario coincide y sólo existe uno procedemos
    if($query->num_rows() == 1)
    {
    	$user = $query->row();
      //en pass guardamos el hash del usuario que tenemos en la base
      //de datos para comprobarlo con el método check_password de Bcrypt
      $pass = $user->password;
    	//esta es la forma de comprobar si el password del
      //formulario coincide con el codificado de la base de datos
      if($this->ci->bcrypt->check_password($contrasena, $pass))
		  {
				return TRUE;
			}else{
				//$this->ci->session->set_flashdata('error','Contraseña incorrecta');
				return FALSE;
			}
		}else {
			//$this->ci->session->set_flashdata('error','Correo no registrado');
			return FALSE;
		}

	}

	//función para loguear a los usuarios api
	public function login_api($correo,$contrasena)
	{
		$this->ci->db->where("(correo='$correo' OR usuario='$correo' ) ", NULL, FALSE);
    $query = $this->ci->db->get('usuarios');
    if($query->num_rows() == 1)
    {
    	$user = $query->row();
      $pass = $user->password;
      if($this->ci->bcrypt->check_password($contrasena, $pass))
		  {
				return $user;
			}else{
				return null;
			}
		}else {
			return null;
		}
	}

	//Validar contraseña
	public function valid_password($id,$contrasena)	{

		$this->ci->db->where('id',$id);


    $query = $this->ci->db->get('usuarios');

    //si el nombre de usuario coincide y sólo existe uno procedemos
    if($query->num_rows() == 1)
    {
    	$user = $query->row();
      //en pass guardamos el hash del usuario que tenemos en la base
      //de datos para comprobarlo con el método check_password de Bcrypt
      $pass = $user->password;
    	//esta es la forma de comprobar si el password del
      //formulario coincide con el codificado de la base de datos
      if($this->ci->bcrypt->check_password($contrasena, $pass))
		  {
				return TRUE;
			}else{
				//$this->ci->session->set_flashdata('error','Contraseña incorrecta');
				return FALSE;
			}
		}else {
			//$this->ci->session->set_flashdata('error','Correo no registrado');
			return FALSE;
		}

	}


	//función para crear sesiones
	public function crear_sesiones($correo,$password)
	{
		// $this->ci->db->where('correo',$correo);
		$query = $this->ci->db->select('usuarios.*')
		->from('usuarios')		
		->where("(usuarios.correo='$correo' OR usuarios.usuario='$correo' ) ", NULL, FALSE)
		->get();
		// $this->ci->db->where("(correo='$correo' OR usuario='$correo' ) ", NULL, FALSE);
    // $query = $this->ci->db->get('usuarios');
  	$user = $query->row();

		$data = array(			
			'nombre' => $user->nombre,
			'avatar'=>$user->imagen);

		$this->ci->session->set_userdata('_rendiapps',$data);

	}


	//función para crear sesiones
	public function cambiar_sesiones($usuario,$empresa,$empresaid,$rfc)
	{
		// $this->ci->db->where('correo',$correo);
		$query = $this->ci->db->select('usuarios.*,empresas.razonsocial,empresas.servidor,empresas.rfc')
		->from('usuarios')
		->join('empresas','usuarios.empresa = empresas.id','inner')
		->where("usuarios.id", $usuario)
		->get();
		// $this->ci->db->where("(correo='$correo' OR usuario='$correo' ) ", NULL, FALSE);
    // $query = $this->ci->db->get('usuarios');
  	$user = $query->row();

		$data = array(
			'servidor' => $user->servidor,
			'razonsocial' => $empresa,
			'usuario' => $user->usuario,
			'id' => $user->id,
			'rfc' => $rfc,
			'empresa' => $empresaid,
			'correo' => $user->correo,
			'telefono' => $user->telefono,
			'nombre' => $user->nombre,
			'avatar'=>$user->imagen);

		$this->ci->session->set_userdata('_rendiapps',$data);

	}

	//función para editar sesiones
	public function editar_sesiones($id)
	{
		try {
			// $this->ci->db->where('correo',$correo);
			$query = $this->ci->db->select('usuarios.*,empresas.razonsocial,empresas.servidor,empresas.rfc')
			->from('usuarios')
			->join('empresas','usuarios.empresa = empresas.id','inner')
			->where('usuarios.id',$id)
			->get();
			// $this->ci->db->where("(correo='$correo' OR usuario='$correo' ) ", NULL, FALSE);
	    // $query = $this->ci->db->get('usuarios');
	  	$user = $query->row();

			$data = array(
				'servidor' => $user->servidor,
				'razonsocial' => $user->razonsocial,
				'usuario' => $user->usuario,
				'id' => $user->id,
				'rfc' => $user->rfc,
				'empresa' => $user->empresa,
				'correo' => $user->correo,
				'telefono' => $user->telefono,
				'nombre' => $user->nombre,
				'avatar'=>$user->imagen);

			$this->ci->session->set_userdata('_rendiapps',$data);
			return 'OK';
		} catch (\Exception $e) {
			return 'Error';
		}




	}

	//función para enviar emails al registrarse
	public function send_mail($nombre,$usuario,$email,$password)
	{

		$config['useragent'] = "Sistema Rendiapps";
		$config['mailpath']	 = "/usr/sbin/sendmail";	// Sendmail path
		$config['protocol']	 = "smtp";	// mail/sendmail/smtp
		$config['smtp_host'] = "ssl://smtp.gmail.com";		// SMTP Server.  Example: mail.earthlink.net
		$config['smtp_user'] = "rendiapps@gmail.com";		// SMTP Username
		$config['smtp_pass'] = "SistemasEsco";		// SMTP Password
		$config['smtp_port'] = "465";		// SMTP Port
		$config['mailtype']	= "html";	// text/html  Defines email formatting
		$config['charset'] = "utf-8";	// Default char set: iso-8859-1 or us-ascii

		$this->ci->load->library('email',$config);
		$this->ci->email->set_newline("\r\n");
		$this->ci->email->from('Sistema Rendiapps','Sistema Rendiapps');
        $this->ci->email->to($email);
        $this->ci->email->subject('Se ha registrado correctamente.');
        $this->ci->email->message($nombre.' estos son sus datos de acceso:<br /><br />Usuario: '.$usuario. '<br />Password: '.$password);
        $this->ci->email->send();

	}

	//función para enviar emails al registrarse
	public function send_mail_contra($email,$url,$password)
	{

		$config['useragent'] = "Sistema Rendiapps";
		$config['mailpath']	 = "/usr/sbin/sendmail";	// Sendmail path
		$config['protocol']	 = "smtp";	// mail/sendmail/smtp
		$config['smtp_host'] = "ssl://smtp.gmail.com";		// SMTP Server.  Example: mail.earthlink.net
		$config['smtp_user'] = "rendiapps@gmail.com";		// SMTP Username
		$config['smtp_pass'] = "SistemasEsco";		// SMTP Password
		$config['smtp_port'] = "465";		// SMTP Port
		$config['mailtype']	= "html";	// text/html  Defines email formatting
		$config['charset'] = "utf-8";	// Default char set: iso-8859-1 or us-ascii
		$data['url'] = $url;
		$data['contra'] = $password;
		$this->ci->load->library('email',$config);
		$this->ci->email->set_newline("\r\n");
		$this->ci->email->from('Sistema Rendiapps','Sistema Rendiapps');
				$this->ci->email->to($email);
				$this->ci->email->subject('Cambio de contraseña.');
				$this->ci->email->message($this->ci->load->view('template_cambiocontra',$data,TRUE));
				$this->ci->email->send();

	}
	//función para cerrar sesión
	public function logout()
	{

		$this->ci->session->unset_userdata(array('usuario', 'password','id','empresa'));
		$this->ci->session->sess_destroy();
		//$this->ci->session->sess_create();
		$this->ci->session->set_flashdata('cerrada','La sessión se ha cerrado correctamente.');
		redirect(base_url('/Login'));

	}

	public function validarUsuario($usuario)
	{
		$this->ci->db->where("(correo='$usuario->usuario' OR usuario='$usuario->usuario' ) ", NULL, FALSE);
    $query = $this->ci->db->get('usuarios');

    if($query->num_rows() == 1) {
    	$user = $query->row();
      $pass = $user->password;
			return $this->ci->bcrypt->check_password($usuario->password, $pass)?$user->id:NULL;
		}else {
			return NULL;
		}
	}

	public function iniciarSesion($usuario){
		$query = sprintf("call pa_loginUser ('%s','%s')",$usuario->id,$usuario->token);
		$response = $this->ci->db->query($query);
		$this->ci->db->close();
		return $response->row();
	}

}
/*
 * end libraries/auth.php
 */
