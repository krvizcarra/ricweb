<?php
class Correo_model extends CI_Model {

  public function asignarCorreo($template,$correo,$nombre,$asunto)
  {
    $config = new stdClass();
    $config->servidor = "smtp.gmail.com";
    $config->correo = "ricdiagnostica@gmail.com";
    $config->alias = "Pagina Web";
    $config->contra = "diagnostica2019";
    $config->puerto = "587";
    $config->zona = "America/Mazatlan";
    $config->debug = "html";
    $config->cifrado = "tls";
    $config->asunto = sprintf("PAGINA RICDIAGNOSTICA: %s",$asunto);
    $config->cuerpo = $template;
    $config->destino = "krvizcarra@gmail.com";
    $config->destinoNombre = "Kristian Omar Vizcarra";
  return $config;
  }

  public function enviarCorreo($archivo,$archivoName,$template,$correo,$nombre,$asunto)
  {
    $destinos = explode(',', $correo);
    $config =$this->asignarCorreo($template,$correo,$nombre,$asunto);
    date_default_timezone_set($config->zona);
    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = $config->debug;
    $mail->Host = $config->servidor;
    $mail->Port = $config->puerto;
    $mail->SMTPSecure = $config->cifrado;
    $mail->SMTPAuth = true;
    $mail->Username = $config->correo;
    $mail->Password = $config->contra;
    // if (!is_null($archivo)) {
    //   if ($reenvio) {
    //     $base = base64_decode($archivo);
    //     $mail->AddStringAttachment($base, $archivoName, "base64",$archivoType);
    //   }else {
    //     $mail->AddAttachment($archivo,$archivoName);
    //   }
    // }
    $mail->setFrom($config->correo, $config->alias);

    // foreach($destinos as $email)
    //   {
    //      $mail->AddCC($email, $email);
    //   }
    $mail->addAddress($config->destino, $config->destinoNombre);
    $mail->Subject = $config->asunto;
    $mail->msgHTML($config->cuerpo);
  return $mail;
  }


}
