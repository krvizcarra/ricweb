<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		// $this->load->model('Login_model','mod_login');
		// $this->load->model('Producto_model','producto');
		$this->load->model('Correo_model','correo');

	}
	public function index()	{
		$this->load->view('header');
		$this->load->view('inicio');
		// $this->load->view('footer');
	}

	public function Nosotros()	{
		$this->load->view('header');
		$this->load->view('nosotros');
		// $this->load->view('footer');
	}

	public function Contacto()	{
		$this->load->view('header');
		$this->load->view('contacto');
		// $this->load->view('footer');
	}

	public function Categorias()
	{
		$data['categorias'] = $this->producto->obtenerCategorias();
		$this->load->view('header');
		$this->load->view('categorias',$data);
		// $this->load->view('footer');
	}


	public function Productos($id = 1)
	{
		$data["paginas"] = ceil($this->producto->contarProductos($id) / 6);
		$data['categorias'] = $this->producto->obtenerCategorias();
		if ($id == -1) {
			$data['productos'] = $this->producto->obtenerProductos();
		}else {
			$data['productos'] = $this->producto->obtenerProductosPorCategoria($id);
		}
		$data['cat'] = $id;
		$this->load->view('header');
		$this->load->view('productos',$data);
		// $this->load->view('footer');
	}

	public function ProductosPorCategoria()
	{
		$response["error"] = true;
		if (isset($_POST['categoria']) && is_numeric($_POST['categoria'])) {
			if ($_POST['categoria'] == -1) {
				$response['msg'] = $this->producto->obtenerProductos();
			}else {
				$response['msg'] = $this->producto->obtenerProductosPorCategoria($_POST['categoria']);
			}
			$response["paginas"] = ceil($this->producto->contarProductos($_POST['categoria']) / 6);
			$response["error"] = false;
		}else {
			$response['msg'] = "Error al consultar productos.";
		}
		echo json_encode($response);
	}

	public function PaginadoProductos()
	{
		$response["error"] = true;
		if (isset($_POST['categoria']) && is_numeric($_POST['categoria']) &&
				isset($_POST['pagina']) && is_numeric($_POST['pagina'])
			) {
				$inicial = ( ($_POST['pagina'] * 6) );
				$final = $inicial + 5;
			if ($_POST['categoria'] == -1) {
				$response['msg'] = $this->producto->obtenerProductos($inicial,$final);
			}else {
				$response['msg'] = $this->producto->obtenerProductosPorCategoria($_POST['categoria'],$inicial,$final);
			}
			$response["error"] = false;
		}else {
			$response['msg'] = "Error al consultar productos.";
		}
		echo json_encode($response);
	}

	public function DetalleProducto()
	{
		$folio = isset($_POST['folio']) ? $_POST['folio'] : 0;
		$data['producto'] = (object) $this->producto->obtenerProductoId($folio);
		echo $this->load->view("productodetalle",$data,true);
	}

	public function CorreoContacto()
	{
		header('Access-Control-Allow-Origin: *');

		if ( isset($_GET['nombre']) &&
		 		 isset($_GET['correo']) &&
				 isset($_GET['asunto']) &&
				 isset($_GET['mensaje'])
			 ){

				 if ($this->isValidEmail($_GET['correo'])) {
					 require 'application/libraries/PHPMailer/PHPMailerAutoload.php';

 					$template = sprintf("<h3>Nombre  : </h3><h3>%s</h3>
					 					 <h3>Correo  : </h3><h3>%s</h3>
					 					 <h3>Mensaje : </h3><h3>%s</h3>",$_GET['nombre'],$_GET['correo'],$_GET['mensaje']);
 					$mail = $this->correo->enviarCorreo(null,null,$template,$_GET['correo'],$_GET['nombre'],$_GET['asunto']);
 					if (!$mail->send()) {
 						$response["msg"] = $mail->ErrorInfo;
 					} else {
 						$response["error"] = false;
 						$response['msg'] = "Correo enviado correctamente.";
 					}

				}else {
					$response['msg'] = 'Correo no valido.';
				}
			}else {
				$response['msg'] = 'Faltan parametros';
			}
		echo json_encode($response);
	}

	function isValidEmail($email){
    return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
  }
}
