<style media="screen">
  .has-error > small{
    color: red;
  }

  .has-error > .form-control{
    border: solid 1px;
    border-color: red;
  }
</style>


    <div class="hero-wrap hero-bread" style="background-image: url('<? echo base_url() ?>plantilla/images/contactanos.jpg'); opacity:0.4;position: absolute;">

    </div>

    <div class="hero-wrap hero-bread">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-0 bread" style="color:#484444f0">Contacto</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section contact-section " style="padding-bottom: 6em;">
      <div class="container">

        <div class="row block-9">
          <div class="col-md-6 order-md-last d-flex">
            <form action="<?php echo base_url('/CorreoContacto') ?>" id="contactForm" class="bg-white contact-form">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Nombre" name="nombre">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Correo" name="correo">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Asunto" name="asunto">
              </div>
              <div class="form-group">
                <textarea cols="30" rows="7" class="form-control" placeholder="Mensaje" name="mensaje"></textarea>
              </div>
              <div class="form-group text-center">
                <!-- <input type="bottom" id="btnsumit" value="Enviar Mensaje" class="btn btn-primary py-3 px-5"> -->
                <!-- <a >Enviar Mensaje</a> -->
                <button type="button" id="btnsumit" value="Enviar Mensaje" class="btn btn-primary py-3 px-5">Enviar Mensaje</button>
              </div>
            </form>

          </div>

          <div class="col-md-6 d-flex">
          <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14367.438764045168!2d-108.9834505!3d25.8082026!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd84e2c3515c6af91!2sRic!5e0!3m2!1ses-419!2smx!4v1552494391489" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

        </div>
      </div>
    </section>

    <footer class="ftco-footer ftco-section" style="background: #f7f6f2 !important;">
      <div class="container">
      	<div class="row">
      		<div class="mouse">
						<a href="#" class="mouse-icon">
							<div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
						</a>
					</div>
      	</div>
        <div class="row mb-5" style="margin-bottom:0 !important">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2">Grupo Químico RIC</h2>
              <p class="text-justify">Somos una empresa Sinaloense, con más de 30 años de experiencia en soporte para diagnóstico clínico, con sede en Los Mochis, Sinaloa y sucursales en Ciudades clave del estado, Sonora, Baja California Norte y Sur.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft" style="margin-top: 1rem">
                <li class="ftco-animate"><a href="#" class="icon-social icon-social-twitter"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/RIC-diagnostica-605725263279611/" class="icon-social icon-social-facebook"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="https://www.instagram.com/ricdiagnostica/" class="icon-social icon-social-instagram"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Menu</h2>
              <ul class="list-unstyled">
              <li><a href="<?php echo base_url('') ?>" class="py-2 d-block">Inicio</a></li>
                <li><a href="<?php echo base_url('/Nosotros') ?>" class="py-2 d-block">Nosotros</a></li>
                <li><a href="<?php echo base_url('/Categorias') ?>" class="py-2 d-block">Productos</a></li>
                <li><a href="<?php echo base_url('/Contacto') ?>" class="py-2 d-block">Contacto</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Contáctanos</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text"></span>21 de Marzo N° 428 pte, Colonia Jiquilpan,
C.P.81220 Los Mochis, Sinaloa, México.</span></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text">(668) 812 6032</span></a></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">matriz@ricdiagnostica.com</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>

      </div>
    </footer>



  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="<? echo base_url() ?>plantilla/js/jquery.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/popper.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/bootstrap.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.easing.1.3.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.waypoints.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.stellar.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/owl.carousel.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.magnific-popup.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/aos.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.animateNumber.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/bootstrap-datepicker.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/scrollax.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/bootstrapvalidator.min.js"></script>
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="<? echo base_url() ?>plantilla/js/google-map.js"></script> -->
  <script src="<? echo base_url() ?>plantilla/js/main.js"></script>
  <script type="text/javascript" src="<?php echo base_url('plantilla/js/sweetalert2.min.js') ?>"></script>
  <link rel="stylesheet" href="<?php echo base_url('plantilla/css/sweetalert.min.css') ?>">
  <script>
    $('#contacto').addClass(" active");

    $(document).ready(function() {
        $('#contactForm').bootstrapValidator({

            fields: {
                nombre: {
                    validators: {
                          notEmpty: {
                              message: 'Campo requerido'
                          }
                    }
                },
                correo: {
                    validators: {
                          notEmpty: {
                              message: 'Campo requerido'
                          },
                          emailAddress: {
                              message: 'No tiene formato de correo electronico'
                          }
                    }
                },
                asunto: {
                    validators: {
                          notEmpty: {
                              message: 'Campo requerido'
                          }
                    }
                },
                mensaje: {
                    validators: {
                          notEmpty: {
                              message: 'Campo requerido'
                          }
                    }
                }
              }
            });
    });


    $("#btnsumit").click(function(e){
    e.preventDefault();
    let datos = $("#contactForm").serialize();
    let url = $("#contactForm").attr("action");


    $("#contactForm").bootstrapValidator('validate');
    var valid= $("#contactForm").data('bootstrapValidator').isValid();
    if (valid ) {
        $('#btnsumit').attr('disabled','disabled');
        $.get(url,datos,function( data){
            $('#btnsumit').removeAttr('disabled');
            if(!data.error){
                $('#contactForm').bootstrapValidator('resetForm', true);
                swal("Exito!"," "+data.msg,"success");
            }else{
                swal("Error!"," "+data.msg,"error");
            }
        },'json').fail(function(e,x,error){
            swal("Error!"," "+error,"error");
            $('#btnsumit').removeAttr('disabled');
        });       
        
    }
    //   swal({
    //     title: "¿Enviar Correo?",
    //     text: '',
    //     // content: wrapper,
    //     closeOnClickOutside: false,
    //     closeOnEsc: false,
    //     buttons: {
    //       cancel: "Cancelar",
    //       confirm:{
    //         text: "Enviar",
    //         closeModal: false,
    //       }

    //     },
    //   })
    //   .then(name => {
    //     // console.log(name);
    //     if (!name) throw null;
    //       return fetch(url+'?'+datos);
    //   })
    //   .then(results => {
    //     return results.json();
    //   })
    //   .then(json => {
    //     if (!json.error) {
    //       $('#contactForm').bootstrapValidator('resetForm', true);
    //       swal("Exito!"," "+json.msg,"success");
    //     }else {
    //       swal("Error!"," "+json.msg,"error");
    //     }

    //   })
    //   .catch(err => {
    //     if (err) {
    //       swal("Error!", " "+err, "error");
    //     } else {
    //       swal.stopLoading();
    //       swal.close();
    //     }
    //   });
    // }



  })

  </script>
  </body>
</html>
