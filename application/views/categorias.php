<style media="screen">
.center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 100%;
  height:200px;
}
</style>

    <div class="hero-wrap hero-bread" style="background-image: url('<? echo base_url() ?>plantilla/images/productos.jpeg'); opacity:0.4;position: absolute;">

    </div>

    <div class="hero-wrap hero-bread">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-0 bread" style="color:#484444f0">Productos</h1>
          </div>
        </div>
      </div>
    </div>

<section class="ftco-section" style="padding: 3em 0 3em 0 !important;">

  <div class="container">
    <div class="row justify-content-center mb-3 pb-3">
      <div class="col-md-12 heading-section text-center ftco-animate">
        <span class="subheading">Nuestros Productos</span>
        <h2 class="mb-4">Categorias</h2>
        <p></p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">

        <?php foreach ($categorias as $categoria): ?>
          <div class="col-md-6 col-lg-4 ftco-animate" style="height:325px;margin-top:10px;">
            <div class="product"  >
              <a href="#" class="img-prod"><img class="img-fluid center" src="<?php echo $categoria->img ?>" alt="img">
                <!-- <span class="status">30%</span> -->
                <div class="overlay"></div>
              </a>
              <div class="text py-3 pb-4 px-3 text-center">
                <h3 style="height:41px;"><a href="#"><?php echo $categoria->categoria ?></a></h3>
                <div class="d-flex">
                  <div class="pricing">
                    <p class="price"><span class="mr-2 price-dc"></span><span class="price-sale"></span></p>
                  </div>
                </div>
                <div class="bottom-area d-flex px-3">
                  <div class="m-auto d-flex">
                    <a href="<?php echo sprintf("%sProductos/%s",base_url(),$categoria->id) ?>" class="add-to-cart d-flex justify-content-center align-items-center text-center" title="Ver Categoria">
                      <span><i class="ion-ios-eye"></i></span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>

    </div>
  </div>
</section>

<footer class="ftco-footer ftco-section" style="background: #f7f6f2 !important;">
      <div class="container">
      	<div class="row">
      		<div class="mouse">
						<a href="#" class="mouse-icon">
							<div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
						</a>
					</div>
      	</div>
        <div class="row mb-5" style="margin-bottom:0 !important">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2">Grupo Químico RIC</h2>
              <p class="text-justify">Somos una empresa Sinaloense, con más de 30 años de experiencia en soporte para diagnóstico clínico, con sede en Los Mochis, Sinaloa y sucursales en Ciudades clave del estado, Sonora, Baja California Norte y Sur.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft" style="margin-top: 1rem">
                <li class="ftco-animate"><a href="#" class="icon-social icon-social-twitter"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/RIC-diagnostica-605725263279611/" class="icon-social icon-social-facebook"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="https://www.instagram.com/ricdiagnostica/" class="icon-social icon-social-instagram"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Menu</h2>
              <ul class="list-unstyled">
              <li><a href="<?php echo base_url('') ?>" class="py-2 d-block">Inicio</a></li>
                <li><a href="<?php echo base_url('/Nosotros') ?>" class="py-2 d-block">Nosotros</a></li>
                <li><a href="<?php echo base_url('/Categorias') ?>" class="py-2 d-block">Productos</a></li>
                <li><a href="<?php echo base_url('/Contacto') ?>" class="py-2 d-block">Contacto</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Contáctanos</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text"></span>21 de Marzo N° 428 pte, Colonia Jiquilpan,
C.P.81220 Los Mochis, Sinaloa, México.</span></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text">(668) 812 6032</span></a></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">matriz@ricdiagnostica.com</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>

      </div>
    </footer>



  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="<? echo base_url() ?>plantilla/js/jquery.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/popper.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/bootstrap.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.easing.1.3.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.waypoints.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.stellar.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/owl.carousel.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.magnific-popup.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/aos.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.animateNumber.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/bootstrap-datepicker.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/scrollax.min.js"></script>
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="<? echo base_url() ?>plantilla/js/google-map.js"></script> -->
  <script src="<? echo base_url() ?>plantilla/js/main.js"></script>
  <script>
    $('#productos').addClass(" active");
  </script>
  </body>
</html>
