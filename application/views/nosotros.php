    <div class="hero-wrap hero-bread" style="background-image: url('<? echo base_url() ?>plantilla/images/slide06.png'); opacity:0.4;position: absolute;">
      
    </div>

    <div class="hero-wrap hero-bread">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">          	
            <h1 class="mb-0 bread" style="color:#484444f0">¿Quiénes somos?</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section ftco-no-pb ftco-no-pt " style="padding-top: 3em !important;">
			<div class="container">
				<div class="row">
					<div class="col-md-5 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url(<? echo base_url() ?>plantilla/images/about02.jpg);">
						
					</div>
					<div class="col-md-7 wrap-about pb-md-5 ftco-animate">
	          <div class="heading-section-bold mb-4 ">
	          	<div class="ml-md-0">
                  <!-- <span class="subheading">Testimony</span> -->
		            <h2 class="mb-4">GRUPO QUÍMICO RIC</h2>
	            </div>
	          </div>
	          <div class="pb-md-5">
	          	<p style="text-align: justify;">Somos una empresa Sinaloense, con más de 30 años de experiencia en soporte para diagnóstico clínico, con sede en Los Mochis, Sinaloa y sucursales en Ciudades clave del estado, Sonora, Baja California Norte y Sur; Dando servicio al sector salud ya sea público, (IMSS, ISSSTE estatal y federal, Hospital Naval, Militar y Secretarias de Salud Estatales) privado, cadenas de Laboratorios y Hospitales privados.</p>
				<p style="text-align: justify;">Ofrecemos productos, equipos e insumos para laboratorios integrales en las áreas de: Química Clínica, Hematología, Uro-análisis, Coagulación, Inmunología, serología y Banco de sangre; incluyendo lo necesario como equipos y programas para la interfaz del laboratorio con el objetivo de automatizar los procesos analíticos y reporte de resultados, maximizando así el control interno y flujo de procesos.</p>
				
			  </div>
			</div>
			</div>
			</div>
        </section>
        
        <section class="ftco-section " style="padding:3em 0 0 0">
      <div class="container">        
        <div class="row ftco-animate">
          <div class="col-md-6">
            <!-- <div class="carousel-testimony owl-carousel"> -->

              <div class="item">
                <div class="testimony-wrap p-4 pb-5">
                  <div class="user-img" style="background-image: url(<? echo base_url() ?>plantilla/images/mision.svg);border-radius:0">
                    
                  </div>
                  <div class="text text-center mb-3">
                    <span class="position text-center" >Misión</span>
                  </div>
                  <div class="text text-center">
                    <p class="mb-5 pl-4 ">Proveer al área químico – médica, productos con alta calidad, servicios y tecnología de punta, para garantizar diagnósticos confiables y oportunos.</p>                    
                  </div>
                </div>
              </div>

            </div>
            <div class="col-md-6">

              <div class="itedm">
                <div class="testimony-wrap p-4 pb-5">
                <div class="user-img" style="background-image: url(<? echo base_url() ?>plantilla/images/vision.svg);border-radius:0">
                    
                  </div>
                  <div class="text text-center mb-3">
                    <span class="position text-center" >Visión</span>
                  </div>
                  <div class="text text-center">
                    <p class="mb-5 pl-4 line">Ser la empresa líder en la comercialización y distribución de agentes de diagnósticos y materiales quirúrgicos, con una amplia penetración en los diferentes nichos de mercado del noroeste del país.</p>              
                  </div>
                </div>
              </div>

              

            <!-- </div> -->
          </div>
        </div>
      </div>
    </section>

			
    
        <section class="ftco-section testimony-section" style="padding:0 0 3em 0;">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 heading-section ftco-animate text-center">
          	<span class="subheading">Nuestos Valores</span>
          </div>
        </div>
        <div class="row ftco-animate">
          <div class="col-md-12">
            <div class="">
              
            <div class="row no-gutters ftco-services">
          <!-- <span class="subheading">Testimony</span> -->
          <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services mb-md-0 mb-4">
              <div class="icon bg-color-1 active d-flex justify-content-center align-items-center mb-2" >
              <img src="<? echo base_url() ?>plantilla/images/exito.svg" style="width: 50%;">
                
              </div>
              <div class="media-body">
                <h3 class="heading">Experiencia</h3>
                <!-- <span>On order over $100</span> -->
              </div>
            </div>      
          </div>
          <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services mb-md-0 mb-4">
              <div class="icon bg-color-2 d-flex justify-content-center align-items-center mb-2" >
              <img src="<? echo base_url() ?>plantilla/images/apoyar.svg" style="width: 50%;">
              </div>
              <div class="media-body">
                <h3 class="heading">Compromiso</h3>
                <!-- <span>Product well package</span> -->
              </div>
            </div>    
          </div>
          <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services mb-md-0 mb-4">
              <div class="icon bg-color-3 d-flex justify-content-center align-items-center mb-2">
            		<span class="flaticon-award"></span>
              </div>
              <div class="media-body">
                <h3 class="heading">Calidad</h3>
                <!-- <span>Quality Products</span> -->
              </div>
            </div>      
          </div>
          <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services mb-md-0 mb-4">
              <div class="icon bg-color-4 d-flex justify-content-center align-items-center mb-2">
            		<span class="flaticon-customer-service"></span>
              </div>
              <div class="media-body">
                <h3 class="heading">Servicio</h3>
                <!-- <span>24/7 Support</span> -->
              </div>
            </div>      
          </div>
        </div>

            </div>
          </div>
        </div>
      </div>
    </section>  


        
        <footer class="ftco-footer ftco-section" style="background: #f7f6f2 !important;">
      <div class="container">
      	<div class="row">
      		<div class="mouse">
						<a href="#" class="mouse-icon">
							<div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
						</a>
					</div>
      	</div>
        <div class="row mb-5" style="margin-bottom:0 !important">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Grupo Químico RIC</h2>
              <p class="text-justify">Somos una empresa Sinaloense, con más de 30 años de experiencia en soporte para diagnóstico clínico, con sede en Los Mochis, Sinaloa y sucursales en Ciudades clave del estado, Sonora, Baja California Norte y Sur.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft" style="margin-top: 1rem">
                <li class="ftco-animate"><a href="#" class="icon-social icon-social-twitter"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/RIC-diagnostica-605725263279611/" class="icon-social icon-social-facebook"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="https://www.instagram.com/ricdiagnostica/" class="icon-social icon-social-instagram"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Menu</h2>
              <ul class="list-unstyled">
              <li><a href="<?php echo base_url('') ?>" class="py-2 d-block">Inicio</a></li>
                <li><a href="<?php echo base_url('/Nosotros') ?>" class="py-2 d-block">Nosotros</a></li>
                <li><a href="<?php echo base_url('/Categorias') ?>" class="py-2 d-block">Productos</a></li>
                <li><a href="<?php echo base_url('/Contacto') ?>" class="py-2 d-block">Contacto</a></li>
              </ul>
            </div>
          </div>          
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Contáctanos</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text"></span>21 de Marzo N° 428 pte, Colonia Jiquilpan,
C.P.81220 Los Mochis, Sinaloa, México.</span></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text">(668) 812 6032</span></a></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">matriz@ricdiagnostica.com</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="<? echo base_url() ?>plantilla/js/jquery.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/popper.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/bootstrap.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.easing.1.3.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.waypoints.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.stellar.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/owl.carousel.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.magnific-popup.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/aos.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.animateNumber.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/bootstrap-datepicker.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/scrollax.min.js"></script>
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="<? echo base_url() ?>plantilla/js/google-map.js"></script> -->
  <script src="<? echo base_url() ?>plantilla/js/main.js"></script>
  <script>
    $('#nosotros').addClass(" active");
  </script>
  </body>
</html>