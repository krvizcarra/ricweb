<style media="screen">
.center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 100%;
  height:200px;
}
</style>
<div class="hero-wrap hero-bread" style="background-image: url('<? echo base_url() ?>plantilla/images/productos.jpeg'); opacity:0.4;position: absolute;">

</div>

<div class="hero-wrap hero-bread">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <h1 class="mb-0 bread" style="color:#484444f0">Productos</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section" style="padding: 3em 0 3em 0 !important;">
  <input type="hidden" id="base_url" value="<?php echo base_url() ?>">
  <div class="container">
    <div class="row justify-content-center mb-3 pb-3">
      <div class="col-md-12 heading-section text-center ftco-animate">
        <span class="subheading">Nuestros Productos</span>
        <h2 class="mb-4">Productos</h2>
        <p></p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12 mb-12 text-center">
        <ul class="product-category">
        <?php if ($cat == -1): ?>
          <li><a class="categorias active" onclick="filtroCategorias(this)" categoria="-1"  href="#">Todo</a></li>
          <?php else: ?>
          <li><a class="categorias" onclick="filtroCategorias(this)" categoria="-1"  href="#">Todo</a></li>
          <?php endif; ?>

          <?php foreach ($categorias as $key => $categoria): ?>
              <?php if ($categoria->id == $cat): ?>
                <li><a class="categorias active" onclick="filtroCategorias(this)" categoria="<?php echo $categoria->id ?>"  href="#"><?php echo $categoria->categoria ?></a></li>
                <?php else: ?>
                  <li><a onclick="filtroCategorias(this)" class="categorias" categoria="<?php echo $categoria->id ?>" href="#"><?php echo $categoria->categoria ?></a></li>
              <?php endif; ?>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>


    <div class="row" id="contenedorProducto">

        <?php foreach ($productos as $producto): ?>
          <div class="col-md-6 col-lg-4 ftco-animate" style="height:325px;margin-top:10px;">
            <div class="product"  >
              <a  class="img-prod"><img class="img-fluid center" alt="img" src="<?php echo $producto->img ?>">
                <!-- <span class="status">30%</span> -->
                <div class="overlay"></div>
              </a>
              <div class="text py-3 pb-4 px-3 text-center">
                <h3 style="height:41px;"><a><?php echo $producto->Producto ?></a></h3>
                <div class="d-flex">
                  <div class="pricing">
                    <p class="price"><span class="mr-2 price-dc"></span><span class="price-sale"></span></p>
                  </div>
                </div>
                <div class="bottom-area d-flex px-3">
                  <div class="m-auto d-flex">
                    <a class="add-to-cart d-flex justify-content-center align-items-center text-center" style="cursor: pointer;" onclick="detalleproducto(this)" folio="<?php echo $producto->id ?>" target="tooltip" title="Ver Detalles">
                      <span><i class="ion-ios-eye"></i></span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>

    </div>

    
      <div class="row mt-5" >
        <div class="col text-center">
          <div class="block-27">
            <ul id="ulPaginacion">
            <?php if ($paginas > 1): ?>
              <?php for ($i=1; $i <= $paginas ; $i++) { ?>
                <?php if ($i==1): ?>
                    <li class="active paginado"><a onclick="paginado(this)" pagina="<?php echo ($i - 1) ?>"><?php echo $i ?></a></li>
                  <?php else: ?>
                    <li class="paginado"><a onclick="paginado(this)" pagina="<?php echo ($i - 1) ?>"><?php echo $i ?></a></li>
                <?php endif; ?>
              <?php } ?>
              <?php endif; ?>
            </ul>
          </div>
        </div>
      </div>
   


  </div>
</section>


<footer class="ftco-footer ftco-section" style="background: #f7f6f2 !important;">
<div class="container">
<div class="row">
  <div class="mouse">
    <a href="#" class="mouse-icon">
      <div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
    </a>
  </div>
</div>
<div class="row mb-5" style="margin-bottom:0 !important">
  <div class="col-md">
    <div class="ftco-footer-widget mb-4">
    <h2 class="ftco-heading-2">Grupo Químico RIC</h2>
      <p class="text-justify">Somos una empresa Sinaloense, con más de 30 años de experiencia en soporte para diagnóstico clínico, con sede en Los Mochis, Sinaloa y sucursales en Ciudades clave del estado, Sonora, Baja California Norte y Sur.</p>
      <ul class="ftco-footer-social list-unstyled float-md-left float-lft" style="margin-top: 1rem">
        <li class="ftco-animate"><a href="#" class="icon-social icon-social-twitter"><span class="icon-twitter"></span></a></li>
        <li class="ftco-animate"><a href="https://www.facebook.com/RIC-diagnostica-605725263279611/" class="icon-social icon-social-facebook"><span class="icon-facebook"></span></a></li>
        <li class="ftco-animate"><a href="https://www.instagram.com/ricdiagnostica/" class="icon-social icon-social-instagram"><span class="icon-instagram"></span></a></li>
      </ul>
    </div>
  </div>
  <div class="col-md">
    <div class="ftco-footer-widget mb-4 ml-md-5">
      <h2 class="ftco-heading-2">Menu</h2>
      <ul class="list-unstyled">
      <li><a href="<?php echo base_url('') ?>" class="py-2 d-block">Inicio</a></li>
        <li><a href="<?php echo base_url('/Nosotros') ?>" class="py-2 d-block">Nosotros</a></li>
        <li><a href="<?php echo base_url('/Categorias') ?>" class="py-2 d-block">Productos</a></li>
        <li><a href="<?php echo base_url('/Contacto') ?>" class="py-2 d-block">Contacto</a></li>
      </ul>
    </div>
  </div>
  <div class="col-md">
    <div class="ftco-footer-widget mb-4">
      <h2 class="ftco-heading-2">Contáctanos</h2>
      <div class="block-23 mb-3">
        <ul>
          <li><span class="icon icon-map-marker"></span><span class="text"></span>21 de Marzo N° 428 pte, Colonia Jiquilpan,
C.P.81220 Los Mochis, Sinaloa, México.</span></li>
          <li><a href="#"><span class="icon icon-phone"></span><span class="text">(668) 812 6032</span></a></li>
          <li><a href="#"><span class="icon icon-envelope"></span><span class="text">matriz@ricdiagnostica.com</span></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

</div>
</footer>





<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.3.2/bootbox.js"></script> -->

<script src="<? echo base_url() ?>plantilla/js/jquery.min.js"></script>
<script src="<? echo base_url() ?>plantilla/js/jquery-migrate-3.0.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<script src="<? echo base_url() ?>plantilla/js/popper.min.js"></script>
<script src="<? echo base_url() ?>plantilla/js/bootstrap.min.js"></script>
<script src="<? echo base_url() ?>plantilla/js/jquery.easing.1.3.js"></script>
<script src="<? echo base_url() ?>plantilla/js/jquery.waypoints.min.js"></script>
<script src="<? echo base_url() ?>plantilla/js/jquery.stellar.min.js"></script>
<script src="<? echo base_url() ?>plantilla/js/owl.carousel.min.js"></script>
<script src="<? echo base_url() ?>plantilla/js/jquery.magnific-popup.min.js"></script>
<script src="<? echo base_url() ?>plantilla/js/aos.js"></script>
<script src="<? echo base_url() ?>plantilla/js/jquery.animateNumber.min.js"></script>
<script src="<? echo base_url() ?>plantilla/js/bootstrap-datepicker.js"></script>
<script src="<? echo base_url() ?>plantilla/js/scrollax.min.js"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="<? echo base_url() ?>plantilla/js/google-map.js"></script> -->
<script src="<? echo base_url() ?>plantilla/js/main.js"></script>
<script type="text/javascript" src="<? echo base_url('plantilla/bootbox/bootbox.all.min.js') ?>"></script>

<script>
  $('#productos').addClass(" active");

function paginado(objecto) {

  let datos = {
    pagina: $(objecto).attr("pagina"),
    categoria: $("a.categorias.active").attr("categoria")
  }
  console.log(datos);
  
  let url = $("#base_url").val() + "Web/Apps/PaginadoProductos";

  $.post(url,datos,function(data){

      if (data.error) {
        alert(data.msg)
      }else {
        $(".paginado>a").css({"color":"#000"})
        $(".paginado").removeClass("active")
        $(objecto).parents("li").addClass("active")
        $(objecto).css({"color":"#fff"})
        $("#contenedorProducto").empty();

        $(data.msg).each(function(i,item){
          $("#contenedorProducto").append('<div class="col-md-6 col-lg-4 ftco-animate fadeInUp ftco-animated" style="height:325px;margin-top:10px;">'+
                                            '<div class="product"  >'+
                                              '<a href="#" class="img-prod"><img class="img-fluid center" alt="img" src="'+item.img+'">'+
                                                '<div class="overlay"></div>'+
                                              '</a>'+
                                              '<div class="text py-3 pb-4 px-3 text-center">'+
                                                '<h3 style="height:41px;"><a href="#">'+item.Producto+'</a></h3>'+
                                                '<div class="d-flex">'+
                                                  '<div class="pricing">'+
                                                    '<p class="price"><span class="mr-2 price-dc"></span><span class="price-sale"></span></p>'+
                                                  '</div>'+
                                                '</div>'+
                                                '<div class="bottom-area d-flex px-3">'+
                                                  '<div class="m-auto d-flex">'+
                                                    '<a class="add-to-cart d-flex justify-content-center align-items-center text-center" onclick="detalleproducto(this)" folio="'+item.id+'" target="tooltip" title="Ver Detalles">'+
                                                      '<span><i class="ion-ios-eye"></i></span>'+
                                                    '</a>'+
                                                  '</div>'+
                                                '</div>'+
                                              '</div>'+
                                            '</div>'+
                                          '</div>')
        });

      }
    },"json").fail(function(e,ex,error){
      alert(error)
    })
}



  function filtroCategorias(objecto) {
    let datos = {categoria: $(objecto).attr("categoria")}
    let url = $("#base_url").val() + "Web/Apps/ProductosPorCategoria";
    $.post(url,datos,function(data){
      // console.log(data);
      if (data.error) {
        alert(data.msg)
      }else {
        $(".categorias").removeClass("active")
        $(objecto).addClass("active")
        paginacion(data.paginas)
        $("#contenedorProducto").empty();
        $(data.msg).each(function(i,item){
          $("#contenedorProducto").append('<div class="col-md-6 col-lg-4 ftco-animate fadeInUp ftco-animated" style="height:325px;margin-top:10px;">'+
                                            '<div class="product"  >'+
                                              '<a href="#" class="img-prod"><img class="img-fluid center" alt="img" src="'+item.img+'">'+
                                                '<div class="overlay"></div>'+
                                              '</a>'+
                                              '<div class="text py-3 pb-4 px-3 text-center">'+
                                                '<h3 style="height:41px;"><a href="#">'+item.Producto+'</a></h3>'+
                                                '<div class="d-flex">'+
                                                  '<div class="pricing">'+
                                                    '<p class="price"><span class="mr-2 price-dc"></span><span class="price-sale"></span></p>'+
                                                  '</div>'+
                                                '</div>'+
                                                '<div class="bottom-area d-flex px-3">'+
                                                  '<div class="m-auto d-flex">'+
                                                  '<a class="add-to-cart d-flex justify-content-center align-items-center text-center" onclick="detalleproducto(this)" folio="'+item.id+'" target="tooltip" title="Ver Detalles">'+
                                                      '<span><i class="ion-ios-eye"></i></span>'+
                                                    '</a>'+
                                                  '</div>'+
                                                '</div>'+
                                              '</div>'+
                                            '</div>'+
                                          '</div>')
        });
      }
    },"json").fail(function(e,ex,error){
      alert(error)
    })
  }

  function paginacion(paginas) {
    $("#ulPaginacion").empty();
    if (paginas > 1) {
      for (var i = 1; i <= paginas; i++) {
        if (i == 1) {
          $("#ulPaginacion").append('<li class="active paginado"><a onclick="paginado(this)" pagina="'+(i-1)+'">' + i +'</a></li>');
        }else {
          $("#ulPaginacion").append('<li class="paginado"><a onclick="paginado(this)" pagina="'+(i-1)+'">'+i+'</a></li>');
        }
      }
    }

  }

  function detalleproducto(objeto) {
    let url = $("#base_url").val() + "Web/Apps/DetalleProducto";
    let datos = {
      folio: $(objeto).attr("folio")
    };
    $.post(url,datos,function(data){
      // console.log(data);
      bootbox.alert({
        title: "Detalle",
        message: $(data),
        size: "large",
        buttons: {
          ok: {
            label: 'cerrar',
            className: 'btn-primary',
          }
        }
      })
    }).fail(function(e,ex,error){
      alert(error);
    })
  }
</script>
</body>
</html>
