
    <section id="home-section" class="hero">

		  <div class="home-slider owl-carousel">
	      <div class="slider-item" style="background-image: url(<? echo base_url() ?>plantilla/images/slide01.jpg);">
	      	<div class="overlay"></div>
	        <div class="container">
	          <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
	          </div>
	        </div>
	      </div>

	      <div class="slider-item" style="background-image: url(<? echo base_url() ?>plantilla/images/slide02.jpg);">
	      	<div class="overlay"></div>
	        <div class="container">
	          <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
	          </div>
	        </div>
        </div>
        
        <div class="slider-item" style="background-image: url(<? echo base_url() ?>plantilla/images/slide03.jpg);">
	      	<div class="overlay"></div>
	        <div class="container">
	          <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
	          </div>
	        </div>
	      </div>

	    </div>
    </section>

    <section class="ftco-section testimony-section">
      <div class="container">
        <div class="row justify-content-center  pb-3">
          <div class="col-md-12 heading-section ftco-animate text-center">
          	<!-- <span class="subheading">Testimony</span> -->
            <h2 class="mb-12">GRUPO QUÍMICO RIC</h2>
            <p>Brindamos sistemas automatizados, pruebas, softwares y servicios para ayudar a los laboratorios clínicos a generar resultados confiables de manera más efectiva y eficiente.</p>
          </div>
        </div>        
      </div>
    </section>

    <section class="ftco-section mb-5">
			<div class="container">
				<div class="row no-gutters ftco-services">
          <!-- <span class="subheading">Testimony</span> -->
          <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services mb-md-0 mb-4">
              <div class="icon bg-color-1 active d-flex justify-content-center align-items-center mb-2" >
              <img src="<? echo base_url() ?>plantilla/images/exito.svg" style="width: 50%;">
                
              </div>
              <div class="media-body">
                <h3 class="heading">Experiencia</h3>
                <!-- <span>On order over $100</span> -->
              </div>
            </div>      
          </div>
          <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services mb-md-0 mb-4">
              <div class="icon bg-color-2 d-flex justify-content-center align-items-center mb-2" >
              <img src="<? echo base_url() ?>plantilla/images/apoyar.svg" style="width: 50%;">
              </div>
              <div class="media-body">
                <h3 class="heading">Compromiso</h3>
                <!-- <span>Product well package</span> -->
              </div>
            </div>    
          </div>
          <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services mb-md-0 mb-4">
              <div class="icon bg-color-3 d-flex justify-content-center align-items-center mb-2">
            		<span class="flaticon-award"></span>
              </div>
              <div class="media-body">
                <h3 class="heading">Calidad</h3>
                <!-- <span>Quality Products</span> -->
              </div>
            </div>      
          </div>
          <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services mb-md-0 mb-4">
              <div class="icon bg-color-4 d-flex justify-content-center align-items-center mb-2">
            		<span class="flaticon-customer-service"></span>
              </div>
              <div class="media-body">
                <h3 class="heading">Servicio</h3>
                <!-- <span>24/7 Support</span> -->
              </div>
            </div>      
          </div>
        </div>
			</div>
    </section>
    
    <section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url(<? echo base_url() ?>plantilla/images/slide07.jpg);">
      <div class="container">
        <div class="row justify-content-center py-5">
          <div class="col-md-10">
            <div class="row">              
              <div class="container">
                <div class="row no-gutters slider-text align-items-center justify-content-center">
                  <div class="col-md-9 ftco-animate text-center fadeInUp ftco-animated">
                    <h1 class="mb-0 bread" style="color:#379a62">En Grupo Químico RIC</h1>
                    <p style="color:#000" class="breadcrumbs">Ofrecemos productos, equipos e insumos para laboratorios integrales en las áreas de: Química Clínica, Hematología, Uroanalisis, Coagulación, Inmunología, Serologíay banco de sangre; incluyendo lo necesario como equipos y programas para la interfaz del laboratorio con el objetivo de automatizar los procesos analíticos y reporte de resultados, maximizando así el control interno y flojo de procesos.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    
    <hr>

		<section class="ftco-section ftco-sectionx ftco-partner">
      <div class="container">
        <section class="customer-logos slider">
          <div class="slide"><img class="img-partner" src="<? echo base_url() ?>plantilla/images/partner01.png"></div>
          <div class="slide"><img src="<? echo base_url() ?>plantilla/images/partner02.png"></div>
          <div class="slide"><img src="<? echo base_url() ?>plantilla/images/partner03.png"></div>
          <div class="slide"><img src="<? echo base_url() ?>plantilla/images/partner04.png"></div>
          <div class="slide"><img src="<? echo base_url() ?>plantilla/images/partner05.png"></div>
          <div class="slide"><img src="<? echo base_url() ?>plantilla/images/partner06.png"></div>
          <div class="slide"><img src="<? echo base_url() ?>plantilla/images/partner07.png"></div>          
        </section>
      </div>
    </section>
		
    <footer class="ftco-footer ftco-section" style="background: #f7f6f2 !important;">
      <div class="container">
      	<div class="row">
      		<div class="mouse">
						<a href="#" class="mouse-icon">
							<div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
						</a>
					</div>
      	</div>
        <div class="row mb-5" style="margin-bottom:0 !important">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Grupo Químico RIC</h2>
              <p class="text-justify">Somos una empresa Sinaloense, con más de 30 años de experiencia en soporte para diagnóstico clínico, con sede en Los Mochis, Sinaloa y sucursales en Ciudades clave del estado, Sonora, Baja California Norte y Sur.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft" style="margin-top: 1rem">
                <li class="ftco-animate"><a href="#" class="icon-social icon-social-twitter"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/RIC-diagnostica-605725263279611/" class="icon-social icon-social-facebook"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="https://www.instagram.com/ricdiagnostica/" class="icon-social icon-social-instagram"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Menu</h2>
              <ul class="list-unstyled">
                <li><a href="<?php echo base_url('') ?>" class="py-2 d-block">Inicio</a></li>
                <li><a href="<?php echo base_url('/Nosotros') ?>" class="py-2 d-block">Nosotros</a></li>
                <li><a href="<?php echo base_url('/Categorias') ?>" class="py-2 d-block">Productos</a></li>
                <li><a href="<?php echo base_url('/Contacto') ?>" class="py-2 d-block">Contacto</a></li>
              </ul>
            </div>
          </div>          
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Contáctanos</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text"></span>21 de Marzo N° 428 pte, Colonia Jiquilpan,
C.P.81220 Los Mochis, Sinaloa, México.</span></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text">(668) 812 6032</span></a></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">matriz@ricdiagnostica.com</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="<? echo base_url() ?>plantilla/js/jquery.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/popper.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/bootstrap.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.easing.1.3.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.waypoints.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.stellar.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/owl.carousel.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.magnific-popup.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/aos.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.animateNumber.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/bootstrap-datepicker.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/scrollax.min.js"></script>
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="<? echo base_url() ?>plantilla/js/google-map.js"></script> -->
  <script src="<? echo base_url() ?>plantilla/js/main.js"></script>
    
  <script>
    $('#inicio').addClass(" active");
  </script>

  </body>
</html>