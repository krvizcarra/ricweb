<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Grupo Químico RIC</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="<? echo base_url() ?>plantilla/images/icon.png" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<? echo base_url() ?>plantilla/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="<? echo base_url() ?>plantilla/css/animate.css">

    <link rel="stylesheet" href="<? echo base_url() ?>plantilla/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<? echo base_url() ?>plantilla/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<? echo base_url() ?>plantilla/css/magnific-popup.css">

    <link rel="stylesheet" href="<? echo base_url() ?>plantilla/css/aos.css">
    <link rel="stylesheet" href="<? echo base_url() ?>plantilla/css/infinite-slider.css">

    <link rel="stylesheet" href="<? echo base_url() ?>plantilla/css/ionicons.min.css">

    <link rel="stylesheet" href="<? echo base_url() ?>plantilla/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<? echo base_url() ?>plantilla/css/jquery.timepicker.css">


    <link rel="stylesheet" href="<? echo base_url() ?>plantilla/css/flaticon.css">
    <link rel="stylesheet" href="<? echo base_url() ?>plantilla/css/icomoon.css">
    <link rel="stylesheet" href="<? echo base_url() ?>plantilla/css/style.css">
  </head>
  <body class="goto-here">
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light scrolled awake" id="ftco-navbar">
	    <div class="container">
	      <!-- <a class="navbar-brand" href="index.html"> -->
			  <img src="<? echo base_url() ?>plantilla/images/Logo_RIC.jpg" style="width: 10%;">
		  <!-- </a> -->
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
			  <li class="nav-item" id="inicio"><a href="<?php echo base_url('') ?>" class="nav-link">Inicio</a></li>
			  <li class="nav-item" id="nosotros"><a href="<?php echo base_url('/Nosotros') ?>" class="nav-link">Nosotros</a></li>
        <li class="nav-item" id="productos"><a href="<?php echo base_url('/Categorias') ?>" class="nav-link">Productos</a></li>

	          <!-- <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Shop</a>
              <div class="dropdown-menu" aria-labelledby="dropdown04">
              	<a class="dropdown-item" href="shop.html">Shop</a>
              	<a class="dropdown-item" href="wishlist.html">Wishlist</a>
                <a class="dropdown-item" href="product-single.html">Single Product</a>
                <a class="dropdown-item" href="cart.html">Cart</a>
                <a class="dropdown-item" href="checkout.html">Checkout</a>
              </div>
            </li> -->
	          <!-- <li class="nav-item"><a href="blog.html" class="nav-link">Blog</a></li> -->
	          <li class="nav-item" id="contacto"><a href="<?php echo base_url('/Contacto') ?>" class="nav-link">Contacto</a></li>
	          <!-- <li class="nav-item cta cta-colored"><a href="cart.html" class="nav-link"><span class="icon-shopping_cart"></span>[0]</a></li> -->

	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->
