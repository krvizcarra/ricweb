<footer class="ftco-footer ftco-section" style="background: #f7f6f2 !important;">
      <div class="container">
      	<div class="row">
      		<div class="mouse">
						<a href="#" class="mouse-icon">
							<div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
						</a>
					</div>
      	</div>
        <div class="row mb-5" style="margin-bottom:0 !important">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Diagnóstica Comercial, S.A. de C.V.</h2>
              <p>Somos una empresa Sinaloense, con más de 30 años de experiencia en soporte para diagnóstico clínico, con sede en Los Mochis, Sinaloa y sucursales en Ciudades clave del estado, Sonora, Baja California Norte y Sur.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="#" class="icon-social"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/RIC-diagnostica-605725263279611/" class="icon-social"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#" class="icon-social"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Menu</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">Shop</a></li>
                <li><a href="#" class="py-2 d-block">About</a></li>
                <li><a href="#" class="py-2 d-block">Journal</a></li>
                <li><a href="#" class="py-2 d-block">Contact Us</a></li>
              </ul>
            </div>
          </div>          
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Contáctanos</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text"></span>21 de Marzo N° 428 pte, Colonia Jiquilpan,
C.P.81220 Los Mochis, Sinaloa, México.</span></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text">(668) 812 6032</span></a></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">matriz@ricdiagnostica.com</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="<? echo base_url() ?>plantilla/js/jquery.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/popper.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/bootstrap.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.easing.1.3.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.waypoints.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.stellar.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/owl.carousel.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.magnific-popup.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/aos.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/jquery.animateNumber.min.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/bootstrap-datepicker.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="<? echo base_url() ?>plantilla/js/google-map.js"></script>
  <script src="<? echo base_url() ?>plantilla/js/main.js"></script>
    
  </body>
</html>