$(document).ready(function () {
    var $image = $(".image-crop > img");
    $('#ModalAvatar').on('shown.bs.modal', function () {

      $($image).cropper({
        aspectRatio: 1.1,
        preview: ".img-preview",
        done: function(data) {
          // Output the result data for cropping image.
        }
      });
    });

    var $inputImage = $("#inputImage");

            if (window.FileReader) {

                $inputImage.change(function() {
                    var fileReader = new FileReader(),
                            files = this.files,
                            file;

                    if (!files.length) {
                        return;
                    }

                    file = files[0];

                    if (/^image\/\w+$/.test(file.type)) {
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function () {
                            $inputImage.val("");
                            $image.cropper("reset", true).cropper("replace", this.result);
                        };
                    } else {
                      $.toast({
                        heading: 'Error',
                        hideAfter: 4000,
                        position: 'bottom-right',
                        text: 'Formato de imagen incorrecto',
                        icon: 'error'
                      });
                    }
                });
            } else {
                $inputImage.addClass("hide");
            }


    $('#btnGuardar').click(function(event) {
      var dato={
        'imagen':$image.cropper("getDataURL")
      };
      var base_url = $('#base_url').val();
      $.ajax({
          url: base_url+'Home/Apps/avatar',
          type: 'POST',
          data: dato,
          cache:false,
          dataType: 'json',
          success: function(data, textStatus, jqXHR) {
              if(!data.error) {
                $('#avatar').attr('src',$image.cropper("getDataURL"));
                $('#avatar-imagen').attr('src',$image.cropper("getDataURL"));
                $('#ModalAvatar').modal('hide');
                  $.toast({
                    heading: 'Exíto',
                    hideAfter: 5000,
                    position: 'bottom-right',
                    text: data.msg,
                    icon: 'success'
                  });

              }else {
                  $.toast({
                    heading: 'Error',
                    hideAfter: 5000,
                    position: 'bottom-right',
                    text: data.msg,
                    icon: 'error'
                  });
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
              // Handle errors here
              console.log('ERRORS: ' + errorThrown);
          }
      });//ajax
    });


}); //.ready


var fila;
$("#tblPerfil tbody").on('click','tr .opcion',function(e) {
    $("#modal-editar")[0].reset();
    fila = $(this).closest('tr');
    var titulo = $(this).attr('data-original-title');
    if (titulo=="Cambiar contraseña") {
      $('#nuevo').attr('type','password');
      $('#confirmar').attr('type','password');
    }else{
      $('#nuevo').attr('type','text');
      $('#confirmar').attr('type','text');
    }
    var opcion = $(this).attr('opcion');
    var nuevo = $(this).attr('new');
    var confirmar = $(this).attr('confirm');

    $('#modal-titulo').text(titulo);
    $('#opcion').val(opcion);
    $('#label-nuevo').text(nuevo);
    $('#label-confirmar').text(confirmar);
});


$("#modal-editar").submit(function(e){
  e.preventDefault();
  var base_url = $('#base_url').val();
  var form = $('#modal-editar');
  $.ajax({
      url: base_url+'Home/Apps/editarPerfil',
      type: 'POST',
      data: form.serialize(),
      cache:false,
      dataType: 'json',
      success: function(data, textStatus, jqXHR) {
          if(!data.error) {
              fila.find('.set').val(data.valor);
              $("#modal-editar")[0].reset();
              $('#ModalEditar').modal('hide');
              $.toast({
                heading: 'Exíto',
                hideAfter: 5000,
                position: 'bottom-right',
                text: data.msg,
                icon: 'success'
              });
          }else {
              $.toast({
                heading: 'Error',
                hideAfter: 5000,
                position: 'bottom-right',
                text: data.msg,
                icon: 'error'
              });
          }
      },
      error: function(jqXHR, textStatus, errorThrown) {
          // Handle errors here
          console.log('ERRORS: ' + errorThrown);
      }
  });//ajax
});

$("#form-perfil").submit(function(e){
  e.preventDefault();
  var base_url = $('#base_url').val();
  $.ajax({
      url: base_url+'Home/Apps/guardarPerfil',
      type: 'POST',
      data: $(this).serialize(),
      cache:false,
      dataType: 'json',
      success: function(data, textStatus, jqXHR) {
          if(!data.error) {
              $('#nombre').val($('#nombre').val().toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
              }));
              $('#avatar-nombre').text($('#nombre').val());
              $('#nombre-titulo').text($('#nombre').val());
              $('#usuario').val($('#usuario').val().toLowerCase());
              $.toast({
                heading: 'Exíto',
                hideAfter: 5000,
                position: 'bottom-right',
                text: data.msg,
                icon: 'success'
              });
              // console.log(data);
          }else {
              $.toast({
                heading: 'Error',
                hideAfter: 5000,
                position: 'bottom-right',
                text: data.msg,
                icon: 'error'
              });
          }
      },
      error: function(jqXHR, textStatus, errorThrown) {
          // Handle errors here
          console.log('ERRORS: ' + errorThrown);
      }
  });//ajax
});
