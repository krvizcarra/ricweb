$(document).ready(function () {
  $('.fileinput').fileinput();
  $('#tblSistemas').DataTable({
      pageLength: 10,
      ajax: $("#base_url").val()+'Home/Apps/obtenerSistemas',
      responsive: true,
      bLengthChange: false,
      language: {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },
      columnDefs: [ { "targets": 5, "sClass": "text-center" },
                    { "targets": 0, "sClass": "id" },
                    { "targets": 1, "sClass": "nombre" },
                    { "targets": 2, "sClass": "descripcion" },
                    { "targets": 3, "sClass": "url" },
                    { "targets": 4, "sClass": "imagen hidden" },

     ],
      dom: '<"html5buttons"B>lTfgitp',
      buttons: [
          { extend: 'copy'},
          {extend: 'excel', title: 'ExampleFile'},
          {extend: 'pdf', title: 'ExampleFile'},

          {extend: 'print',
           customize: function (win){
                  $(win.document.body).addClass('white-bg');
                  $(win.document.body).css('font-size', '10px');

                  $(win.document.body).find('table')
                          .addClass('compact')
                          .css('font-size', 'inherit');
          }
          }
      ]
  });//DataTable

  var fila;
  $("#tblSistemas tbody").on('click','tr .opciones',function(e) {
      $('#lb-titulo').text('Modificar sistema');
      fila = $(this).closest('tr');
      var id = fila.find(".id").text();
      var nombre = fila.find(".nombre").text();
      var descripcion = fila.find(".descripcion").text();
      var url = fila.find(".url").text();
      var imagen = fila.find(".imagen").text();

      $('#id').val(id);
      $('#nombre').val(nombre);
      $('#descripcion').val(descripcion);
      $('#url').val(url);
      $('#filename').val(imagen);
      $('#file').attr('required', false);
      $('#preview').text('');
      $('#preview').append('<img src="'+imagen+'" />');
      //$('#.fileinput-new').text('');
  });
  $('#file').change(function() {
    var fileReader = new FileReader(),
            files = this.files,
            file;

    if (!files.length) {
        return;
    }

    file = files[0];

    if (/^image\/\w+$/.test(file.type)) {
        fileReader.readAsDataURL(file);
        fileReader.onload = function () {
            $('#filename').val(this.result);
            $('#preview').text('');
            $('#preview').append('<img src="'+this.result+'" />');

        };
    } else {
      $.toast({
        heading: 'Error',
        hideAfter: 4000,
        position: 'bottom-right',
        text: 'Formato de imagen incorrecto',
        icon: 'error'
      });
    }
    $('.fileinput-new').css({'display':'block'});

  });

  $("#form-sistemas").submit(function(e){
    e.preventDefault();
    var base_url = $('#base_url').val();
    $.ajax({
        url: base_url+'Home/Apps/guardarSistema',
        type: 'POST',
        data: $(this).serialize(),
        cache:false,
        dataType: 'json',
        success: function(data, textStatus, jqXHR) {
            if(!data.error) {
                $('#tblSistemas').DataTable().ajax.reload();
                $('#Modal').modal('hide');
                $('form#form-sistemas :input').each(function (i,item) {
                  $(this).val('');
                })
                $.toast({
                  heading: 'Exíto',
                  hideAfter: 5000,
                  position: 'bottom-right',
                  text: 'Información guardada correctamente',
                  icon: 'success'
                });

            }else {
                $.toast({
                  heading: 'Error',
                  hideAfter: 5000,
                  position: 'bottom-right',
                  text: data.msg,
                  icon: 'error'
                });
            }
            // console.log(data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // Handle errors here
            console.log('Error: '+jqXHR);
        }
    });//ajax
  });

  $('#btnNuevo').click(function(event) {
    event.preventBubble=true;
    $('#lb-titulo').text('Nuevo sistema');
    $('form#form-sistemas :input').each(function (i,item) {
      $(this).val('');
    })
    $('#preview').text('');
    $('#id').val(0);
    $('#file').attr('required', true);
  });


}); //.ready
