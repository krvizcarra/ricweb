$("#btnNuevo").on("click",function(e){

    let urlForm = $("#base_url").val()+'RendivalesCredito/Apps/nuevoPago';

    $.get(urlForm,{},function(data){
        bootbox.confirm({
            message: $(data),
            buttons: {
                cancel: {
                  label: '<span class="glyphicon glyphicon-remove"></span> Cancelar',
                  className: 'btn btn-danger btn-sm'
                },
                confirm: {
                    label: '<span class="glyphicon glyphicon-floppy-saved"></span> Confirmar',
                    className: 'btn-primary btn-sm'
                }
              },
            callback: function (result) {
              if (result) {
                  $("#frmCorte").submit();
                return false;
              }
            }
        });
    })

});

$(document).on( 'click', '.viewCortes', function (){
  var folio = $(this).attr("folio");

  var row = $(this).closest("tr");
  var estacion = row.find(".estacion").text();


  var url = $("#base_url").val() + 'RendivalesCredito/Apps/verCortesPago?folio='+folio+'&estacion='+estacion;

  window.location.href = url;
});

$(document).on( 'click', '.deletePago', function (){
  var row = $(this).closest("tr");
  var url = $("#base_url").val() + 'RendivalesCredito/Apps/eliminarPago';
  var datos = {folio: $(this).attr("folio")};
  swal({
      text: "Eliminar Pago "+datos.folio+" de la estación: "+row.find(".estacion").text(),
      content: "info",
      closeOnConfirm: false,
      buttons: {
        cancel: "Cancelar",
        catch: {
          text: "Confirmar",
        }
      }
  }).then(name => {
      if (name == 'catch') {
        $.post(url,datos,function(data){
          tblCortes.ajax.reload();
          if (data.error) {
            swal("Error!", data.msg, "error");
          }else {
            swal("Exito!", data.msg, "success");
          }
        },'json').fail(function(e,x,error){
          swal("Error!", error, "error");
        });
      }
  })
});


$(document).on( 'click', '.confirmarPago', function (){
  var row = $(this).closest("tr");
  var url = $("#base_url").val() + 'RendivalesCredito/Apps/confirmarPago';
  var datos = {folio: $(this).attr("folio")};
  swal({
      text: "Confirmar Pago "+datos.folio+" de la estación: "+row.find(".estacion").text(),
      content: "info",
      closeOnConfirm: false,
      buttons: {
        cancel: "Cancelar",
        catch: {
          text: "Confirmar",
        }
      }
  }).then(name => {
      if (name == 'catch') {
        $.post(url,datos,function(data){
          tblCortes.ajax.reload();
          if (data.error) {
            swal("Error!", data.msg, "error");
          }else {
            swal("Exito!", data.msg, "success");
          }

        },'json').fail(function(e,x,error){
          swal("Error!", error, "error");
        });
      }
  })
});

tblCortes = $('#tb-cortes').DataTable({
      pageLength: 10,
      ajax: $("#base_url").val()+'RendivalesCredito/Apps/obtenerPagosVales',
      columns: [
          { title: "ID", className: 'folio'},
          { title: "Estación",className: 'estacion'},
          { title: "Fecha"},
          { title: "Monto",className: 'dt-center'},
          { title: "Monto Verificado",className: 'dt-center'},
          { title: "Estado",className: 'dt-center'},
          { title: "Opciones",className: 'dt-center'},
      ],
      responsive: true,
      bLengthChange: false,
      order: [ [0, 'desc'] ],
      language: {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },
      columnDefs: [ {
          "targets": -1,
          // "data": null,
          "sClass": "text-center",

      } ],
      dom: '<"html5buttons"B>lTfgitp',
      buttons: [
          { extend: 'copy'},
          {extend: 'excel', title: 'ExampleFile'},
          {extend: 'pdf', title: 'ExampleFile'},

          {extend: 'print',
           customize: function (win){
                  $(win.document.body).addClass('white-bg');
                  $(win.document.body).css('font-size', '10px');

                  $(win.document.body).find('table')
                          .addClass('compact')
                          .css('font-size', 'inherit');
          }
          }
      ]

  })
