  $(document).ready(function(){

  tblVentas =  $('#tb-ventas').DataTable({
                  pageLength: 10,
                  ajax: $("#base_url").val()+'RendivalesCredito/Apps/obtenerVentas',
                  responsive: true,
                  fnDrawCallback: function() {
                    $('.toggle-estado').bootstrapToggle({
                          on: 'Pagada',
                          off: 'Pendiente',

                    });
                  },
                  order:[[0,'desc']],
                  bLengthChange: false,
                  language: {
                      "sProcessing":     "Procesando...",
                      "sLengthMenu":     "Mostrar _MENU_ registros",
                      "sZeroRecords":    "No se encontraron resultados",
                      "sEmptyTable":     "Ningún dato disponible en esta tabla",
                      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                      "sInfoPostFix":    "",
                      "sSearch":         "Buscar:",
                      "sUrl":            "",
                      "sInfoThousands":  ",",
                      "sLoadingRecords": "Cargando...",
                      "oPaginate": {
                          "sFirst":    "Primero",
                          "sLast":     "Último",
                          "sNext":     "Siguiente",
                          "sPrevious": "Anterior"
                      },
                      "oAria": {
                          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                      }
                  },
                  columnDefs: [
                      {
                        "targets": [-1],
                        "sClass": "text-center ajustar",
                      },
                      {
                        "targets": [-2],
                        "sClass": "text-center ajustar",
                        "width": "5%"
                      },
                        //  {
                        //   "targets": 4,
                        //   "sClass": "text-center",
                        // },

                     ],
                  dom: '<"html5buttons"B>lTfgitp',
                  buttons: [
                      { extend: 'copy'},
                      {extend: 'excel', title: 'ExampleFile'},
                      {extend: 'pdf', title: 'ExampleFile'},

                      {extend: 'print',
                       customize: function (win){
                              $(win.document.body).addClass('white-bg');
                              $(win.document.body).css('font-size', '10px');

                              $(win.document.body).find('table')
                                      .addClass('compact')
                                      .css('font-size', 'inherit');
                      }
                      }
                  ]

              });

    $('#formVenta').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: { },
        fields: {
            InputCliente: {
              validators: {
                notEmpty: {
                  message: 'Seleccione un cliente'
                }
              }
            },
        }
    })
    .on('focusout', '.out', function() {
        var $row  = $(this).parents('.row'),
        index = $row.attr('data-row-index'),
        valor = parseInt($('.monto'+index).attr('valor')),
        cantidad = parseInt($('.monto'+index).val().replace(/,/g , ""))?parseInt($('.monto'+index).val().replace(/,/g , "")):0,
        inicial = parseInt($('.folioInicial'+index).val().replace(/,/g , ""))-1;

        if (cantidad > 0 && cantidad % valor ==0) {
          $('.monto'+index).val(currency(cantidad,2, [',', ",", '.']));
          $('.cantidad'+index).val(cantidad/valor);
          $('.folioFinal'+index).val(currency(inicial+(cantidad/valor),0, [',', ",", '']));
        }else {
          $('.monto'+index).val('');
          $('.cantidad'+index).val(0);
          $('.folioFinal'+index).val('');
        }

        sumaTotal();

    })
    .on('focus','.out', function(){
        var $row  = $(this).parents('.row'),
        index = $row.attr('data-row-index');
        parseInt($('.monto'+index).val())>0?$('.monto'+index).val(parseInt($('.monto'+index).val().replace(/,/g , ""))):'';
    });

    $('#editarVenta').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: { },
        fields: {
            editFactura: {
              validators: {
                notEmpty: {
                  message: 'Ingrese factura'
                }
              }
            },
            editCliente: {
              validators: {
                notEmpty: {
                  message: 'Seleccione un cliente'
                }
              }
            }
        }
    });


    fields = [];
    close = 0;
    var tapIndex = 4;
    $('#Inputvales').change(function(){
      var text = $("#Inputvales option:selected").text();
      var val = $("#Inputvales option:selected").val();

      if($("#row"+val).length == 0){
        $.ajax({
           type: 'POST',
           url: 'ultimoFolio',
           data:'tipovale='+val+'&opcion=Ventas',
           cache:false,
           dataType:'text'
        }).done(function(data){
          try {
            var data = JSON.parse(data);

              $('#listVales').append(
                '<div class="row" data-row-index="'+val+'" id="row'+val+'">'+
                  '<div class="col-md-3">'+
                      '<div class="bg-info p-xs b-r-xs">'+text+'</div>'+
                      '<input type="hidden" class="form-control" name="Inputvale['+val+'][tipovale]" value="'+val+'">'+
                  '</div>'+
                  '<div class="col-md-2">'+
                      '<input type="text" readonly class="form-control folioInicial'+val+'" name="Inputvale['+val+'][folioInicial]" value="'+currency(data.inicial,0, [',', ",", ''])+'">'+
                  '</div>'+
                  '<div class="col-md-2">'+
                      '<input type="text" readonly class="form-control folioFinal'+val+'" name="Inputvale['+val+'][folioFinal]">'+
                  '</div>'+
                  '<div class="col-md-3">'+
                    '<div class="form-group">'+
                      '<input type="text" class="form-control out monto'+val+'" tabindex="'+tapIndex+'"  valor="'+data.valorVale+'" onkeypress="return isNumber(event);" name="Inputvale['+val+'][monto]">'+
                      '<input type="hidden" class="form-control cantidad'+val+'" valor="0" name="Inputvale['+val+'][cantidad]">'+
                    '</div>'+
                  '</div>'+
                  '<div class="col-md-2">'+
                      '<a data-toggle="tooltip" data-original-title="Eliminar Hoja" href="javascript:eliminarFila('+val+');" class="btn-sm btn-danger fa fa-trash-o" style="margin-top: 5px;margin-left: 5px;"></a>'+
                  '</div>'+
                '</div>'
              )
              tapIndex +=1;
              agregarFields(val,data.valorVale,data.valorMaximo);
              fields.push(val);
              if (fields.length == 1) {
                $('#totalVenta').append(
                    '<div class="row">'+
                      '<div class="col-md-offset-5 col-md-2">'+
                        '<div class="bg-info p-xs b-r-xs">Total</div>'+
                      '</div>'+
                      '<div class="col-md-3">'+
                        '<div class="form-group">'+
                          '<input type="text" readonly class="form-control" id="InputTotal" name="InputTotal" value="0.00">'+
                        '</div>'+
                      '</div>'+
                    '</div>'
                )
              }


          } catch (e) {
            $('#modalsession').append($(data));
          }
        }).fail(function(xhr, status, error) {
            console.log(error);
        });
      }
    });

});

function sumaTotal() {
  var suma = 0;
  if ($('.out').length>0) {
    $('.out').each(function(i,item){
      suma +=  parseInt($(item).val().replace(/,/g , ""))?parseInt($(item).val().replace(/,/g , "")):0;
    });
    $('#InputTotal').val(currency(suma,2, [',', ",", '.']));
  }
}

function agregarFields(index,min,max) {
  var cantidadValidators = {
      validators: {
        callback:{
          callback: function(value, validator, $field) {
              if (parseInt(value.replace(/,/g , "")) % min ==0 && parseInt(value.replace(/,/g , ""))>0) {
                return {
                  valid: true
                }
              }else {
                return {
                  valid: false,
                  message:'Monto invalido'
                }
              }
            }
        },
        /*lessThan: {
            value: max,
            message: 'Disponibles $ '+currency(max,2, [',', ",", '.'])
        },*/
        notEmpty: {
            message: 'Ingrese monto'
        }
      }
  };
  $('#formVenta').formValidation('addField','Inputvale['+index+'][monto]',cantidadValidators);
}

function eliminarFields() {
  if (fields.length >0) {
    $.each(fields,function(i,item){
      console.log($('Inputvale['+parseInt(item)+'][monto]').length);
      //if ($('Inputvale['+parseInt(item)+'][monto]').length>0) {

        $("#formVenta").data('formValidation').removeField('Inputvale['+parseInt(item)+'][monto]');
      //}
    });
    fields = [];
  }
}



function eliminarFila(index) {
    $("#row"+index).remove();
    $("#formVenta").data('formValidation').removeField('Inputvale['+index+'][monto]');
    index = fields.indexOf(index.toString());
    fields.splice(index, 1);
    if (fields.length == 0) {
      $('#totalVenta').text('');
    }
    sumaTotal();
}

$('.toggle-estadoF').on('change',function(e){
  var check = $(this);
  if (check.is(':checked')) {
    $('#InputEstadoFactura').val(1);
  }else {
    $('#InputEstadoFactura').val(0);
  }
});

$(document).on('change',".toggle-estado",function(e){
    var check = $(this);
    var url = $("#base_url").val();
    var row = $(this).closest("tr");
    folio = row.find(".removeTr").text();
    var toggle = check.data('bs.toggle');
  //  alert(check.prop('checked'));

    modalFecha = bootbox.confirm({
        title: "Cambiar Estado",
        message: "¿Desea cambiar el estatus de esta factura?",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar',
                className: 'btn-sm btn-default'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar',
                className: 'btn-sm btn-primary'
            }
        },
        callback: function (result) {
          var estado = 1;
          if (!check.is(':checked')) {
            estado = 2;
          }
          if (!result) {
            if (estado === 1) {
              toggle.off(true);
            }else {
              toggle.on(true);
            }
          }else {
            console.log('error');
          }
          // }else {
          //     $.post(url + "Cortes/Cortes/cambiarEstado",{folio: folio,estado: estado},function(data){
          //       if (!data.error) {
          //         console.log(data);
          //         if (estado === 1) {
          //           row.find('#lblmodificar').removeAttr('style');
          //           row.find('#lblmodificar').addClass('label-primary');
          //           row.find('#lblfecha').removeAttr('style');
          //           row.find('#lblfecha').addClass('label-warning');
          //           row.find('.puntero').removeAttr('style');
          //           row.find(".puntero").css({'margin-left':'1%'});
          //
          //         }else {
          //           row.find('#lblmodificar').removeClass('label-primary');
          //           row.find('#lblmodificar').css({'color':'#fff','background-color':'#74BF8F'});
          //           row.find('#lblfecha').removeClass('label-warning');
          //           row.find('#lblfecha').css({'color':'#fff','background-color':'#F4BF8F'});
          //           row.find(".puntero").css({'pointer-events':'none','cursor':'default'});
          //
          //         }
          //         $.toast({
          //             heading: 'Alerta',
          //             hideAfter: 4000,
          //             position: 'bottom-right',
          //             text: data.msg,
          //             icon: 'success'
          //           });
          //       }else {
          //         if (estado === 1) {
          //           toggle.off(true);
          //         }else {
          //           toggle.on(true);
          //         }
          //         $.toast({
          //           heading: 'Error',
          //           hideAfter: 4000,
          //           position: 'bottom-right',
          //           text: data.msg,
          //           icon: 'error'
          //         });
          //       }
          //     },'json').fail(function (jqXHR, textStatus, errorThrown) {
          //       if (estado === 1) {
          //         toggle.off(true);
          //       }else {
          //         toggle.on(true);
          //       }
          //       $.toast({
          //         heading: 'Error',
          //         hideAfter: 4000,
          //         position: 'bottom-right',
          //         text: errorThrown,
          //         icon: 'error'
          //       });
          //     });
          // }
        }
    });

});


$(document).on('click','.in',function(e){
  var id=e.target.id;
  id=='myModal5'?Animate('modal1','shake'):'';
  id=='myModal'?Animate('modal2','shake'):'';
});

function Animate(modal,x) {
    $('#'+modal).removeClass().addClass(x + ' animated modal-content').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
      $(this).removeClass().addClass('modal-content');
    });
  };


$('#myModal5').on('show.bs.modal', function (e) {
    $('.toggle-estadoF').bootstrapToggle('on');
    eliminarFields();
    fields = [];
    close = 0;
});

$('#myModal5').on('hidden.bs.modal', function (e) {
    eliminarFields();
    $('#totalVenta').text('');
    $('#formVenta')[0].reset();
    $('#InputCliente')
    .find('option:first-child').prop('selected', true)
    .end().trigger('chosen:updated');
    $("#formVenta").data('formValidation').resetForm();
    $('#listVales').text('');

});

$("#formVenta").on('submit',function(e){

  e.preventDefault();

  try {
    $('#btnGuardar').prop('disabled', true);
    var listvales = $('#listVales').html().trim();
    if (listvales!=='') {
      $("#formVenta").data('formValidation').validate();
      var valid = $("#formVenta").data('formValidation').isValid();
      var form = $(this).serialize();
      if (valid) {
        var guardando=bootbox.dialog(
                   {'title':'Guardando.',
                    'message':
                    '<div class="sk-spinner sk-spinner-wave">'+
                          '<div class="sk-rect1" style="margin-right:8%"></div>'+
                          '<div class="sk-rect2" style="margin-right:8%"></div>'+
                          '<div class="sk-rect3" style="margin-right:8%"></div>'+
                          '<div class="sk-rect4" style="margin-right:8%"></div>'+
                          '<div class="sk-rect5"></div>'+
                      '</div>','size':'small',
                    closeButton: false,
                    className: 'scrollhide'
                  }).find('.modal-content').css({'text-align':'center'});
        $.post( "nuevaVenta", form ,function( data ) {
          try {
              var data = JSON.parse(data);
              if (!data.error) {
                $('#myModal5').modal('toggle');
                $('#btnGuardar').prop('disabled', false);
                $('#tb-ventas').DataTable().ajax.reload();

                setTimeout(function(){ location.reload(); }, 3000);


                $.toast({
                  heading: 'Alerta',
                  hideAfter: 4000,
                  position: 'bottom-right',
                  text: data.msg ,
                  icon: 'success'
                });
              } else {
                $('#btnGuardar').prop('disabled', false);
                $.toast({
                  heading: 'Error',
                  hideAfter: 4000,
                  position: 'bottom-right',
                  text: data.msg,
                  icon: 'error'
                });
              }
              bootbox.hideAll();
          } catch (e) {
              bootbox.hideAll();
              $('#modalsession').append($(data));
              $('#btnGuardar').prop('disabled', false);
              // guardando.hide();
          }

        },"text").fail(function(xhr, status, error) {
          $('#btnGuardar').prop('disabled', false);
            bootbox.hideAll();
            $.toast({
              heading: 'Error',
              hideAfter: 4000,
              position: 'bottom-right',
              text: error,
              icon: 'error'
            });
        });
      }else {
        $('#btnGuardar').prop('disabled', false);
        bootbox.hideAll();
      }
    }else {
      $('#btnGuardar').prop('disabled', false);
      bootbox.hideAll();
      $.toast({
        heading: 'Error',
        hideAfter: 4000,
        position: 'bottom-right',
        text: 'Seleccione un tipo de vale.',
        icon: 'error'
      });
    }
  } catch (e) {
    console.log(e.message);
  }
});

$(document).on('click',".imprimirreporte",function(e){
  var $row  = $(this);
  id=$row.attr('id_venta');

  var url = 'ventaPrint' + '?id='+id;
  window.open(url, '_blank');
});

$(document).on('click',".fa-pencil-square-o",function(e){
  var $row  = $(this);
  id=$row.attr('id_venta');
  $('#editID').val(id);
  $.post( "getVenta" , {id:id} ,function( data ) {
    $('#myModal').modal();
    $('#editFactura').val(data.factura);
    $('#editCliente').val(data.cliente);
    $('#editCliente').trigger("chosen:updated");
  },"json").fail(function(xhr, status, error) {
      console.log(error);
      $.toast({
        heading: 'Error',
        hideAfter: 4000,
        position: 'bottom-right',
        text: error,
        icon: 'error'
      });
  });
});

$("#editarVenta").on('submit',function(e){

    e.preventDefault();

    $('#btnEditar').prop('disabled', true);
    $("#editarVenta").data('formValidation').validate();
    var valid = $("#editarVenta").data('formValidation').isValid();
    var form = $(this).serialize();
    if (valid) {
        var guardando=bootbox.dialog(
                   {'title':'Guardando.',
                    'message':
                    '<div class="sk-spinner sk-spinner-wave">'+
                          '<div class="sk-rect1" style="margin-right:8%"></div>'+
                          '<div class="sk-rect2" style="margin-right:8%"></div>'+
                          '<div class="sk-rect3" style="margin-right:8%"></div>'+
                          '<div class="sk-rect4" style="margin-right:8%"></div>'+
                          '<div class="sk-rect5"></div>'+
                      '</div>','size':'small',
                    closeButton: false,
                    className: 'scrollhide'
                  }).find('.modal-content').css({'text-align':'center'});
        $.post( "editarVenta", form ,function( data ) {
          try {
              var data = JSON.parse(data);
              if (!data.error) {
                $('#myModal').modal('toggle');
                $('#btnEditar').prop('disabled', false);
                $('#tb-ventas').DataTable().ajax.reload();
                $.toast({
                  heading: 'Alerta',
                  hideAfter: 4000,
                  position: 'bottom-right',
                  text: 'Venta editada correctamente' ,
                  icon: 'success'
                });
              } else {
                $('#btnEditar').prop('disabled', false);
                $.toast({
                  heading: 'Error',
                  hideAfter: 4000,
                  position: 'bottom-right',
                  text: 'Error al editar venta',
                  icon: 'error'
                });
              }
              bootbox.hideAll();
          } catch (e) {
              bootbox.hideAll();
              $('#modalsession').append($(data));
              $('#btnEditar').prop('disabled', false);
          }

        },"text").fail(function(xhr, status, error) {
          $('#btnEditar').prop('disabled', false);
            bootbox.hideAll();
            $.toast({
              heading: 'Error',
              hideAfter: 4000,
              position: 'bottom-right',
              text: error,
              icon: 'error'
            });
        });
      }else {
        $('#btnEditar').prop('disabled', false);
      }
});



//Validar es numero
function isNumber(e) {
        k = (document.all) ? e.keyCode : e.which;
        if (k==8 || k==0) return true;
        patron = /\d/;
        n = String.fromCharCode(k);
        return patron.test(n);
    }
function currency(value, decimals, separators) {
        decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
        separators = separators || ['.', "'", ','];
        var number = (parseFloat(value) || 0).toFixed(decimals);
        if (number.length <= (4 + decimals))
            return number.replace('.', separators[separators.length - 1]);
        var parts = number.split(/[-.]/);
        value = parts[parts.length > 1 ? parts.length - 2 : 0];
        var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
            separators[separators.length - 1] + parts[parts.length - 1] : '');
        var start = value.length - 6;
        var idx = 0;
        while (start > -3) {
            result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
                + separators[idx] + result;
            idx = (++idx) % 2;
            start -= 3;
        }
        return (parts.length == 3 ? '-' : '') + result;
    }
    $(document).on("click",".eliminarVenta",function(e){
      let datos = {folio: $(this).attr("id_venta")}
      let url = $("#base_url").val() + "RendivalesCredito/Apps/eliminarVenta"

      swal({
          title: "Alerta",
          text: "¿Desea Eliminar Esta Venta?",
          content: "warning",
          closeOnConfirm: false,
          buttons: {
            cancel: "Cancelar",
            catch: {
              text: "Confirmar",
              // closeModal: false,
            }
          }
      }).then(name => {
        if (name !== null) {
          $.post(url,datos,function(data){
            tblVentas.ajax.reload()
            if (data.error) {
              swal("Error!", " " + data.msg, "error");
            }else {
              swal("Exito!", " " + data.msg, "success");
            }
          },'json').fail(function(e,x,error){
            swal("Error!", " " + error, "error");
          });
        }
      });

    })
