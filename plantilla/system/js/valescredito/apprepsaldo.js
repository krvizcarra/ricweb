$(function() {
  var url = $("#base_url").val() + 'RendivalesCredito/Apps/dataSaldos';
  var table = $('#tblSaldos').DataTable({
          ajax: url,
          pageLength: 10,
          order: [[ 0, "desc" ]],
          bLengthChange: false,
          language: {
              "sProcessing":     "Procesando...",
              "sLengthMenu":     "Mostrar _MENU_ registros",
              "sZeroRecords":    "No se encontraron resultados",
              "sEmptyTable":     "Ningún dato disponible en esta tabla",
              "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix":    "",
              "sSearch":         "Buscar:",
              "sUrl":            "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "Último",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
          },
          aoColumnDefs: [
                  { "bVisible": true, "aTargets": [0] },
                  {
                      "bSortable": false
                  }],
                  dom: '<"html5buttons"B>lTfgitp',
                  buttons: [
                      {extend: 'excel', text: 'Expotar Excel', title: 'Rendiapps',className: 'btnexcel'},
                      { extend: 'copy' , text: 'Copiar filas'},
                      {extend: 'pdf', text: 'Expotar PDF', title: 'Rendiapps'},
                      {extend: 'print',text: 'Imprimir'}
                  ]

      });


      // var table =   $('#tblSaldos').dataTable();
      $('#Input1, #Input2 , #Input3').change( function() {
        table.draw();
    });

    // $(".btnexcel").removeClass('buttons-excel buttons-html5 btn btn-default').addClass('btn btn-primary');
  });


$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var val1 = $("#Input1").prop("checked");
        var val2 = $("#Input2").prop("checked");
        var val3 = $("#Input3").prop("checked");
        var dato = data[6].replace(/,/g , "");
        dato = dato.replace(/%/g,"");
        if (val1 || val2 || val3) {
            if (val1){
                if ( dato >= 0 && dato<=39.9999 ){
                    return true;
                }
            }
            if (val2){
                if ( dato >= 40 && dato<=79.9999 ){
                    return true;
                }
            }
            if (val3){
                if ( dato >= 80 ){
                    return true;
                }
            }
            return false;
        } else {
            return true;
        }
    }
);
