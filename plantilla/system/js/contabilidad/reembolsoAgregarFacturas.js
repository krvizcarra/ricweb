
$('#menu_reenv').addClass(" active");

$("#btnEnviarFacturas").on('click',function(e){
  var table = $("table tbody");
  var facturas = {
      base: $("#base").val(),
      factura: []
  };
  var rowcount = $('#tbxmls tr').length;
  if (rowcount > 1) {
    table.find('tr').each(function (i) {
        row = $(this).closest("tr");
        var checked = row.find('.checkfactura').is(':checked');
        var $tds = $(this).find('td');
        if ($tds.eq(8).text() === 'Listo' && checked) {
          facturas.factura.push({
            "id": $tds.eq(0).text(),
            "razon": $tds.eq(1).text(),
            "rfc": $tds.eq(2).text(),
            "serie": $tds.eq(3).text(),
            "folio": $tds.eq(4).text(),
            "total": $tds.eq(5).text().replace("$",""),
            "tipogasto": row.find('.tipogasto').val(),
            "servicio": row.find('.tiposervicio').val(),
            "estatus": $tds.eq(8).text(),
            "transladado": $tds.eq(10).text(),
            "version": $tds.eq(12).text(),
            "fecha": $tds.eq(13).text(),
            "subtotal": $tds.eq(14).text(),
            "descuento": $tds.eq(15).text(),
            "tipocomprobante": $tds.eq(16).text(),
            "uuid": $tds.eq(17).text(),
            "id_proveedor": $tds.eq(18).text(),
            "retencion1": $tds.eq(19).text(),
            "retencion2": $tds.eq(20).text(),
            "checked": checked
          });
        }
    });
    if (facturas.factura.length > 0 && $('.checkfactura:checked').length) {
      $('#modal1').children('.ibox-content').toggleClass('sk-loading');
      $('#myModal5').modal('toggle');
      $.post($("#base_url").val()+'Contabilidad/Apps/registrarFacturas',facturas,function(data){
        try {
          var obj = JSON.parse(data);
          if (obj.error) {
            $('#myModal5').modal('toggle');
            $.toast({
              heading: 'Error',
              hideAfter: 4000,
              position: 'bottom-right',
              text:obj.msg,
              icon: 'error'
            });
          }else {
            $.each(JSON.parse(obj.msg),function(i,item){
              if (item.respuesta != 1) {
                $(".uid").each(function(e,elemnt){
                  if ($(elemnt).text() === item.uuid) {
                    var row = $(elemnt).closest("tr");
                    var tag = row.find('.etiqueta');
                    row.find('.msg').text(item.mensaje);
                    tag.removeAttr('class');
                    tag.attr('class','label label-danger etiqueta');
                    tag.text('Error');
                  }
                });
              }else {
                $(".uid").each(function( i,it){
                  if ($(it).text() == item.uuid) {
                    var row = $(it).closest("tr");
                    row.remove();
                  }
                });
                $.toast({
                      heading: 'Aviso',
                      hideAfter: 4000,
                      position: 'bottom-right',
                      text:"Facturas Enviadas",
                      icon: 'success'
                    });
              }
            });
            $("#archivos").val('');
            $('#myModal5').modal('toggle');
          }
        } catch (e) {
          $('#myModal5').modal('toggle');
          if (data.indexOf("estoesunmodal") > -1) {
            $("#divappend").append(data);
          }
        }
      },"text").fail(function( jqXHR, textStatus, errorThrown ) {
          $('#myModal5').modal('toggle');
          console.log(errorThrown);
       });
    }else {
      $.toast({
            heading: 'Error',
            hideAfter: 4000,
            position: 'bottom-right',
            text:"No hay facturas listas para enviar",
            icon: 'error'
          });
    }
  }else {
    $.toast({
          heading: 'Error',
          hideAfter: 4000,
          position: 'bottom-right',
          text:"Agregar facturas",
          icon: 'error'
        });
  }
});

$("#tbxmls tbody").on('change','tr .tipogasto',function(e){
  var row = $(this).closest("tr");
  var select = row.find('.tiposervicio').val()
  var msg = row.find('.msg').text()
  console.log("-> "+msg);
  if (select != null && msg !== 'Factura no corresponde a esta empresa') {
    var tag = row.find('.etiqueta');
    var msg = row.find('.msg');
    tag.removeAttr('class');
    tag.attr('class','label label-primary etiqueta');
    tag.text('Listo');
    msg.text('Listo para enviar');
  }
});

$("#tbxmls tbody").on('change','tr .tiposervicio',function(e){
  var row = $(this).closest("tr");
  var select = row.find('.tipogasto').val();
  var msg = row.find('.msg').text()
  console.log("-> "+msg);
  if (select != null && msg !== 'Factura no corresponde a esta empresa') {
    var tag = row.find('.etiqueta');
    var msg = row.find('.msg');
    tag.removeAttr('class');
    tag.attr('class','label label-primary etiqueta');
    tag.text('Listo');
    msg.text('Listo para enviar');
  }
});

function readfile(){

  if($("#archivos").get(0).files.length > 0){
    $('#modal1').children('.ibox-content').toggleClass('sk-loading');
    $('#myModal5').modal('toggle');
      var form = document.getElementById("formulario");
      var formData = new FormData($(form)[0]);
      $.ajax({
          type: $(form).attr("method"),
          url: $(form).attr("action"),
          data:formData,
          cache:false,
          contentType: false,
          processData:false
          }).done(function(data){

            $('#myModal5').modal('toggle');
            if(data != null ){
              document.getElementById("facturas").innerHTML = data;
            }

          }).fail(function(e,r) {
            $('#myModal5').modal('toggle');
              console.log(e);
          });

      }
}


$(document).on('click',".verconceptos",function(e) {
  console.log("entre");
  var row = $(this).closest("tr");
  var conceptos = row.find(".conceptos").text();
  try {
    $(".fila").remove();
    var obj = JSON.parse(conceptos);
    $.each(obj,function(i,item){
      var html = '<tr class="fila">'+
                      '<td>'+item.noIdentificacion[0]+'</td>'+
                      '<td>'+item.descripcion[0]+'</td>'+
                      '<td>'+item.valorUnitario[0]+'</td>'+
                      '<td>'+item.importe[0]+'</td>'+
                      '<td>'+item.unidad[0]+'</td>'+
                      '<td>'+item.cantidad[0]+'</td>'+
                   '</tr>';
        $("#tableConceptos").append(html);
    });

    bootbox.alert({'title':'Conceptos de la factura','message':$("#tb-conceptos").html(),'size': "large"});
  } catch (e) {
    console.log(e);
  }

});

function currency(value, decimals, separators) {
    decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
    separators = separators || ['.', "'", ','];
    var number = (parseFloat(value) || 0).toFixed(decimals);
    if (number.length <= (4 + decimals))
        return number.replace('.', separators[separators.length - 1]);
    var parts = number.split(/[-.]/);
    value = parts[parts.length > 1 ? parts.length - 2 : 0];
    var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
        separators[separators.length - 1] + parts[parts.length - 1] : '');
    var start = value.length - 6;
    var idx = 0;
    while (start > -3) {
        result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
            + separators[idx] + result;
        idx = (++idx) % 2;
        start -= 3;
    }
    return (parts.length == 3 ? '-' : '') + result;
}
