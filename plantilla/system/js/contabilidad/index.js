$("#tb-sucursales tbody").on('click','tr .seleccionarempresa',function(e){
  var row = $(this).closest("tr");
  var datos = {
    ide: row.find(".ide").text(),
    razon: row.find(".razon").text(),
    rfc: row.find(".rfc").text(),
    base: row.find(".base").text(),
    direccion: row.find(".direccion").text()
  }

  $.post($("#base_url").val() + "Contabilidad/Apps/seleccionarEmpresa",datos,function(data) {
    try {
      var obj = JSON.parse(data);
      if (obj.error) {
        $.toast({
          heading: 'Error',
          hideAfter: 4000,
          position: 'bottom-right',
          text: obj.msg,
          icon: 'error'
        });
      }else {
       window.location.href = obj.msg;
      }
    } catch (e) {
      if (data.indexOf("estoesunmodal") > -1) {
        $("#divappend").append(data);
      }
    }

  },'text').fail(function( jqXHR, textStatus, errorThrown ) {
    $.toast({
      heading: 'Error',
      hideAfter: 4000,
      position: 'bottom-right',
      text: errorThrown,
      icon: 'error'
    });
  });
});


  $('#tb-sucursales').DataTable( {
        data: adata,
        columns: [
            { title: "#" , className: "ide"},
            { title: "Razón Social" , className: "razon"},
            { title: "RFC" , className: "rfc"},
            { title: "Base", className: "hidden base" },
            { title: "Sucursal", className: "direccion" },
            { title: "Opción"},
        ],

        responsive: true,
        bLengthChange: false,
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        columnDefs: [ {
            "targets": -1,
            "data": null,
            "sClass": "text-center",
            "defaultContent": "<button type='button' class='btn btn-primary btn-xs seleccionarempresa'>Elegir</i></button><button type='button' style='margin-left:2%' class='btn btn-warning btn-xs editarEmpresa'>Editar</i></button>"
        } ],
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            { extend: 'copy'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},

            {extend: 'print',
             customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
            }
            }
        ]
    } );

    var sucursal = '';
    var editable = true;
    $("#tb-sucursales tbody").on('click','tr .editarEmpresa',function(e){
        var row = $(this).closest("tr");
        var direccion = row.find(".direccion");
        if (!$(this).hasClass("confirmarEmpresa")) {
          $(".confirmarEmpresa").each(function(i,item){
            editable = false
          });
          if (editable) {
            sucursal = direccion.text();
            $(direccion).attr("contenteditable",true);
            $(direccion).css({"border": "1px solid #3EC114"});
            $(direccion).focus();
            $(this).removeClass('btn btn-warning btn-xs');
            $(this).text("Guardar");
            $(this).addClass("btn btn-info btn-xs confirmarEmpresa");
          }
        }else {
          $(direccion).attr("contenteditable",false);
          $(direccion).removeAttr("style");
          $(this).removeClass('btn btn-info btn-xs confirmarEmpresa');
          $(this).text("Editar");
          $(this).addClass("btn btn-warning btn-xs");
        }

    });

    $("#tb-sucursales tbody").on('click','tr .confirmarEmpresa',function(e){
      editable = true;
      var row = $(this).closest("tr");
      var direccion = row.find(".direccion");
      var ide = row.find(".ide");
      var url = $("#base_url").val() + "Contabilidad/Apps/editarDireccionEmpresa";
      if(sucursal != direccion.text()){
        var datos = {direccion: direccion.text(), ide: ide.text()};
        $.post(url,datos,function (data) {
          try {
            var obj = JSON.parse(data);
            if (obj.error) {
              $.toast({
                heading: 'Error',
                hideAfter: 4000,
                position: 'bottom-right',
                text: obj.msg,
                icon: 'error'
              });
            }else {
              $.toast({
                heading: 'Aviso',
                hideAfter: 4000,
                position: 'bottom-right',
                text: obj.msg,
                icon: 'success'
              });
            }
          } catch (e) {
            if (data.indexOf("estoesunmodal") > -1) {
              $("#divappend").append(data);
            }
          }
        },"text").fail(function( jqXHR, textStatus, errorThrown ) {
          console.log(errorThrown);
        });
      }
    });
