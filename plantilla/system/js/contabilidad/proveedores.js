
$('#menu_prov_gasto').addClass(" active");
$('#listado').bootstrapDualListbox({
                selectorMinimalHeight: 160,
            });
$(document).ready(function(){

            $('.dataTables-example').DataTable({
                pageLength: 10,
                ajax: $("#base_url").val()+'Contabilidad/Apps/obtenerProvedores',
                responsive: true,
                bLengthChange: false,
                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                columnDefs: [ {
                    "targets": -1,
                    // "data": null,
                    "sClass": "text-center",

                } ],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

            $("#tblProveedores tbody").on('click','tr .opciones',function(e){
                $(".filter").val('');
                $('.btn-modal').prop('disabled', true);
                var row = $(this);
                var id = row.attr('idProveedor');
                var rfc = row.attr('rfcProveedor');
                var url = row.attr('url');
                var title = row.attr('title');
                var subtitle = row.attr('subtitle');
                var opcion = row.attr('opcion');
                $('#idProveedor').val(id);
                $('#rfcProveedor').val(rfc);
                $('#opcion').val(opcion);
                $('#modal-title').text(title);
                $('#modal-subtitle').text(subtitle);
                $('#listado').text('');
                $('.sk-spinner').css({'display':'block'});
                $('.bootstrap-duallistbox-container').css({'display':'none'});
                $.ajax({
                     type: 'POST',
                     url: url,
                     data:'idProveedor='+id,
                     cache:false,
                }).done(function(data){
                    $('#listado').append(data);
                    $('#listado').bootstrapDualListbox('refresh', true);
                    $('.bootstrap-duallistbox-container').css({'display':'block'});
                    $('.sk-spinner').css({'display':'none'});
                    $('.btn-modal').prop('disabled', false);
                });
            });

});

$("#btnGuardarServicios").click(function(e){

    var selected=[];
    $('#listado :selected').each(function(i,item){
      // console.log($(item).val());
      if ($(item).text() != '') {
        var sel = {
           'id': $(item).val(),
           'text': $(item).text()
         }
          selected.push(sel);
      }

    });
    var datos={
      'idProveedor':$('#idProveedor').val(),
      'rfcProveedor':$('#rfcProveedor').val(),
      'opcion':$('#opcion').val(),
      'seleccion':selected
    };

// console.log(datos);
    console.log(selected);
    $('.btn-modal').prop('disabled', true);
    $('.sk-spinner').css({'display':'block'});
    $('#modal1').children('.ibox-content').toggleClass('sk-loading');

    e.preventDefault();

    $.ajax({
         type: 'POST',
         url: $("#base_url").val()+'Contabilidad/Apps/guardar_listado',
         data: datos,
         cache:false,
         dataType: 'json',
    }).done(function(data){
      console.log(data);
        $('.sk-spinner').css({'display':'none'});
        $('#modal1').children('.ibox-content').toggleClass('sk-loading');
        $('#myModal5').modal('toggle');
        if (!data.error) {
          $.toast({
            heading: 'Aviso',
            hideAfter: 4000,
            position: 'bottom-right',
            text: data.msg,
            icon: 'success'
          });
        }else {
          $.toast({
            heading: 'Error',
            hideAfter: 4000,
            position: 'bottom-right',
            text: data.msg,
            icon: 'error'
          });
        }
    }).fail(function( jqXHR, textStatus, errorThrown ) {
      $.toast({
        heading: 'Error',
        hideAfter: 4000,
        position: 'bottom-right',
        text: textStatus,
        icon: 'error'
      });
    });
});
