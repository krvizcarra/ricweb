$.fn.dataTable.moment('DD-MM-YYYY HH:mm');
preciotb = $('#tb-preciosc').DataTable( {
      // data: adata,
      ajax: $("#base_url").val() + 'Precios/Apps/dataProximosPreciosPemex',
      // id, fecha, permiso, magna, premium, diesel, magnaneto, premiumneto, dieselneto, magnabruto, premiumbruto, dieselbruto, magnaproximo, premiumproximo, dieselproximo, aplicado, idradec
      columns: [
          { title: "FECHA"},
          { title: "ESTACION"},
          { title: "MAGNA",className:'verde'},
          { title: "NETA",className:'verde'},
          { title: "BRUTA",className:'verde'},
          { title: "PREMIUM",className:'rojo'},
          { title: "NETA",className:'rojo'},
          { title: "BRUTA",className:'rojo'},
          { title: "DIESEL",className:'negro' },
          { title: "NETA",className:'negro'},
          { title: "BRUTA",className:'negro'},
          { title: "OPCIONES",className:'dt-center'},
          { title: "RADEC",className:'hidden'},
          { title: "CONTROL",className:'hidden'}
      ],
      order: [ [0, 'desc'] ],
      responsive: true,
      bLengthChange: false,
      language: {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },

      dom: '<"html5buttons"B>lTfgitp',
      buttons: [
          { extend: 'copy'},
          {extend: 'excel', title: 'ExampleFile'},
          {extend: 'pdf', title: 'ExampleFile'},

          {extend: 'print',
           customize: function (win){
                  $(win.document.body).addClass('white-bg');
                  $(win.document.body).css('font-size', '10px');

                  $(win.document.body).find('table')
                          .addClass('compact')
                          .css('font-size', 'inherit');
          }
          }
      ],
      fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          if ( aData[12] == 0 && aData[13] == 0 )
          {
              $('td', nRow).css('background-color', '#F78181');
              $('td', nRow).css('color', 'white');

          }else {
            $('td', nRow).css('background-color', '#298A08');
            $('td', nRow).css('color', 'white');
          }
      }
  });


$('#tb-preciosc tbody').on( 'click', 'tr .btnEliminarPrecio', function (){
  var row = $(this).closest("tr");
  var url = $("#base_url").val() + 'Precios/Apps/eliminarProximoPrecioPemex';
  var datos = {
      folio: $(this).attr("folio"),
      idradec: $(this).attr("idradec"),
      controlgas: $(this).attr("controlgas"),
      region: $(this).attr("region"),
      fecha: $(this).attr("fecha"),
      permiso: $(this).attr("permiso")

    };
  swal({
      text: "¿Desea eliminar cambio de precio la estacion "+datos.region+" del dia "+datos.fecha+"?",
      content: "warning",
      closeOnConfirm: false,
      buttons: {
        cancel: "Cancelar",
        catch: {
          text: "Confirmar",
        }
      }
  }).then(name => {
    if (name !== null) {
      $(".btn-click").attr("disabled","true");
      var modal = bootbox.dialog({'title':'Cargando','message':
                  '<div class="sk-spinner sk-spinner-wave">'+
                        '<div class="sk-rect1" style="margin-right:8%"></div>'+
                        '<div class="sk-rect2" style="margin-right:8%"></div>'+
                        '<div class="sk-rect3" style="margin-right:8%"></div>'+
                        '<div class="sk-rect4" style="margin-right:8%"></div>'+
                        '<div class="sk-rect5"></div>'+
                    '</div>','size':'small',
                  closeButton: false,
                  className: 'scrollhide'
                });
      $.post(url,datos,function(data){
        $(".btn-click").removeAttr("disabled");
        modal.modal('hide');
        preciotb.ajax.reload();
        if (data.error) {
          swal("Error!", " " + data.msg, "error");
        }else {
          swal("Exito!", " " + data.msg, "success");
        }
      },'json').fail(function(e,x,error){
        $(".btn-click").removeAttr("disabled");
        modal.modal('hide');
        swal("Error!", " " + error, "error");
      });
    }
  });
})

$('#tb-preciosc tbody').on( 'click', 'tr .btnEnviarPrecio', function (){
  var row = $(this).closest("tr");
  var url = $("#base_url").val() + 'Precios/Apps/enviarProximoPrecioPemex';
  var datos = {
      folio: $(this).attr("folio"),
      region: $(this).attr("region"),
      fecha: $(this).attr("fecha")
    };
  swal({
      text: "¿Desea reenviar el cambio de precio la estacion "+datos.region+" del dia "+datos.fecha+"?",
      content: "warning",
      closeOnConfirm: false,
      buttons: {
        cancel: "Cancelar",
        catch: {
          text: "Confirmar",
        }
      }
  }).then(name => {
    if (name !== null) {
      $(".btn-click").attr("disabled","true");
      var modal = bootbox.dialog({'title':'Cargando','message':
                  '<div class="sk-spinner sk-spinner-wave">'+
                        '<div class="sk-rect1" style="margin-right:8%"></div>'+
                        '<div class="sk-rect2" style="margin-right:8%"></div>'+
                        '<div class="sk-rect3" style="margin-right:8%"></div>'+
                        '<div class="sk-rect4" style="margin-right:8%"></div>'+
                        '<div class="sk-rect5"></div>'+
                    '</div>','size':'small',
                  closeButton: false,
                  className: 'scrollhide'
                });
      $.post(url,datos,function(data){
        $(".btn-click").removeAttr("disabled");
        modal.modal('hide');
        preciotb.ajax.reload();
        if (data.error) {
          swal("Error!", " " + data.msg, "error");
        }else {
          swal("Exito!", " " + data.msg, "success");
        }
      },'json').fail(function(e,x,error){
        $(".btn-click").removeAttr("disabled");
        modal.modal('hide');
        swal("Error!", " " + error, "error");
      });
    }
  });
})

function numero(campo) {
    campo.value   = campo.value.replace(/,/g , "");
}

function formatnum(campo) {
    campo.value = currency(campo.value,8, [',', ",", '.']);
}

function currency(value, decimals, separators) {
    decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
    separators = separators || ['.', "'", ','];
    var number = (parseFloat(value) || 0).toFixed(decimals);
    if (number.length <= (4 + decimals))
        return number.replace('.', separators[separators.length - 1]);
    var parts = number.split(/[-.]/);
    value = parts[parts.length > 1 ? parts.length - 2 : 0];
    var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
        separators[separators.length - 1] + parts[parts.length - 1] : '');
    var start = value.length - 6;
    var idx = 0;
    while (start > -3) {
        result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
            + separators[idx] + result;
        idx = (++idx) % 2;
        start -= 3;
    }
    return (parts.length == 3 ? '-' : '') + result;
}
