$.fn.dataTable.moment('DD-MM-YYYY HH:mm');
preciotb = $('#tb-preciosc').DataTable( {
      // data: adata,
      ajax: $("#base_url").val() + 'Precios/Apps/dataProximosPrecios',
      // id, fecha, permiso, magna, premium, diesel, magnaneto, premiumneto, dieselneto, magnabruto, premiumbruto, dieselbruto, magnaproximo, premiumproximo, dieselproximo, aplicado, idradec
      columns: [
          { title: "FECHA"},
          { title: "REGION"},
          { title: "MAGNA",className:'verde'},
          { title: "PREMIUM",className:'rojo'},
          { title: "DIESEL",className:'negro' },
          { title: "OPCIONES",className:'dt-center'}
      ],
      order: [ [0, 'desc'] ],
      responsive: true,
      bLengthChange: false,
      language: {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },

      dom: '<"html5buttons"B>lTfgitp',
      buttons: [
          { extend: 'copy'},
          {extend: 'excel', title: 'ExampleFile'},
          {extend: 'pdf', title: 'ExampleFile'},

          {extend: 'print',
           customize: function (win){
                  $(win.document.body).addClass('white-bg');
                  $(win.document.body).css('font-size', '10px');

                  $(win.document.body).find('table')
                          .addClass('compact')
                          .css('font-size', 'inherit');
          }
          }
      ]
  });

$("#btn-nvo").on('click',function(e){
  var url = $("#base_url").val() + 'Precios/Apps/frmProximosPrecio';
  $.get(url,{},function(data){
    bootbox.confirm({
      title: 'Cambio de precios',
      message: $(data),
      size: 'large',
      buttons: {
          confirm: {
              label: 'Guardar',
              className: 'btn-primary'
          },
          cancel: {
              label: 'Cancelar',
              className: 'btn-default'
          }
      },
      callback: function (result) {
        if (result) {
          $("#frmProximoPrecio").submit();
          return false;
        }
      }
    })
  })
});


$('#tb-preciosc tbody').on( 'click', 'tr .btnEliminarPrecio', function (){
  var row = $(this).closest("tr");
  var url = $("#base_url").val() + 'Precios/Apps/eliminarProximoPrecio';
  var datos = {
      fecha: $(this).attr("fecha"),
      region: $(this).attr("region"),
      folio: $(this).attr("folio")
    };
  swal({
      text: "¿Desea eliminar cambio de precio la region "+datos.region+" del dia "+datos.fecha+"?",
      content: "warning",
      closeOnConfirm: false,
      buttons: {
        cancel: "Cancelar",
        catch: {
          text: "Confirmar",
        }
      }
  }).then(name => {
    if (name !== null) {
      $.post(url,datos,function(data){
        preciotb.ajax.reload();
        if (data.error) {
          swal("Error!", " " + data.msg, "error");
        }else {
          swal("Exito!", " " + data.msg, "success");
        }
      },'json').fail(function(e,x,error){
        swal("Error!", " " + error, "error");
      });
    }
  });
})

$(document).on("click",".btnEditarPrecio",function(e){
  var url = $("#base_url").val() + 'Precios/Apps/editarProximoPrecio';
  var datos = {
    folio: $(this).attr("folio"),
    fecha: $(this).attr("fecha"),
    region: $(this).attr("region"),
    razon: $(this).attr("razon"),
    magna: $(this).attr("magna"),
    premium: $(this).attr("premium"),
    diesel: $(this).attr("diesel")
  };
  bootbox.confirm({
      title: "¿Desea editar estos precios?",
      message: $(frmeditar(datos.magna,datos.premium,datos.diesel,datos.region,datos.folio,datos.razon)),
      buttons: {
          confirm: {
              label: 'Confirmar',
              className: 'btn-primary btn-sm'
          },
          cancel: {
              label: 'Cancelar',
              className: 'btn-default btn-sm'
          }
      },
      callback: function (result) {
        if (result) {
          let form = $("#frmeditarprecio").serialize();

          swal({
              title: "Alerta",
              text: "¿Desea Editar Este Precio?",
              content: "warning",
              closeOnConfirm: false,
              buttons: {
                cancel: "Cancelar",
                catch: {
                  text: "Confirmar",
                  // closeModal: false,
                }
              }
          }).then(name => {
            if (name !== null) {
              $.post( url,form, function( data ) {
                 preciotb.ajax.reload();
                 if (data.error) {
                   swal("Error!", data.msg, "error");
                 }else {
                   bootbox.hideAll();
                   swal("Exito!", data.msg, "success");
                 }
               },'json').fail(function(e,x,error){
                 swal("Error!", error, "error");
               });
            }
          });
         return false;
        }
      }
  })

  $('.fechadt').datetimepicker({
    format:'DD-MM-YYYY',
    date: new Date(datos.fecha)
  });
});


function frmeditar($magna,$premium,$diesel,$region,$folio,$razon) {
  let html = '<form id="frmeditarprecio" role="form"><div class="row">'+
    '<div class="col-lg-12 col-md-12 col-sm-12 ">'+
        '<div class="col-lg-6 col-md-6 col-sm-6 form-group" style="text-align:right;">'+
        '<div class="inputGroupContainer  " >'+
            '<div class="input-group fechadt"> <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>'+
              '<input type="text" style="text-align:center;" name="txtfecha"  id="txtfecha" class="form-control" readonly>'+
          '</div>'+
          '</div>'+
        '</div>'+

        '<div class="col-lg-6 col-md-6 col-sm-6 form-group" style="text-align:right;">'+
        '<div class="inputGroupContainer  " >'+
            '<div class="input-group "> <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>'+
              '<input type="hidden" style="text-align:center;" name="txtregion"  id="txtregion"  value="'+$region+'" >'+
              '<input type="text" style="text-align:center;" id="txtrazon" value="'+$razon+'" class="form-control" readonly>'+
          '</div>'+
          '</div>'+
        '</div>'+
        '<input type="hidden" name="folio"  id="folio" value="'+$folio+'" />'+
        '<div class="col-lg-4 col-md-4 col-sm-4 form-group">'+
          '<div class="input-group date">'+
              '<input type="text" id="pmagna" name="pmagna" class="form-control borde inputs" onfocus="numero(this);"  onfocusout="formatnum(this);" value="'+$magna+'" />'+
              '<span class="input-group-addon borde" style="background-color:green;color:white;">'+
                  '<span class="glyphicon glyphicon-tint"></span>'+
              '</span>'+
          '</div>'+
        '</div>'+

        '<div class="col-lg-4 col-md-4 col-sm-4 form-group">'+
          '<div class="input-group date">'+
              '<input type="text" id="ppremium" name="ppremium" class="form-control borde inputs" onfocus="numero(this);"  onfocusout="formatnum(this);" value="'+$premium+'" />'+
              '<span class="input-group-addon borde" style="background-color:red;color:white;">'+
                  '<span class="glyphicon glyphicon-tint"></span>'+
              '</span>'+
          '</div>'+
        '</div>'+

        '<div class="col-lg-4 col-md-4 col-sm-4 form-group">'+
          '<div class="input-group date">'+
              '<input type="text" id="pdiesel" name="pdiesel" class="form-control borde inputs" onfocus="numero(this);"  onfocusout="formatnum(this);" value="'+$diesel+'" />'+
              '<span class="input-group-addon borde" style="background-color:black;color:white;">'+
                  '<span class="glyphicon glyphicon-tint"></span>'+
              '</span>'+
          '</div>'+
        '</div>'+

      '</div>'+
    '</div></form>'

  return html;
}

function numero(campo) {
    campo.value   = campo.value.replace(/,/g , "");
}

function formatnum(campo) {
    campo.value = currency(campo.value,8, [',', ",", '.']);
}

function currency(value, decimals, separators) {
    decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
    separators = separators || ['.', "'", ','];
    var number = (parseFloat(value) || 0).toFixed(decimals);
    if (number.length <= (4 + decimals))
        return number.replace('.', separators[separators.length - 1]);
    var parts = number.split(/[-.]/);
    value = parts[parts.length > 1 ? parts.length - 2 : 0];
    var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
        separators[separators.length - 1] + parts[parts.length - 1] : '');
    var start = value.length - 6;
    var idx = 0;
    while (start > -3) {
        result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
            + separators[idx] + result;
        idx = (++idx) % 2;
        start -= 3;
    }
    return (parts.length == 3 ? '-' : '') + result;
}

$("#btn-pdf").on("click",function(e){
  $('#modal1').children('.ibox-content').toggleClass('sk-loading');
  $('#myModal5').modal('toggle');
  $.post($("#base_url").val()+"Precios/Apps/consultarPdfPemex",{},function(data){
    $('#myModal5').modal('toggle');
    preciotb.ajax.reload();
    if (!data.error) {
      swal("Exito!", "Precios consultados correctamente.", "success");
    }else {
      swal("Error!", " "+data.msg, "error");
    }
  },'json').fail(function(jqXHR, textStatus, errorThrown) {
    $('#myModal5').modal('toggle');
    swal("Error!", " " + errorThrown, "error");

  });
})
