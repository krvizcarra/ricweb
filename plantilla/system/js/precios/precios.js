tblUsuarios = $('#tb-precios').DataTable( {
      data: adata,
      columns: [
          { title: "REGION" , className: "removeTr tamano","width": "10%"},
          { title: "FECHA" , className: "tamano","width": "10%"},
          { title: "VENTA MAGNA" , className: "verde tamano","width": "8%"},
          { title: "FACT. MAGNA" , className: "verde tamano","width": "8%"},
          { title: "DIFERENCIA" , className: "tamano","width": "8%"},
          { title: "VENTA PREMIUM",className: "rojo tamano","width": "8%"},
          { title: "FACT. PREMIUM",className: "rojo tamano","width": "8%"},
          { title: "DIFERENCIA",className: "tamano","width": "8%"},
          { title: "VENTA DIESEL",className: "negro tamano","width": "8%"},
          { title: "FACT. DIESEL",className: "negro tamano","width": "8%"},
          { title: "DIFERENCIA",className: "tamano","width": "8%"}
      ],
      order: [ 1, 'desc' ],
      responsive: true,
      bLengthChange: false,
      language: {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },

      dom: '<"html5buttons"B>lTfgitp',
      buttons: [
          { extend: 'copy'},
          {extend: 'excel', title: 'ExampleFile'},
          {extend: 'pdf', title: 'ExampleFile'},

          {extend: 'print',
           customize: function (win){
                  $(win.document.body).addClass('white-bg');
                  $(win.document.body).css('font-size', '10px');

                  $(win.document.body).find('table')
                          .addClass('compact')
                          .css('font-size', 'inherit');
          }
          }
      ]
  });

$("#btn-sync").on("click",function(e){
  $('#modal1').children('.ibox-content').toggleClass('sk-loading');
  $('#myModal5').modal('toggle');
  $.post('https://rendiapps.ddns.net/Pemex/pemexcurl.php',{},function(data){
    $('#myModal5').modal('toggle');
    // console.log(data);
    if (data.length > 0) {
      $.toast({
            heading: 'Alerta',
            hideAfter: 4000,
            position: 'bottom-right',
            text: "Precios consultados correctamente.",
            icon: 'success'
          });
          $.each(data,function(i,item){
            // console.log(item);
            let difmagna = '';
            let difpremium = '';
            let difdiesel = '';
            //MAGNA
            if (item.difmagna > 0) {
              difmagna = '$'+item.difmagna.toFixed(5)+'<i class="fa fa-arrow-up verde"></i>';
            }else if (item.difmagna < 0){
              difmagna = '$'+item.difmagna.toFixed(5)+'<i class="fa fa-arrow-down rojo"></i>';
            }else {
              difmagna = '$'+item.difmagna.toFixed(5);
            }
            //PREMIUM
            if (item.difpremium > 0) {
              difpremium = '$'+item.difpremium.toFixed(5)+'<i class="fa fa-arrow-up verde"></i>';
            }else if (item.difpremium < 0){
              difpremium = '$'+item.difpremium.toFixed(5)+'<i class="fa fa-arrow-down rojo"></i>';
            }else {
              difpremium = '$'+item.difpremium.toFixed(5);
            }
            if (item.difdiesel > 0) {
              difdiesel = '$'+item.difdiesel.toFixed(5)+'<i class="fa fa-arrow-up verde"></i></b>';
            }else if (item.difdiesel < 0){
              difdiesel = '$'+item.difdiesel.toFixed(5)+'<i class="fa fa-arrow-down rojo"></i></b>';
            }else {
              difdiesel = '$'+item.difdiesel.toFixed(5);
            }

            $(".tfecha").text(item.fecha);
            if (item.region === 13) {
              $("#cmagna").text("$"+item.precio_fac_magna);
              $("#cpremium").text("$"+item.precio_fac_premium);
              $("#cdiesel").text("$"+item.precio_fac_diesel);
              $("#cmagnav").text("$"+item.precio_ven_magna);
              $("#cpremiumv").text("$"+item.precio_ven_premium);
              $("#cdieselv").text("$"+item.precio_ven_diesel);
              $("#cmagnad").html(difmagna);
              $("#cpremiumd").html(difpremium);
              $("#cdieseld").html(difdiesel);
            }else if (item.region === 20) {
              $("#gmagna").text("$"+item.precio_fac_magna);
              $("#gpremium").text("$"+item.precio_fac_premium);
              $("#gdiesel").text("$"+item.precio_fac_diesel);
              $("#gmagnav").text("$"+item.precio_ven_magna);
              $("#gpremiumv").text("$"+item.precio_ven_premium);
              $("#gdieselv").text("$"+item.precio_ven_diesel);
              $("#gmagnad").html(difmagna);
              $("#gpremiumd").html(difpremium);
              $("#gdieseld").html(difdiesel);
            }else if (item.region === 35) {
              $("#mmagna").text("$"+item.precio_fac_magna);
              $("#mpremium").text("$"+item.precio_fac_premium);
              $("#mdiesel").text("$"+item.precio_fac_diesel);
              $("#mmagnav").text("$"+item.precio_ven_magna);
              $("#mpremiumv").text("$"+item.precio_ven_premium);
              $("#mdieselv").text("$"+item.precio_ven_diesel);
              $("#mmagnad").html(difmagna);
              $("#mpremiumd").html(difpremium);
              $("#mdieseld").html(difdiesel);
            }else if (item.region === 55) {
              $("#tmagna").text("$"+item.precio_fac_magna);
              $("#tpremium").text("$"+item.precio_fac_premium);
              $("#tmagnav").text("$"+item.precio_ven_magna);
              $("#tpremiumv").text("$"+item.precio_ven_premium);
              $("#tmagnad").html(difmagna);
              $("#tpremiumd").html(difpremium);
              if (item.precio_fac_diesel == 0) {
                $("#tdiesel").text("$0.00000");
                $("#tdieseld").text("$0.00000");
              }else {
                $("#tdiesel").text("$"+item.precio_fac_diesel);
                $("#tdieselv").text("$"+item.precio_ven_diesel);
                $("#tdieseld").html(difdiesel);
              }
            }
          })
          bootbox.alert(
            {'message':$("#tb-ultimosprecios"),
            'title':'Ultimos precios publicados.',
            closeButton: false,
            size: 'large',
            // callback: function(result){ /* result is a boolean; true = OK, false = Cancel*/ }
              callback: function(result) {
                window.setTimeout(function(){ location.reload()}, 1000);
              }
          });
    }else {
      $.toast({
            heading: 'Error',
            hideAfter: 4000,
            position: 'bottom-right',
            text: "Error al consultar precios.",
            icon: 'error'
          });
    }
  },'json').fail(function(jqXHR, textStatus, errorThrown) {
    console.log(jqXHR);
    $('#myModal5').modal('toggle');
    $.toast({
          heading: 'Error',
          hideAfter: 4000,
          position: 'bottom-right',
          text:errorThrown,
          icon: 'error'
        });
  });
})
