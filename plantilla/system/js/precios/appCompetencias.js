$("#menu_competencias").addClass(" active");
$("#menu_precios_competencias").addClass(" active");
$("#Empresas").select2();

function comparar(precio, precio2){
   var resta = precio-precio2;
   var result = "";
   switch (true) {
    case (resta<0):
        result ='<span class="description-percentage text-green">'+
                '<font style="font-size: 80%;"> ('+number_format(Math.abs(precio-precio2),2)+') </font>'+
                '<i class="fas fa-level-up-alt" style="font-size: 18px;"></i></span>';
        break;
    case (resta>0):
        result ='<span class="description-percentage text-red">'+
                '<font style="font-size: 80%;"> ('+number_format(Math.abs(precio-precio2),2)+') </font>'+
                '<i class="fas fa-level-down-alt" style="font-size: 18px;"></i></span>';
        break;
    default:
        result ='<span class="description-percentage text-yellow">'+
                '<font style="font-size: 80%;"> ('+number_format(Math.abs(precio-precio2),2)+') </font>'+
                '<i class="fa fa-minus" style="font-size: 18px;"></i></span>';
        break;
  }
   return(result);
}

function compararAumento(aumento) {
  var result = '';
  if (aumento > 0) {
    result = '<small class="badge badge-danger" style="font-size: 9px;background-color: #76b8e4;margin-left: 5px;"><i class="fas fa-plus" style="font-size:8px" aria-hidden="true"></i> '+number_format(Math.abs(aumento),2)+'</small>';
  }
  if (aumento < 0) {
    result = '<small class="badge badge-danger" style="font-size: 9px;background-color: #76b8e4;margin-left: 5px;"><i class="fas fa-minus" style="font-size:8px" aria-hidden="true"></i> '+number_format(Math.abs(aumento),2)+'</small>';
  }
  return result;

}

function preciosSugeridos(parametros) {

  var url = $("#base_url").val() + "Precios/Apps/frmPreciosSugeridos";
  var html;
  $.post(url,parametros,function(data){
    bootbox.confirm({
      title: 'Cambio de precios',
      size: 'large',
      message: $(data),
      buttons: {
          cancel: {
            label: '<span class="glyphicon glyphicon-remove" style="margin-right:5%;"></span> Cancelar',
            className: 'btn btn-default btn-sm'
          },
          confirm: {
              label: '<span class="glyphicon glyphicon-floppy-saved" style="margin-right:5%;"></span>Registrar Cambio de Precios ',
              className: 'btn-primary btn-click btn-sm'
          }
        },
      callback: function(result){
        if(result)
        {
           $("#frmPreciosSugeridos").submit();
           return false;
        }
      },
    })
  }).fail(function(e,ex,error){
    swal("Error"," "+error,"error");
  })

}

$("#divmagnasug,#divpremiumsug,#divdieselsug").on("click",function(e){
  var parametros = {
    permiso: $("#Empresas").val()
  }
  preciosSugeridos(parametros);

})



$( "#Empresas" ).change(function() {
  $('#modal1').children('.ibox-content').toggleClass('sk-loading');
  $('#myModal5').modal('toggle');


  if (this.value !='0') {
    $('#modal1').children('.ibox-content').toggleClass('sk-loading');

    var url = $("#base_url").val() + "Precios/Apps/obtenerPrecios"
    var datos = {permiso: this.value};
    $.post(url, datos,function( data) {
      // console.log(data);
        if (data.Sugeridos != null) {
          $("#divSugeridos").removeClass("novisible");
          $("#magnasug").text("$ " + number_format(data.Sugeridos.magna,2));
          $("#premiumsug").text("$ " + number_format(data.Sugeridos.premium,2));
          $("#dieselsug").text("$ " + number_format(data.Sugeridos.diesel,2));

          $("#Fecha_magnasug").text("Sugerido para: " + data.Sugeridos.FechaF);
          $("#Fecha_premiumsug").text("Sugerido para: " + data.Sugeridos.FechaF);
          $("#Fecha_dieselsug").text("Sugerido para: " + data.Sugeridos.FechaF);

        }else {
          $("#divSugeridos").addClass("novisible");
        }
        $('#myModal5').modal('toggle');
        $('#magna').text('$ '+data.Precios[0].magna);
        $('#Fecha_magna').text(data.Precios[0].fecha_magna);
        $('#premium').text('$ '+data.Precios[0].premium);
        $('#Fecha_premium').text(data.Precios[0].fecha_premium);
        $('#diesel').text('$ '+data.Precios[0].diesel);
        $('#Fecha_diesel').text(data.Precios[0].fecha_diesel);

        $('#competencias').text("");
        if (data.Competencia.length > 1) {

          $.each(data.Competencia,function(i,item){
            var iconom = comparar(data.Precios[0].magna,item.magna);
            var iconop = comparar(data.Precios[0].premium,item.premium);
            var iconod = comparar(data.Precios[0].diesel,item.diesel);
            var aummagna = "";
            var aumpremium = "";
            var aumdiesel ="";
            if (parseInt(item.Cambio) == 1){
              aummagna = compararAumento(item.aummagna);
              aumpremium = compararAumento(item.aumpremium);
              aumdiesel = compararAumento(item.aumdiesel);
            }
            $('#competencias').append(
                '<tr id="'+item.permiso+'">'+
                  '<td style="vertical-align: middle;" class="hidden">'+item.permiso+'</td>'+
                  '<td style="vertical-align: middle;" class="">'+item.nombre+((parseInt(item.Cambio)==1)?'<small class="badge badge-danger" data-toggle="tooltip" title="Precios Actualizados" style="font-size: 9px;margin-left: 5px;margin-bottom: 22px;">N</small>':'')+'</td>'+
                  '<td nowrap class="precio">$ '+number_format(item.magna,2)+' '+iconom+
                            '<span class="users-list-date"></br>'+item.fecha_magna+aummagna+'</span></td>'+
                  '<td nowrap class="precio">$ '+number_format(item.premium,2)+' '+iconop+
                            '<span class="users-list-date"></br>'+item.fecha_premium+aumpremium+'</span></td>'+
                  '<td nowrap class="precio">$ '+number_format(item.diesel,2)+' '+((parseFloat(item.diesel)>0)?iconod:'')+
                           '<span class="users-list-date"></br>'+item.fecha_diesel+aumdiesel+'</span></td>'+
                '</tr>'
              );

              /*$('#competencias').append(
                  '<tr id="'+item.permiso+'">'+
                    '<td style="vertical-align: middle;" class="hidden">'+item.permiso+'</td>'+
                    // '<td style="vertical-align: middle;white-space: nowrap;" class="">'+item.nombre+'</td>'+
                    '<td style="vertical-align: middle;" class="">'+item.nombre+'</td>'+
                    '<td nowrap class="precio">$ '+number_format(item.magna,2)+' '+iconom+
                              '<span class="users-list-date"></br>'+item.fecha_magna+'</span></td>'+
                    '<td nowrap class="precio">$ '+number_format(item.premium,2)+' '+iconop+
                              '<span class="users-list-date"></br>'+item.fecha_premium+'</span></td>'+
                    '<td nowrap class="precio">$ '+number_format(item.diesel,2)+' '+((parseFloat(item.diesel)>0)?iconod:'')+
                             '<span class="users-list-date"></br>'+item.fecha_diesel+'</span></td>'+
                  '</tr>'
                );*/

              // <h5 class="description-header" id="magna">$0.00</h5>
              // <span class="users-list-date" id="Fecha_magna"></span>
          });
        }else {
          $('#competencias').append(
              '<tr>'+
                '<td style="vertical-align: middle; text-aling:center;" colspan="5">No se encontraron competencias</td>'+
              '</tr>'
            );
        }
        mostrar(data.Estacion,data.Precios[0],data.Competencia);
      },"json").fail(function(xhr, status, error) {
          $('#myModal5').modal('toggle');
          // console.log(error);
          swal("Error"," "+error,"error");
      });
  }else{
    $('#magna').text('$ 0.00 ');
    $('#Fecha_magna').text('00-00-0000');
    $('#premium').text('$ 0.00 ');
    $('#Fecha_premium').text('00-00-0000');
    $('#diesel').text('$ 0.00 ');
    $('#Fecha_diesel').text('00-00-0000');
    $('#competencias').text("");
  }
});

function initMap() {
map = new google.maps.Map(document.getElementById('map'), {
  center: new google.maps.LatLng(24.788894, -107.428449),
  zoom: 8
});
infoWindow = new google.maps.InfoWindow;
}

function mostrar(estacion,precios,competencias){
  map = new google.maps.Map(document.getElementById('map'), {
    center: new google.maps.LatLng(estacion['lng'],estacion['lat']),
    zoom: 14
  });

    competencias.push({
        "nombre":estacion['nombre'],
        "direccion":estacion['direccion'],
        "permiso":estacion['permiso'],
        "lat":estacion['lat'],
        "lng":estacion['lng'],
        "icon":estacion['icon'],
        "magna":precios['magna'],
        "fecha_magna":precios['fecha_magna'],
        "premium":precios['premium'],
        "fecha_premium":precios['fecha_premium'],
        "diesel":precios['diesel'],
        "fecha_diesel":precios['fecha_diesel']
    });
    var Circle = new google.maps.Circle({
           strokeColor: '#00a65a',
           strokeOpacity: 0.5,
           strokeWeight: 2,
           fillColor: '#00a65a',
           fillOpacity: 0.35,
           map: map,
           center: new google.maps.LatLng(estacion['lng'],estacion['lat']),
           radius: 2000
         });
         // console.log(competencias);
    $.each(competencias,function(i,item){
      var point = new google.maps.LatLng(
          parseFloat(item.lng),
          parseFloat(item.lat));

      var infowincontent = '<div>'+
            '<p style="font-size:14px" class="cortar" data-toggle="tooltip" title="'+item.nombre+'" ><i style="color:#386090;font-size:16px;padding-right: 6px;" class="fas fa-gas-pump"></i>  <b>'+ item.nombre+'</b></p>'+
            '<p style="font-size:14px" class="cortar" data-toggle="tooltip" title="'+item.direccion+'"><i style="color:red;font-size:16px;padding-right: 9px;" class="fas fa-map-marker-alt"></i> '+ item.direccion+'</p>'+
            '<p style="font-size:12px"><i style="color:#386090;font-size:16px;padding-right: 2px;" class="fas fa-tags"></i> '+ item.permiso+'</p>'+
            '<div class="table-responsive no-padding">'+

              '<table class="table table-hover">'+
                 '<tbody>'+
                    '<tr style="background-color: #e3e5e6;">'+
                     '<th>Combustible</th>'+
                     '<th class="text-center">MXN</th>'+
                     '<th class="text-center">Actualización</th>'+
                    '</tr>'+
                    '<tr>'+
                     '<td><span class="label label-primary" style="float: right;width: 100%;height: 20px;">Magna</span></td>'+
                     // '<td><div class="widget style1 navy-bg">Magna</div></td>'
                     '<td class="text-center">$ '+number_format(item.magna,2)+'</td>'+
                     '<td class="text-center">'+item.fecha_magna+'</td>'+
                    '</tr>'+
                    '<tr>'+
                     '<td><span class="label label-danger" style="float: right;width: 100%;height: 20px;">Premium</span></td>'+
                     '<td class="text-center">$ '+number_format(item.premium,2)+'</td>'+
                     '<td class="text-center">'+item.fecha_premium+'</td>'+
                    '</tr>'+
                    '<tr>'+
                     '<td><span class="label label-navy" style="float: right;width: 100%;height: 20px;background-color: black;color: white;">Diesel</span></td>'+
                     '<td class="text-center">$ '+((item.diesel>0)?number_format(item.diesel,2):'<b>N/D</b>')+'</td>'+
                     '<td class="text-center">'+((item.diesel>0)?item.fecha_diesel:'<b>N/D</b>')+'</td>'+
                    '</tr>'+
           '</tbody></table>'
            '</div>'+
          '</div>';

      var icon = {};
      var marker = new google.maps.Marker({
        map: map,
        position: point,
        label: icon.label,
        icon: item.icon
      });

      marker.addListener('click', function() {
        infoWindow.setContent(infowincontent);
        infoWindow.open(map, marker);
      });

      var comp = document.getElementById(item.permiso);
      if (comp) {
        google.maps.event.addDomListener(comp,'click',function(){
          infoWindow.setContent(infowincontent);
          infoWindow.open(map, marker);
        });
      }

    })
}

function deleteMarkets() {
 var markers= map.markers;
      for (var i = 0; i < markers.length; i++) {
        // console.log(merkers);
        markers[i].setMap(null);

      }
      map.markers = [];
}

function downloadUrl(url, callback) {
var request = window.ActiveXObject ?
    new ActiveXObject('Microsoft.XMLHTTP') :
    new XMLHttpRequest;

request.onreadystatechange = function() {
  if (request.readyState == 4) {
    request.onreadystatechange = doNothing;
    callback(request, request.status);
  }
};

request.open('GET', url, true);
request.send(null);
}

var number_format = function(amount, decimals) {
  //if(!separa) { return amount.toFixed(decimals); }
  amount += ''; // por si pasan un numero en vez de un string
  amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto
  decimals = decimals || 0; // por si la variable no fue fue pasada
  // si no es un numero o es igual a cero retorno el mismo cero
  if (isNaN(amount) || amount === 0)
    return parseFloat(0).toFixed(decimals);
  // si es mayor o menor que cero retorno el valor formateado como numero
  amount = '' + amount.toFixed(decimals);
  var amount_parts = amount.split('.'),
  regexp = /(\d+)(\d{3})/;
  while (regexp.test(amount_parts[0]))
   amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
  return amount_parts.join('.');
}

function doNothing() {}

function CargaScript() {
    var script = document.createElement('script');
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyByVgWLve0CNSrpVzQJM3cfMv_ij5P-CnY&callback=initMap';
    document.body.appendChild(script);
}

window.onload = CargaScript;

$("#btn-sync").click(function(){
  var url = $("#base_url").val() + "Precios/Apps/descargarPrecios";
  $('#modal1').children('.ibox-content').toggleClass('sk-loading');
  $('#myModal5').modal('toggle');
  $.post( url , function( data,status ) {
      $('#myModal5').modal('toggle');
      swal("Exito"," "+data,"success")
    },"json").fail(function(xhr, status, error) {
      $('#myModal5').modal('toggle');
      swal("Error"," " + error,"error")
    });

});


$("#btn-syncprecios").click(function(){
  // var url = $("#base_url").val() + "Precios/Apps/descargarPrecios";
  // $('#modal1').children('.ibox-content').toggleClass('sk-loading');
  // $('#myModal5').modal('toggle');
  // $.post( url , function( data,status ) {
  //     $('#myModal5').modal('toggle');
  //     swal("Exito"," "+data,"success")
  //   },"json").fail(function(xhr, status, error) {
  //     $('#myModal5').modal('toggle');
  //     swal("Error"," " + error,"error")
  //   });
  var url = $("#base_url").val() + "Precios/Apps/frmConsultarPrecios";
  $.get(url,{},function(data){
      bootbox.alert({
        title: 'Consultar precios cre',
        message:$(data),
        buttons: {
            ok: {
                label: '<span class="glyphicon glyphicon-remove" style="margin-right:5%;"></span> Cerrar',
                className: 'btn-primary btn-click btn-sm'
            }
          },

      })
  }).fail(function (e,ex,error) {
    swal("Error"," "+error,"error");
  })


});
