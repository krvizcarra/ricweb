  $(document).ready(function(){
    $('#formBuscar').bootstrapValidator({
             message: 'Campo requerido',
                feedbackIcons: {
                    // valid: 'glyphicon glyphicon-ok',
                    // invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
            },
            // This option will not ignore invisible fields which belong to inactive panels
            //excluded: ':disabled',
            fields: {
                Codigo: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido'
                        }
                    }
                }
            }
        });

        $('#btnBuscar').on('click',function(e){
              $('#formConsulta')[0].reset();
              $("#formBuscar").bootstrapValidator('validate');
              var valid= $("#formBuscar").data('bootstrapValidator').isValid();
              if (valid) {
                var dato ={codigo:$('#Codigo').val()};
                $.post( "buscarVale", dato ,function( data ) {
                  try {
                    var data = JSON.parse(data);
                    if (data.error) {
                      $.toast({
                        heading: 'Error',
                        hideAfter: 4000,
                        position: 'bottom-right',
                        text: 'Vale no encontrado',
                        icon: 'error'
                      });
                    }else {
                      $("#FacturaCom").val(data.datos.factura);
                      $("#FechaCom").val((new Date(data.datos.fecha).toLocaleString('en-GB')));
                      $("#Estado").val(data.datos.descripcion);
                      $("#Empresa").val(data.datos.empresa);
                      $("#Sucursal").val(data.datos.sucursalvent);
                      $("#FacturaVen").val(data.datos.facturacli);
                      $("#FechaVen").val((new Date(data.datos.fechatransito).toLocaleString('en-GB')));
                      $("#SucursalEnt").val(data.datos.sucursalreci);
                      $("#FechaEnt").val((new Date(data.datos.fecharecibido).toLocaleString('en-GB')));
                    }
                  } catch (e) {
                    $('#modalsession').append($(data));
                  }
                },"text").fail(function(xhr, status, error) {
                    $.toast({
                      heading: 'Error',
                      hideAfter: 4000,
                      position: 'bottom-right',
                      text: error,
                      icon: 'error'
                    });
                });
              }else {
                $.toast({
                  heading: 'Error',
                  hideAfter: 4000,
                  position: 'bottom-right',
                  text: 'Inserte un código valido',
                  icon: 'error'
                });
              }
        });      


  });//document ready
