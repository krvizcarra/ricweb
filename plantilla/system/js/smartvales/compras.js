  $(document).ready(function(){

    $('#tb-compras').DataTable({
                  pageLength: 10,
                  ajax: $("#base_url").val()+'Smartvales/Apps/obtenerCompras',
                  responsive: true,
                  bLengthChange: false,
                  order:[[0,'desc']],
                  language: {
                      "sProcessing":     "Procesando...",
                      "sLengthMenu":     "Mostrar _MENU_ registros",
                      "sZeroRecords":    "No se encontraron resultados",
                      "sEmptyTable":     "Ningún dato disponible en esta tabla",
                      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                      "sInfoPostFix":    "",
                      "sSearch":         "Buscar:",
                      "sUrl":            "",
                      "sInfoThousands":  ",",
                      "sLoadingRecords": "Cargando...",
                      "oPaginate": {
                          "sFirst":    "Primero",
                          "sLast":     "Último",
                          "sNext":     "Siguiente",
                          "sPrevious": "Anterior"
                      },
                      "oAria": {
                          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                      }
                  },
                  columnDefs: [ {
                      // "targets": -1,
                      // "data": null,
                      // "sClass": "text-center",

                  } ],
                  dom: '<"html5buttons"B>lTfgitp',
                  buttons: [
                      { extend: 'copy'},
                      {extend: 'excel', title: 'ExampleFile'},
                      {extend: 'pdf', title: 'ExampleFile'},

                      {extend: 'print',
                       customize: function (win){
                              $(win.document.body).addClass('white-bg');
                              $(win.document.body).css('font-size', '10px');

                              $(win.document.body).find('table')
                                      .addClass('compact')
                                      .css('font-size', 'inherit');
                      }
                      }
                  ]

              });


    $('#formCompra').formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                // valid: 'glyphicon glyphicon-ok',
                // invalid: 'glyphicon glyphicon-remove',
                // validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                InputFactura: {
                  validators: {
                    notEmpty: {
                      message: 'Campo requerido'
                    }
                  }
                }
            }
        })
        // Remove button click handler
        .on('focusout', '.out', function() {
            var $row  = $(this).parents('.row'),
                index = $row.attr('data-row-index'),
                cantidad = parseInt($('.cantidad'+index).val())?parseInt($('.cantidad'+index).val()):0,
                inicial = parseInt($('.folioInicial'+index).val().replace(/,/g , ""))-1;
            cantidad==0?$('.folioFinal'+index).val(''):$('.folioFinal'+index).val(currency(cantidad+inicial,0, [',', ",", '']));
        });


        fields = [];
        $('#Inputvales').change(function(){
          var text = $("#Inputvales option:selected").text();
          var val = $("#Inputvales option:selected").val();

          if($("#row"+val).length == 0){
            $.ajax({
               type: 'POST',
               url: 'ultimoFolio',
               data:'tipovale='+val+'&opcion=Compras',
               cache:false,
               dataType:'text'
            }).done(function(data){
              try {
                var data = JSON.parse(data);
                $('#listVales').append(
                  '<div class="row" data-row-index="'+val+'" id="row'+val+'">'+
                    '<div class="col-md-3">'+
                        '<div class="bg-info p-xs b-r-xs">'+text+'</div>'+
                        '<input type="hidden" class="form-control" name="Inputvale['+val+'][tipovale]" value="'+val+'">'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<input type="text" readonly class="form-control folioInicial'+val+'" name="Inputvale['+val+'][folioInicial]" value="'+currency(data.folioInicial,0, [',', ",", ''])+'">'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<input type="text" readonly class="form-control folioFinal'+val+'" name="Inputvale['+val+'][folioFinal]">'+
                    '</div>'+
                    '<div class="col-md-2">'+
                      '<div class="form-group">'+
                        '<input type="text" class="form-control out cantidad'+val+'" onkeypress="return isNumber(event);" name="Inputvale['+val+'][cantidad]">'+
                      '</div>'+
                    '</div>'+
                    '<div class="col-md-1">'+
                        '<a data-toggle="tooltip" data-original-title="Eliminar Hoja" href="javascript:eliminarFila('+val+');" class="btn-sm btn-danger fa fa-trash-o" style="margin-top: 5px;margin-left: 5px;"></a>'+
                    '</div>'+
                  '</div>'
                )
                $('#formCompra').formValidation('addField','Inputvale['+val+'][cantidad]',cantidadValidators);
                fields.push(val);
              } catch (e) {
                $('#modalsession').append($(data));
              }
            }).fail(function(xhr, status, error) {
                console.log(error);
            });
          }
        });

});

function eliminarFields() {
  $.each(fields,function(i,item){
    $("#formCompra").data('formValidation').removeField('Inputvale['+item+'][cantidad]');
  });

}

var cantidadValidators = {
    validators: {
      greaterThan: {
          value: 1,
          message: 'Cantidad invalida'
      },
      notEmpty: {
          message: 'Ingrese una cantidad'
      }
    }
};

function eliminarFila(index) {
    $("#row"+index).remove();
    $("#formCompra").data('formValidation').removeField('Inputvale['+index+'][cantidad]');
}
$('#myModal5').on('hide.bs.modal', function (e) {
    $('#listVales').text('');
    $('#formCompra')[0].reset();
    eliminarFields();
    $("#formCompra").data('formValidation').resetForm();
});

$("#formCompra").on('submit',function(e){
  e.preventDefault();
  $('#btnGuardar').prop('disabled', true);
  var listvales = $('#listVales').html().trim();
  if (listvales!=='') {
    $("#formCompra").data('formValidation').validate();
    var valid = $("#formCompra").data('formValidation').isValid();
    var form = $(this).serialize();
    if (valid) {
      $.post( "nuevaCompra", form ,function( data ) {
        try {
            var data = JSON.parse(data);
            $('#myModal5').modal('toggle');
            $('#btnGuardar').prop('disabled', false);
            $('#tb-compras').DataTable().ajax.reload();
        } catch (e) {
            $('#modalsession').append($(data));
            $('#btnGuardar').prop('disabled', false);
        }
      },"text").fail(function(xhr, status, error) {
          $('#btnGuardar').prop('disabled', false);
          $.toast({
            heading: 'Error',
            hideAfter: 4000,
            position: 'bottom-right',
            text: error,
            icon: 'error'
          });
      });
    }else{
      $('#btnGuardar').prop('disabled', false);
    }
  }else {
    $('#btnGuardar').prop('disabled', false);
    $.toast({
      heading: 'Error',
      hideAfter: 4000,
      position: 'bottom-right',
      text: 'Seleccione un tipo de vale.',
      icon: 'error'
    });
  }
});


//Validar es numero
function isNumber(e) {
        k = (document.all) ? e.keyCode : e.which;
        if (k==8 || k==0) return true;
        patron = /\d/;
        n = String.fromCharCode(k);
        return patron.test(n);
    }
function currency(value, decimals, separators) {
        decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
        separators = separators || ['.', "'", ','];
        var number = (parseFloat(value) || 0).toFixed(decimals);
        if (number.length <= (4 + decimals))
            return number.replace('.', separators[separators.length - 1]);
        var parts = number.split(/[-.]/);
        value = parts[parts.length > 1 ? parts.length - 2 : 0];
        var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
            separators[separators.length - 1] + parts[parts.length - 1] : '');
        var start = value.length - 6;
        var idx = 0;
        while (start > -3) {
            result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
                + separators[idx] + result;
            idx = (++idx) % 2;
            start -= 3;
        }
        return (parts.length == 3 ? '-' : '') + result;
    }
