
// obtenerMesajes();

obtenerAlertasSoporte();
navbarRendiapps = $('#navbarRendiapps').html();

function obtenerAlertasSoporte() {
  let url = $("#base_url").val() + "SoporteTecnico/Apps/getMensajes";
  // console.log(url);
  $.get(url,{},function(data){
    let html = '';
    // console.log(data);
    if (data.mensajes.length > 0) {
      html += '<li class="dropdown">'+
                   '<a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">'+
                   '<i class="fa fa-envelope"></i>  <span class="label label-primary">'+data.mensajes.length+'</span></a>'+
                   '<ul class="dropdown-menu dropdown-alerts">';
      $.each(data.mensajes,function(i,item){
        html += '<li>'+
                                  '<div class="dropdown-messages-box" url="'+$("#base_url").val()+item.ruta+'">'+
                                      '<a class="dropdown-item float-left" href="'+$("#base_url").val()+item.ruta+'">'+
                                          '<img alt="image" class="rounded-circle" src="'+item.imagen+'">'+
                                      '</a>'+
                                      '<div class="media-body" >'+
                                          '<small class="float-right">'+item.tiempo+'</small>'+
                                          '<strong>'+item.nombre+'</strong> de <strong>'+item.alias+'</strong>. <br>'+
                                          '<small class="text-muted">'+moment(new Date(item.fecha)).add('days',1).format("DD-MM-YYYY")+'</small>'+
                                      '</div>'+
                                  '</div>'+
                              '</li>'+
                              '<li class="dropdown-divider"></li>';

      });

      html += '</ul></li>';
    }

    if (data.pendientes.length > 0) {
      html += '<li class="dropdown">'+
                   '<a class="dropdown-toggle count-info" data-toggle="dropdown" >'+
                   '<i class="fa fa-bell"></i>  <span class="label label-danger">'+data.countp+'</span></a>'+
                   '<ul class="dropdown-menu dropdown-alerts">';
      $.each(data.pendientes,function(i,item){
        html += '<li>'+
                                  '<div class="dropdown-messages-box" url="'+$("#base_url").val()+'/SoporteTecnico/Apps/reportesPendientes?folio='+item.id+'">'+
                                      '<a class="dropdown-item float-left" href="'+$("#base_url").val()+'/SoporteTecnico/Apps/reportesPendientes">'+
                                          '<img alt="image" class="rounded-circle" src="'+item.imagen+'">'+
                                      '</a>'+
                                      '<div class="media-body ">'+
                                          '<small class="float-right">'+item.tiempo+'</small>'+
                                          '<strong>'+item.nombre+'</strong> de <strong>'+item.alias+'</strong>. <br>'+
                                          '<small class="text-muted">'+moment(new Date(item.fecha)).add('days',1).format("DD-MM-YYYY")+'</small>'+
                                      '</div>'+
                                  '</div>'+
                              '</li>'+
                              '<li class="dropdown-divider"></li>';

      });
      html +=   '<li>'+
                  '<div class="text-center link-block">'+
                      '<a href="'+$("#base_url").val()+'/SoporteTecnico/Apps/reportesPendientes" class="dropdown-item">'+
                          '<i class="fa fa-bell"></i> <strong>Ver todos los pendientes</strong>'+
                      '</a>'+
                  '</div>'+
              '</li>';
      html += '</ul></li>';
    }

    if (data.atendidos.length > 0) {
      html += '<li class="dropdown">'+
                   '<a class="dropdown-toggle count-info" data-toggle="dropdown" >'+
                   '<i class="fa fa-bell"></i>  <span class="label label-warning">'+data.counta+'</span></a>'+
                   '<ul class="dropdown-menu dropdown-alerts">';
      $.each(data.atendidos,function(i,item){
        html += '<li>'+
                                  '<div class="dropdown-messages-box" url="'+$("#base_url").val()+'/SoporteTecnico/Apps/reportesProceso?folio='+item.id+'">'+
                                      '<a class="dropdown-item float-left" href="'+$("#base_url").val()+'/SoporteTecnico/Apps/reportesProceso">'+
                                          '<img alt="image" class="rounded-circle" src="'+item.imagen+'">'+
                                      '</a>'+
                                      '<div class="media-body ">'+
                                          '<small class="float-right">'+item.tiempo+'</small>'+
                                          '<strong>'+item.nombre+'</strong> Atiende <strong>'+item.atendiendo+'</strong>. <br>'+
                                          '<small class="text-muted">'+moment(new Date(item.fecha)).add('days',1).format("DD-MM-YYYY")+'</small>'+
                                      '</div>'+
                                  '</div>'+
                              '</li>'+
                              '<li class="dropdown-divider"></li>';
      });
      html +=   '<li>'+
                  '<div class="text-center link-block">'+
                      '<a href="'+$("#base_url").val()+'/SoporteTecnico/Apps/reportesProceso" class="dropdown-item">'+
                          '<i class="fa fa-bell"></i> <strong>Ver todos los atendidos</strong>'+
                      '</a>'+
                  '</div>'+
              '</li>';
      html += '</ul></li>';
    }

    if (data.avisosReportes[0]._activos > 0){
      // obtenerMesajes(data.avisosReportes[0]._msjprocesados,data.avisosReportes[0]._msjfinalizados,data.avisosReportes[0]._repfinalizados);
    }

    $('#navbarRendiapps').empty();
    $('#navbarRendiapps').append(html+navbarRendiapps);

  },'json').fail(function(e,x,error){
    console.log(error);
  })
}

function obtenerMesajes(msjprocesados,msjfinalizados,repfinalizados) {
  // Swal.fire({
  //   title: 'Avisos de reportes: ',
  //   type: 'info',
  //   html:
  //     '<p class="mt-1">'+
  //     ((msjprocesados > 0 || msjfinalizados >0)?'<strong class="mb-2">Mensajes nuevos</strong><hr>':'')+
  //     ((msjprocesados>0)?'Mensajes de reportes pendientes <a class="" href="'+$("#base_url").val() + 'SoporteTecnico/Apps/reportesProceso'+'"><i class="fa fa-comments"></i><span class="badge badge-danger msgalert">'+msjprocesados+'</span></a><br>':'')+
  //     ((msjfinalizados>0)?'Mensajes de reportes finalizados <a class="" href="'+$("#base_url").val() + 'SoporteTecnico/Apps/reportesFinalizados'+'"><i class="fa fa-comments"></i><span class="badge badge-danger msgalert">'+msjfinalizados+'</span></a>':'')+
  //     ((repfinalizados>0)?'<hr><strong>Reportes pendientes de finalizar </strong><a href="'+$("#base_url").val() + 'SoporteTecnico/Apps/reportesFinalizados'+'"><i class="fa fa-envelope"></i> <span class="badge badge-danger msgalert">'+repfinalizados+'</span></a>':'')+
  //     '</p>',
  //   showCancelButton: false,
  //   confirmButtonColor: '#d33',
  //   confirmButtonText: 'Cancelar'
  // });
}



$(document).on("click",".dropdown-messages-box",function() {
  window.location.href=$(this).attr("url")
});

$(document).on("click",".checkdiv",function() {
  if ($("#divContenido").html().trim() === '') {
    formulario();
  }
});
function formulario() {
  let url = $("#base_url").val() + "SoporteTecnico/Apps/frmNuevoReporte";
  $.get(url,{},function(data){
    $("#divContenido").empty();
    $("#divContenido").append(data);
  }).fail(function(e,x,error){
    console.log(error);
    $("#divContenido").append("Error intente más tarde.");

  })
}
