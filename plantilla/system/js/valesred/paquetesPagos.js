$("#btnNuevo").on("click",function(e){

    let urlForm = $("#base_url").val()+'Rendivales/Apps/nuevoPago';

    $.get(urlForm,{},function(data){
        bootbox.confirm({
            message: $(data),
            buttons: {
                cancel: {
                  label: '<span class="glyphicon glyphicon-remove"></span> Cancelar',
                  className: 'btn btn-danger btn-sm'
                },
                confirm: {
                    label: '<span class="glyphicon glyphicon-floppy-saved"></span> Confirmar',
                    className: 'btn-primary btn-sm'
                }
              },
            callback: function (result) {
              if (result) {
                  $("#frmCorte").submit();
                return false;
              }
            }
        });
    })

});

$(document).on( 'click', '.viewCortes', function (){
  var folio = $(this).attr("folio");

  var row = $(this).closest("tr");
  var estacion = row.find(".estacion").text();


  var url = $("#base_url").val() + 'Rendivales/Apps/verCortesPago?folio='+folio+'&estacion='+estacion;

  window.location.href = url;
});

$(document).on( 'click', '.deletePago', function (){
  var row = $(this).closest("tr");
  var url = $("#base_url").val() + 'Rendivales/Apps/eliminarPago';
  var datos = {folio: $(this).attr("folio"),estacion: $(this).attr("estacion")};
  swal({
      text: "Eliminar Pago "+datos.folio+" de la estación: "+datos.estacion,
      content: "info",
      closeOnConfirm: false,
      buttons: {
        cancel: "Cancelar",
        catch: {
          text: "Confirmar",
        }
      }
  }).then(name => {
      if (name == 'catch') {
        $.post(url,datos,function(data){
          tblCortes.ajax.reload();
          if (data.error) {
            swal("Error!", data.msg, "error");
          }else {
            swal("Exito!", data.msg, "success");
          }
        },'json').fail(function(e,x,error){
          swal("Error!", error, "error");
        });
      }
  })
});


$(document).on( 'click', '.confirmarPago', function (){
  var row = $(this).closest("tr");
  var url = $("#base_url").val() + 'Rendivales/Apps/confirmarPago';
  var datos = {folio: $(this).attr("folio")};
  swal({
      text: "Confirmar Pago "+datos.folio+" de la estación: "+row.find(".estacion").text(),
      content: "info",
      closeOnConfirm: false,
      buttons: {
        cancel: "Cancelar",
        catch: {
          text: "Confirmar",
        }
      }
  }).then(name => {
      if (name == 'catch') {
        $.post(url,datos,function(data){
          tblCortes.ajax.reload();
          if (data.error) {
            swal("Error!", data.msg, "error");
          }else {
            swal("Exito!", data.msg, "success");
          }

        },'json').fail(function(e,x,error){
          swal("Error!", error, "error");
        });
      }
  })
});

let classHidden = "";
if ($("#permisoId").val() == 2) {
  classHidden = "hidden";
}


tblCortes = $('#tb-cortes').DataTable({
      pageLength: 10,
      ajax: $("#base_url").val()+'Rendivales/Apps/obtenerPagosVales',
      columns: [
          { title: "ID", className: 'folio'},
          { title: "Estación",className: 'estacion'},
          { title: "Fecha"},
          { title: "Monto Paquete",className: classHidden},
          { title: "Monto A Pagar",className: 'dt-center'},
          { title: "Estado",className: 'dt-center'},
          { title: "Opciones",className: 'dt-center'},
          { title: "EstadoId",className: 'hidden'},
      ],
      responsive: true,
      bLengthChange: false,
      order: [ [0, 'desc'] ],
      language: {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },
      columnDefs: [ {
          "targets": -1,
          // "data": null,
          "sClass": "text-center",

      } ],
      dom: '<"html5buttons"B>lTfgitp',
      buttons: [
          { extend: 'copy'},
          {extend: 'excel', title: 'ExampleFile'},
          {extend: 'pdf', title: 'ExampleFile'},

          {extend: 'print',
           customize: function (win){
                  $(win.document.body).addClass('white-bg');
                  $(win.document.body).css('font-size', '10px');

                  $(win.document.body).find('table')
                          .addClass('compact')
                          .css('font-size', 'inherit');
          }
          }
      ],

      footerCallback: function( tfoot, data, start, end, display ) {
        var api = this.api();
        // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
              return typeof i === 'string' ?
                  i.replace(/[\$,]/g, '')*1 :
                  typeof i === 'number' ?
                      i : 0;
          };
          let permiso = $("#permisoId").val();
          if (permiso == 2) {
            total = api
                  .column( 3 ,{'filter':'applied'})
                  .data()
                  .reduce( function (a, b) {
                      return intVal(a) + intVal(b);
                  }, 0 );

                  $( api.column( 3 ).footer() ).html(
                        '<b>$ '+currency(total,2, [',', ",", '.'])+'</b>'
                    );
          }else {
            total = api
                  .column( 3 ,{'filter':'applied'})
                  .data()
                  .reduce( function (a, b) {
                      return intVal(a) + intVal(b);
                  }, 0 );

                  $( api.column( 3 ).footer() ).html(
                        '<b>$ '+currency(total,2, [',', ",", '.'])+'</b>'
                    );
              total2 = api
                    .column( 4 ,{'filter':'applied'})
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );


                    $( api.column( 4 ).footer() ).html(
                          '<b>$ '+currency(total2,2, [',', ",", '.'])+'</b>'
                      );




                    $( api.column( 5 ).footer() ).html(
                          '<b>$ '+currency((total - total2),2, [',', ",", '.'])+'</b>'
                      );
          }

      }

  });

  $(document).on( 'click', '.verificarCorte', function (){
    var datos = {folio: $(this).attr("folio")};

    let url = $("#base_url").val() + "Rendivales/Apps/frmVerificarCorte";

    $.post(url,datos,function(data){
      bootbox.confirm({
            'title': 'Verificar Vales',
            'message': $(data),
            'size': 'large',
            'buttons': {
                cancel: {
                  label: '<span class="glyphicon glyphicon-remove" style="margin-right:5%;"></span> Cancelar',
                  className: 'btn btn-danger btn-sm'
                },
                confirm: {
                    label: '<span class="glyphicon glyphicon-floppy-saved" style="margin-right:5%;"></span> Guardar',
                    className: 'btn-primary btn-click btn-sm'
                },

              },
            callback: function(result){
              if(result)
              {
                  $("#frmCorte").submit();
                 return false;
              }

            },
          })
    }).fail(function(e,x,error){
      swal("Error"," "+error,"error");
    })

  });

  $(document).on( 'click', '.eliminarVerificacion', function (){
    var row = $(this).closest("tr");
    var url = $("#base_url").val() + 'Rendivales/Apps/eliminarVerificacion';
    var datos = {folio: $(this).attr("folio")};
    swal({
        text: "¿Eliminar verificación? ",
        content: "info",
        closeOnConfirm: false,
        buttons: {
          cancel: "Cancelar",
          catch: {
            text: "Confirmar",
          }
        }
    }).then(name => {
        if (name == 'catch') {
          $.post(url,datos,function(data){
            tblCortes.ajax.reload();
            if (data.error) {
              swal("Error!", data.msg, "error");
            }else {
              swal("Exito!", data.msg, "success");
            }
          },'json').fail(function(e,x,error){
            swal("Error!", error, "error");
          });
        }
    })
  });

  $(document).on( 'click', '.subirArchivo', function (){
    var datos = {folio: $(this).attr("folio")};

    bootbox.confirm({
          'title': 'Agregar Ficha De Deposito',
          'message': $(form(datos.folio)),
          'buttons': {
              cancel: {
                label: '<span class="glyphicon glyphicon-remove" style="margin-right:5%;"></span> Cancelar',
                className: 'btn btn-danger btn-sm'
              },
              confirm: {
                  label: '<span class="glyphicon glyphicon-floppy-saved" style="margin-right:5%;"></span> Guardar',
                  className: 'btn-primary btn-click btn-sm'
              }
            },
          callback: function(result){
            if(result)
            {
                $("#frmArchivo").submit();
               return false;
            }

          },
        })
  });

  function form(corte) {
      let html =
                '<div class="row" style="margin-top:3%;">'+
                    '<form id="frmArchivo" role="form">'+
                      '<input type="hidden" name="corte" value="'+corte+'" />'+
                          '<div class="col-lg-6 col-md-6 col-sm-6">'+
                            '<div class=" form-group" style="">'+
                              '<div class="inputGroupContainer">'+
                                '<label for="cargar-factura" style="width:100%;" class="custom-cargar-factura" >'+
                                    '<i class="fa fa-cloud-upload"></i> Cargar comprobante'+
                                '</label>'+
                                '<input id="cargar-factura" type="file" name="archivo" accept="image/* , application/pdf"/>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                          '<div class="col-lg-6 col-md-6 col-sm-6">'+
                            '<label for="monto" id="lblFact" style="word-wrap: normal; word-break: break-all;"></label>'+
                          '</div>'+
                        '</form>'+
                    '</div>';
    return html;
  }

  $(document ).on('change','#cargar-factura' , function(e){
    let fileName;
    try {    fileName = e.target.files[0].name;
    } catch (e) {    fileName = "";  }
    $("#lblFact").text(fileName)
  });

  $(document).on('submit',"#frmArchivo",function(e){
    e.preventDefault();
    let url = $("#base_url").val() + "Rendivales/Apps/registrarComprobantePago";
    var form = new FormData(this);
     $.ajax({
           async:true,
           data: form,
           contentType: false,
           cache: false,
           processData:false,
           type: "post",
           dataType: "json",
           url: url,
       })
        .done(function( data) {
          if (data.error) {
            swal("Error"," "+data.msg,"error");
          }else {
            tblCortes.ajax.reload();
            bootbox.hideAll();
            swal("Exito"," "+data.msg,"success");
          }
        })
        .fail(function( jqXHR, textStatus, errorThrown ) {
          swal("Error"," "+errorThrown,"error");

       });
  });


  $(document).on('click',".viewFile",function(e){
    window.open($("#base_url").val()+"/Rendivales/Apps/verArchivo?folio="+$(this).attr("folio"),"_blank");
  });


  $('#Input1, #Input2 , #Input3, #Input4').change( function() {
    tblCortes.draw();
  });

  $.fn.dataTable.ext.search.push(
      function( settings, data, dataIndex ) {
          var val1 = $("#Input1").prop("checked");
          var val2 = $("#Input2").prop("checked");
          var val3 = $("#Input3").prop("checked");
          var val4 = $("#Input4").prop("checked");

          var dato = data[7];
          if (val1 || val2 || val3 || val4) {
              if (val1){
                  if ( dato == 1 ){
                      return true;
                  }
              }
              if (val2){
                  if ( dato == 2 ){
                      return true;
                  }
              }
              if (val3){
                  if ( dato == 3 ){
                      return true;
                  }
              }

              if (val4){
                  if ( dato == 4 ){
                      return true;
                  }
              }
              return false;
          } else {
              // return true;
          }
      }
  );
