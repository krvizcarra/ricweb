$("#btnNuevo").on("click",function(e){

    let urlForm = $("#base_url").val()+'Rendivales/Apps/nuevoCorte';

    $.get(urlForm,{},function(data){
    modalVales = bootbox.confirm({
            message: $(data),
            size: 'large',
            buttons: {
                cancel: {
                  label: '<span class="glyphicon glyphicon-remove"></span> Cancelar',
                  className: 'btn btn-danger btn-sm'
                },
                confirm: {
                    label: '<span class="glyphicon glyphicon-floppy-saved"></span> Confirmar',
                    className: 'btn-primary btn-sm disable btn-click'
                }
              },
            callback: function (result) {
              if (result) {
                  $("#frmCorte").submit();
                return false;
              }
            }
        })//.find(".disable").attr("disabled","disabled");
    })

});

$(document).on( 'click', '.viewCorte', function (){
  var folio = $(this).attr("folio");
  var url = $("#base_url").val() + 'Rendivales/Apps/verValesCorte?folio='+folio;

  window.location.href = url;
});


$(document).on('click',".viewFile",function(e){
  window.open($("#base_url").val()+"/Rendivales/Apps/verArchivo?folio="+$(this).attr("folio"),"_blank");
});

$(document).on( 'click', '.deleteCorte', function (){
  var row = $(this).closest("tr");
  var url = $("#base_url").val() + 'Rendivales/Apps/eliminarCorte';
  var datos = {folio: $(this).attr("folio")};
  swal({
      text: "Eliminar Corte "+datos.folio,
      content: "info",
      closeOnConfirm: false,
      buttons: {
        cancel: "Cancelar",
        catch: {
          text: "Confirmar",
        }
      }
  }).then(name => {
      if (name == 'catch') {
        $.post(url,datos,function(data){
          console.log(
            data
          );
          tblCortes.ajax.reload();
          if (data.error) {
            swal("Error!", data.msg, "error");
          }else {
            swal("Exito!", data.msg, "success");
          }
        },'json').fail(function(e,x,error){
          swal("Error!", error, "error");
        });
      }
  })
});

tblCortes = $('#tb-cortes').DataTable({
      pageLength: 10,
      ajax: $("#base_url").val()+'Rendivales/Apps/obtenerCortesVales',
      responsive: true,
      bLengthChange: false,
      order: [ [0, 'desc'] ],
      columns: [
          { title: "ID", className: 'folio'},
          { title: "Estación",className: 'estacion'},
          { title: "Fecha"},
          { title: "Monto Paquete",className: 'dt-center'},
          { title: "Monto A Pagar",className: 'dt-center'},
          { title: "Estado",className: 'dt-center'},
          { title: "Opciones",className: 'dt-center'},
          { title: "EstadoId",className: 'hidden'}
      ],
      language: {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },
      columnDefs: [ {
          "targets": -1,
          // "data": null,
          "sClass": "text-center",

      } ],
      dom: '<"html5buttons"B>lTfgitp',
      buttons: [
          { extend: 'copy'},
          {extend: 'excel', title: 'ExampleFile'},
          {extend: 'pdf', title: 'ExampleFile'},

          {extend: 'print',
           customize: function (win){
                  $(win.document.body).addClass('white-bg');
                  $(win.document.body).css('font-size', '10px');

                  $(win.document.body).find('table')
                          .addClass('compact')
                          .css('font-size', 'inherit');
          }
          }
      ],
      footerCallback: function( tfoot, data, start, end, display ) {
        var api = this.api();
        // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
              return typeof i === 'string' ?
                  i.replace(/[\$,]/g, '')*1 :
                  typeof i === 'number' ?
                      i : 0;
          };


            total = api
                  .column( 3 ,{'filter':'applied'})
                  .data()
                  .reduce( function (a, b) {
                      return intVal(a) + intVal(b);
                  }, 0 );

                  $( api.column( 3 ).footer() ).html(
                        '<b>$ '+currency(total,2, [',', ",", '.'])+'</b>'
                    );
              total2 = api
                    .column( 4 ,{'filter':'applied'})
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );


                    $( api.column( 4 ).footer() ).html(
                          '<b>$ '+currency(total2,2, [',', ",", '.'])+'</b>'
                      );




                    $( api.column( 5 ).footer() ).html(
                          '<b>$ '+currency((total - total2),2, [',', ",", '.'])+'</b>'
                      );

      }

  })

  $('#Input1, #Input2 , #Input3, #Input4').change( function() {
    tblCortes.draw();
  });

  $.fn.dataTable.ext.search.push(
      function( settings, data, dataIndex ) {
          var val1 = $("#Input1").prop("checked");
          var val2 = $("#Input2").prop("checked");
          var val3 = $("#Input3").prop("checked");
          var val4 = $("#Input4").prop("checked");

          // var dato = data[6].replace(/,/g , "");
          // dato = dato.replace(/%/g,"");
          var dato = data[7];
          if (val1 || val2 || val3 || val4) {
              if (val1){
                  if ( dato == 1 ){
                      return true;
                  }
              }
              if (val2){
                  if ( dato == 2 ){
                      return true;
                  }
              }
              if (val3){
                  if ( dato == 3 ){
                      return true;
                  }
              }

              if (val4){
                  if ( dato == 4 ){
                      return true;
                  }
              }
              return false;
          } else {
              // return true;
          }
      }
  );
