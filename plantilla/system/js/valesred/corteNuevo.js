
$(".disable").attr("disabled","disabled");
$('#fecha').parents('.bootbox').removeAttr('tabindex');

$('#fecha').datetimepicker({
  format: 'DD-MM-YYYY hh:mm A',
  defaultDate: new Date()
});

$("#estacion").select2({
  // minimumResultsForSearch: Infinity,
  width: '100%',
  // theme: "bootstrap"
});

$('.select2-container').addClass('form-control');
// $(".modal").removeAttr("tabindex");
// $('.js-example-basic-multiple').select2({width: "100%"});



$("#btn-consulta").on("click",function () {
  let datos = {estacion: $("#estacion").val(),fechaI: $("#fechaI").find("input").val(),fecha: $("#fecha").find("input").val()};

  let url = $("#base_url").val() + "Rendivales/Apps/valesPendientes";

  $("#frmCorte").bootstrapValidator('validate');
  var valid = $("#frmCorte").data('bootstrapValidator').isValid();
  if (valid) {
      $.post(url,datos,function (data) {
        $("#tb-vales").empty();
        if (data.error) {
          $(".disable").attr("disabled","disabled");
          swal("Error"," "+data.msg,"error");
        }else {
          $(".disable").removeAttr("disabled");
          $.each(data.msg,function(i,item){
            $("#tb-vales").append("<tr>"+
                                    "<td style='text-align:center;'>"+item.fecha+"</td>"+
                                    "<td style='text-align:center;'>"+currency(item.cant50,2, [',', ",", '.'])+"</td>"+
                                    "<td style='text-align:center;'>"+currency(item.cant100,2, [',', ",", '.'])+"</td>"+
                                    "<td style='text-align:center;'>"+currency(item.cant200,2, [',', ",", '.'])+"</td>"+
                                    "<td style='text-align:center;'>"+currency(item.cant500,2, [',', ",", '.'])+"</td>"+
                                    "<td style='text-align:center;'>"+currency(item.cant5000,2, [',', ",", '.'])+"</td>"+
                                    "<td style='text-align:center;'>"+currency(item.cantidad,2, [',', ",", '.'])+"</td>"+
                                    "<td style='text-align:center;'>"+currency(item.total,2, [',', ",", '.'])+"</td>"+
                                  "</tr>");
          });
        }

      },"json").fail(function(e,ex,error){
        $(".disable").attr("disabled","disabled");
        swal("Error"," "+error,"error");
      });
  }

})

$("#estacion").on("change",function(e){
  $(".disable").attr("disabled","disabled");

  let datos = {estacion: $(this).val()};
  let url = $("#base_url").val() + "Rendivales/Apps/ultimoCorte";
  $.post(url,datos,function (data) {
    if (data.error) {
      swal("Error"," "+data.msg,"error");
    }else {

      if (!data.msg) {
        $("#fechaini").removeAttr("style");
        $("#fechaId").removeAttr("disabled");

        // $('.#fechaI').datetimepicker('remove');
        $("#fechaI").datetimepicker({
          format: 'DD-MM-YYYY hh:mm A',
          defaultDate: new Date()
        });

        $('#fecha').data("DateTimePicker").minDate(false);
        $("#frmCorte").bootstrapValidator('updateStatus', "fechaI", 'NOT_VALIDATED').bootstrapValidator('validateField', "fechaI");
      }else {
        $("#fechaI").find("input").val("")
        $("#fechaini").css({"display":"none"});
        $("#fechaId").attr("disabled","disabled");
        $('#fecha').data("DateTimePicker").minDate(new Date(data.msg));
      }
    }
  },"json").fail(function(e,ex,error){
    swal("Error"," "+error,"error");
  });
});


$("#fechaI").on("dp.change", function (e) {
  $("#frmCorte").bootstrapValidator('updateStatus', "fechaI", 'NOT_VALIDATED').bootstrapValidator('validateField', "fechaI");
});

$("#frmCorte").submit(function(e){
  e.preventDefault();
  let url = $(this).attr("action");
  let datos = $(this).serialize();
  $.post(url,datos,function(data){
    if (data.error) {
      swal("Error"," "+data.msg,"error");
    }else {
      tblCortes.ajax.reload();
      bootbox.hideAll();
      swal("Exito"," "+data.msg,"success");
    }
  },'json').fail(function(e,ex,error) {
    swal("Error"," "+error,"error");
  })
})

$('#frmCorte').bootstrapValidator({
  framework: 'bootstrap',
  excluded: ':disabled',
  fields: {
    estacion:{
      validators:{
          notEmpty: {
            message: 'Campo requerido'
          }
      }
    },
    fechaI:{
      validators:{
          notEmpty: {
            message: 'Campo requerido'
          }
      }
    },
    fecha:{
      validators:{
          notEmpty: {
            message: 'Campo requerido'
          }
      }
    }
  }
});
