$("#codigo").focus();
var codigos = [];
var vale50 = [];
var vale100 = [];
var vale200 = [];
var vale500 = [];
var vale5000 = [];

  $("#codigo").on("keyup",function (e) {
    let datos = {valor: $(this).val(),corte: $("#corte").val()};
    let url = $("#base_url").val() + "Rendivales/Apps/validarValeCorte";
    if (datos.valor != '') {
      if($.inArray(datos.valor, codigos) !== -1){
        swal("Error","Ya se agrego este vale en la tabla","error");
      }else {
        $.post(url,datos,function(data){
          if (data.error) {
            swal("Error"," "+data.msg,"error");
          }else {


                codigos.push(datos.valor);
                let montovale = data.msg.monto.replace(/,/g, "");
                switch (parseFloat(montovale)) {
                  case 50:
                    vale50.push(datos.valor);
                    break;
                  case 100:
                    vale100.push(datos.valor);
                    break;
                  case 200:
                    vale200.push(datos.valor);
                    break;
                  case 500:
                    vale500.push(datos.valor);
                    break;
                  case 5000:
                    vale5000.push(datos.valor);
                    break;
                  default:
                }
                sumatorias();

          }
          $("#codigo").focus();
        },'json').fail(function(e,ex,error){
          swal("Error"," "+error,"error");
        })
      }
    }
    $("#codigo").val("");
    $("#codigo").focus();
  });

  function detallesVales(vales) {
    let table = '<div class="responsive"><table class="table table-striped table-bordered table-hover dataTables-example" id="tb-detalle">'+
                  '<thead>'+
                    '<tr>'+
                      '<th>Codigo</th>'+
                      '<th style="text-align:center;">Monto</th>'+
                      '<th style="text-align:center;">Opción</th>'+
                    '</tr>'
                  '</thead>'+
                  '<tbody>';
         table += '<tfoot><tr><td style="text-align:right;"><b>Total</b></td><td style="text-align:center;"><b>$ 0.00</b></td><td style="text-align:center;"><b>Vales: 0.00</b></td></tr></tfoot>';
        table += '</tbody></table></div>';
    return table;
  }

  function detallesVVales(vales) {
    let table = '<div class="responsive"><table class="table table-striped table-bordered table-hover dataTables-example" id="tb-detalle">'+
                  '<thead>'+
                    '<tr>'+
                      '<th>Codigo</th>'+
                      '<th style="text-align:center;">Monto</th>'+
                      // '<th style="text-align:center;">Opción</th>'+
                    '</tr>'
                  '</thead>'+
                  '<tbody>';
         table += '<tfoot><tr><td style="text-align:right;"><b>Total</b></td><td style="text-align:center;"><b>$ 0.00</b></td></tr></tfoot>';
        table += '</tbody></table></div>';
    return table;
  }

  function sumatorias() {
    $("#v50").text(currency(vale50.length ,2, [',', ",", '.']));
    $("#v100").text(currency(vale100.length ,2, [',', ",", '.']));
    $("#v200").text(currency(vale200.length ,2, [',', ",", '.']));
    $("#v500").text(currency(vale500.length ,2, [',', ",", '.']));
    $("#v5000").text(currency(vale5000.length ,2, [',', ",", '.']));

    $("#cantidad").text( currency( (vale50.length+vale100.length+vale200.length+vale500.length+vale5000.length) ,2, [',', ",", '.']) );
    $("#total").text("$"+ currency( ( (vale50.length*50)+(vale100.length*100)+(vale200.length*200)+(vale500.length*500)+(vale5000.length*5000)) ,2, [',', ",", '.']) );
  }

  $(document).on('click','.eliminarFila',function(e){
    let row = $(this).closest('tr');

    let valor = row.find(".codigo").text();

    swal({
        text: "¿ Eliminar Vale " + valor + " ? ",
        content: "info",
        closeOnConfirm: false,
        buttons: {
          cancel: "Cancelar",
          catch: {
            text: "Confirmar",
          }
        }
    }).then(name => {
        if (name == 'catch') {
          tblvales.row(row).remove().draw();
          codigos = arrayRemove(codigos,valor);
          vale50 = arrayRemove(vale50,valor);
          vale100 = arrayRemove(vale100,valor);
          vale200 = arrayRemove(vale200,valor);
          vale500 = arrayRemove(vale500,valor);
          vale5000 = arrayRemove(vale5000,valor);
          sumatorias();
        }
    })
  });

  $("#frmCorte").on("submit",function(e) {
    e.preventDefault();
    if (codigos.length > 0) {
      // alert("hola que hace")

      let datos = {codigos: codigos,corte : $("#corte").val()};
      // console.log(datos);
      let url = $(this).attr("action");
          swal({
              text: "Vales en el corte: "+$("#cantidadvales").text()+" , vales validados: "+$("#cantidad").text()+" ¿ Verificar corte  ? ",
              content: "info",
              closeOnConfirm: false,
              buttons: {
                cancel: "Cancelar",
                catch: {
                  text: "Confirmar",
                }
              }
          }).then(name => {
              if (name == 'catch') {
                $.post(url,datos,function(data){


                  if (data.error) {
                    swal("Error"," "+data.msg,"error");
                  }else {
                    swal("Exito", data.msg, "success");
                    bootbox.hideAll();
                    tblCortes.ajax.reload();
                  }
                },"json").fail(function(e,ex,error){
                  swal("Error"," "+error,"error");
                })
              }
          })

      }else {
        swal("Error","Se deben agregar vales","error");
      }
  });

  $("#v50").click(function(e){
    bootbox.alert(
      {
        title:'Vales $50',
        message:$(detallesVales(vale50)),
        onEscape: true,
        buttons:{
          ok: {
                    label: '<span class="glyphicon glyphicon-remove"></span> Cerrar',
                    className: 'btn btn-danger btn-sm'
              }
        }
      })
      llenarTabla(vale50);
  });

  $("#vv50").click(function(e){
    bootbox.alert(
      {
        title:'Vales $50',
        message:$(detallesVVales(vvale50)),
        onEscape: true,
        buttons:{
          ok: {
                    label: '<span class="glyphicon glyphicon-remove"></span> Cerrar',
                    className: 'btn btn-danger btn-sm'
              }
        }
      });

      llenarVTabla(vvale50);
  });

  $("#v100").click(function(e){
    bootbox.alert(
      {
        title:'Vales $100',
        message:$(detallesVales(vale100)),
        onEscape: true,
        buttons:{
          ok: {
                    label: '<span class="glyphicon glyphicon-remove"></span> Cerrar',
                    className: 'btn btn-danger btn-sm'
              }
        }
      })
      llenarTabla(vale100);
  });

  $("#vv100").click(function(e){
    bootbox.alert(
      {
        title:'Vales $100',
        message:$(detallesVVales(vvale100)),
        onEscape: true,
        buttons:{
          ok: {
                    label: '<span class="glyphicon glyphicon-remove"></span> Cerrar',
                    className: 'btn btn-danger btn-sm'
              }
        }
      });

      llenarVTabla(vvale100);
  });

  $("#v200").click(function(e){
    bootbox.alert(
      {
        title:'Vales $200',
        message:$(detallesVales(vale200)),
        onEscape: true,
        buttons:{
          ok: {
                    label: '<span class="glyphicon glyphicon-remove"></span> Cerrar',
                    className: 'btn btn-danger btn-sm'
              }
        }
      })
      llenarTabla(vale200);
  });

  $("#vv200").click(function(e){
    bootbox.alert(
      {
        title:'Vales $200',
        message:$(detallesVVales(vvale200)),
        onEscape: true,
        buttons:{
          ok: {
                    label: '<span class="glyphicon glyphicon-remove"></span> Cerrar',
                    className: 'btn btn-danger btn-sm'
              }
        }
      });

      llenarVTabla(vvale200);
  });


  $("#v500").click(function(e){
    bootbox.alert(
      {
        title:'Vales $500',
        message:$(detallesVales(vale500)),
        onEscape: true,
        buttons:{
          ok: {
                    label: '<span class="glyphicon glyphicon-remove"></span> Cerrar',
                    className: 'btn btn-danger btn-sm'
              }
        }
      })
      llenarTabla(vale500);
  });

  $("#vv500").click(function(e){
    bootbox.alert(
      {
        title:'Vales $500',
        message:$(detallesVVales(vvale500)),
        onEscape: true,
        buttons:{
          ok: {
                    label: '<span class="glyphicon glyphicon-remove"></span> Cerrar',
                    className: 'btn btn-danger btn-sm'
              }
        }
      });

      llenarVTabla(vvale500);
  });

  $("#v5000").click(function(e){
    bootbox.alert(
      {
        title:'Vales $5,000',
        message:$(detallesVales(vale5000)),
        onEscape: true,
        buttons:{
          ok: {
                    label: '<span class="glyphicon glyphicon-remove"></span> Cerrar',
                    className: 'btn btn-danger btn-sm'
              }
        }
      })
      llenarTabla(vale5000);
  });

  $("#vv5000").click(function(e){
    bootbox.alert(
      {
        title:'Vales $5000',
        message:$(detallesVVales(vvale5000)),
        onEscape: true,
        buttons:{
          ok: {
                    label: '<span class="glyphicon glyphicon-remove"></span> Cerrar',
                    className: 'btn btn-danger btn-sm'
              }
        }
      });

      llenarVTabla(vvale5000);
  });

  function llenarVTabla(vales) {
    tblvales = $("#tb-detalle").DataTable({
      pageLength: 10,
      columns: [
          { title: "Codigo",className: "codigo"},
          { title: "Monto",className: 'dt-center'},
      ],
      responsive: true,
      bLengthChange: false,
      order: [ [0, 'desc'] ],
      language: {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },
      columnDefs: [ {
          "targets": -1,
          // "data": null,
          "sClass": "text-center",

      } ],
      dom: '<"html5buttons"B>lTfgitp',
      buttons: [],
      footerCallback: function( tfoot, data, start, end, display ) {
        var api = this.api();
        // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
              return typeof i === 'string' ?
                  i.replace(/[\$,]/g, '')*1 :
                  typeof i === 'number' ?
                      i : 0;
          };

          num = 0;
          total = api
                .column(1,{'filter':'applied'})
                .data()
                .reduce( function (a, b) {
                    num++;
                    return intVal(a) + intVal(b);
                }, 0 );

                $( api.column(1).footer() ).html(
                      '<b>$ '+currency(total,2, [',', ",", '.'])+'</b>'
                  );
                // $( api.column(2).footer() ).html(
                //       '<b>Vales: '+currency(num,2, [',', ",", '.'])+'</b>'
                //   );
      }
    });

    $.each(vales,function(i,item){
      tblvales.row.add([
        item,
        "$"+ currency(parseFloat(item.substring(8,item.length)),2, [',', ",", '.']),
        '<button data-toggle="tooltip" title="Eliminar vale" codigo="'+item+'" type="button" style="margin-right:1%" class="btn btn-danger btn-xs eliminarFila" name="button"><span class="glyphicon glyphicon-trash"></span></button>'
      ]).draw().node();
    });

  }

  function llenarTabla(vales) {
    tblvales = $("#tb-detalle").DataTable({
      pageLength: 10,
      columns: [
          { title: "Codigo",className: "codigo"},
          { title: "Monto",className: 'dt-center'},
          { title: "Opciones",className: 'dt-center'},
      ],
      responsive: true,
      bLengthChange: false,
      order: [ [0, 'desc'] ],
      language: {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },
      columnDefs: [ {
          "targets": -1,
          // "data": null,
          "sClass": "text-center",

      } ],
      dom: '<"html5buttons"B>lTfgitp',
      buttons: [],
      footerCallback: function( tfoot, data, start, end, display ) {
        var api = this.api();
        // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
              return typeof i === 'string' ?
                  i.replace(/[\$,]/g, '')*1 :
                  typeof i === 'number' ?
                      i : 0;
          };

          num = 0;
          total = api
                .column(1,{'filter':'applied'})
                .data()
                .reduce( function (a, b) {
                    num++;
                    return intVal(a) + intVal(b);
                }, 0 );

                $( api.column(1).footer() ).html(
                      '<b>$ '+currency(total,2, [',', ",", '.'])+'</b>'
                  );
                $( api.column(2).footer() ).html(
                      '<b>Vales: '+currency(num,2, [',', ",", '.'])+'</b>'
                  );
      }
    });

    $.each(vales,function(i,item){
      tblvales.row.add([
        item,
        "$"+ currency(parseFloat(item.substring(8,item.length)),2, [',', ",", '.']),
        '<button data-toggle="tooltip" title="Eliminar vale" codigo="'+item+'" type="button" style="margin-right:1%" class="btn btn-danger btn-xs eliminarFila" name="button"><span class="glyphicon glyphicon-trash"></span></button>'
      ]).draw().node();
    });

  }

  function arrayRemove(arr, value) {
     return arr.filter(function(ele){
         return ele != value;
     });
  }

  // $("#valesfaltantes").on('click',function(e){
  //   // console.log(vale50);
  //   // console.log(vvale50);
  //
  //   comparearrays(vvale50,vale50);
  //   return false;
  //
  // });

  function comparearrays(arr1,arr2){


    const finalarray = [];
    arr1.forEach((e1)=>arr2.forEach(e2=>{
      if (e1 === e2){
        // finalarray.push(e1)
      }else {
        finalarray.push(e1)
      }
    }))

    console.log(finalarray);
  }
