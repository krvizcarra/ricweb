  $(document).ready(function(){
    $('#tb-clientes').DataTable({
                  pageLength: 10,
                  ajax: $("#base_url").val()+'Rendivales/Apps/obtenerClientes',
                  responsive: true,
                  bLengthChange: false,
                  language: {
                      "sProcessing":     "Procesando...",
                      "sLengthMenu":     "Mostrar _MENU_ registros",
                      "sZeroRecords":    "No se encontraron resultados",
                      "sEmptyTable":     "Ningún dato disponible en esta tabla",
                      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                      "sInfoPostFix":    "",
                      "sSearch":         "Buscar:",
                      "sUrl":            "",
                      "sInfoThousands":  ",",
                      "sLoadingRecords": "Cargando...",
                      "oPaginate": {
                          "sFirst":    "Primero",
                          "sLast":     "Último",
                          "sNext":     "Siguiente",
                          "sPrevious": "Anterior"
                      },
                      "oAria": {
                          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                      }
                  },
                  columnDefs: [ {
                      "targets": -1,
                      // "data": null,
                      "sClass": "text-center",

                  } ],
                  dom: '<"html5buttons"B>lTfgitp',
                  buttons: [
                      { extend: 'copy'},
                      {extend: 'excel', title: 'ExampleFile'},
                      {extend: 'pdf', title: 'ExampleFile'},

                      {extend: 'print',
                       customize: function (win){
                              $(win.document.body).addClass('white-bg');
                              $(win.document.body).css('font-size', '10px');

                              $(win.document.body).find('table')
                                      .addClass('compact')
                                      .css('font-size', 'inherit');
                      }
                      }
                  ]

              });

    $("#tb-clientes tbody").on('click','tr .editar',function(e){

        var row = $(this);
        var id = row.attr('id');
        $('#modal-subtitle').text('Editar Cliente');
        $.ajax({
           type: 'POST',
           url: 'getCliente',
           data:'id='+id,
           cache:false,
           dataType:'text'
        }).done(function(data){
          try {
            var data = JSON.parse(data);
            $('#myModal5').modal('toggle');
            $('#id').val(id);
            $('#InputCuenta').val(data.cuenta);
            $('#InputRazon').val(data.razonsocial);
            $('#InputRfc').val(data.rfc);
            $('#InputContacto').val(data.contacto);
            $('#InputCorreo').val(data.correo);
            $('#InputTelefono').val(data.telefono);
            $('#InputDireccion').val(data.direccion);

          } catch (e) {
            $('#modalsession').append($(data));
          }
        }).fail(function(xhr, status, error) {
            console.log(error);
            });
    });

    $('#myModal5').on('show.bs.modal', function (e) {
        $('#formCliente')[0].reset();
        $('#formCliente').data('bootstrapValidator').resetForm();
    });

    $('#formCliente').bootstrapValidator({
             message: 'Campo requerido',
                feedbackIcons: {
                    // valid: 'glyphicon glyphicon-ok',
                    // invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
            },
            // This option will not ignore invisible fields which belong to inactive panels
            //excluded: ':disabled',
            fields: {
                InputCuenta: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido'
                        }
                    }
                },
                InputRazon: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido'
                        }
                    }
                },
                InputRfc: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido'
                        }
                    }
                },
                InputContacto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido'
                        }
                    }
                },
                InputCorreo: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido'
                        },
                        emailAddress: {
                            message: 'Dirección de correo inválida'
                        }
                    }
                },
                InputTelefono: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido'
                        }
                    }
                }
            }
        });


});

$('#btnNuevo').on('click',function(e){
  $('#id').val(0);
  $('#modal-subtitle').text('Agregar Cliente');
});

$("#formCliente").on('submit',function(e){
  e.preventDefault();
  $("#formCliente").bootstrapValidator('validate');
  var valid= $("#formCliente").data('bootstrapValidator').isValid();
  if (valid) {
    var url = $("#base_url").val();
    var form = $('#formCliente').serialize();
    $.post( "nuevoCliente", form ,function( data ) {
      if (data._error == 1) {
        $.toast({
          heading: 'Error',
          hideAfter: 4000,
          position: 'bottom-right',
          text: data._mensaje,
          icon: 'error'
        });
      }else {
        bootbox.hideAll();
        $.toast({
          heading: 'Alerta',
          hideAfter: 4000,
          position: 'bottom-right',
          text: data._mensaje ,
          icon: 'success'
        });
        $('#myModal5').modal('toggle');
        $('#tb-clientes').DataTable().ajax.reload();
      }
    },"json").fail(function(xhr, status, error) {
        $.toast({
          heading: 'Error',
          hideAfter: 4000,
          position: 'bottom-right',
          text: error,
          icon: 'error'
        });
    });
  }
});










//
//
// $("#btnNuevo").click(function(){
//         $("#InputCuenta").attr("readonly", false);
//         $("#formNuevo").removeClass('hidden');
//         $("#listClientes").addClass('hidden');
//         //document.getElementById("InputOpcion").val('nuevo');
//         $("#InputOpcion").val('Nuevo');
//         document.getElementById("titulo").innerHTML = 'Nuevo Cliente';
//         $("#InputCuenta").focus();
//     });
//
// $("#btnCancelar").click(function(){
//         $('#nuevoCliente').data('bootstrapValidator').resetForm(true);
//         $("#formNuevo").addClass('hidden');
//         $("#listClientes").removeClass('hidden');
//         document.getElementById("nuevoCliente").reset();
//         $('#tblClientes').DataTable().ajax.reload();
//         $("#InputCuenta").attr("readonly", true);
//     });
//
//
// function editarCliente(id) {
//
//     $.ajax({
//         type: 'POST',
//         url: '/apps/rendivales/editarCliente',
//         data:'id='+id,
//         cache:false
//
//         }).done(function(data) {
//             if (data) {
//                 $cliente = jQuery.parseJSON(data);
//                 //console.log($cliente.data.id)
//                 $("#InputCuenta").attr("readonly", true);
//                 $("#InputId").val(id);
//                 $("#InputCuenta").val($cliente.data.cuenta);
//                 $("#InputRazon").val($cliente.data.razonsocial);
//                 $("#InputRfc").val($cliente.data.rfc);
//                 $("#InputContacto").val($cliente.data.contacto);
//                 $("#InputCorreo").val($cliente.data.correo);
//                 $("#InputTelefono").val($cliente.data.telefono);
//                 $("#InputDireccion").val($cliente.data.direccion);
//                 $("#InputOpcion").val('Editar');
//                 document.getElementById("titulo").innerHTML = 'Editar Cliente';
//                 $("#formNuevo").removeClass('hidden');
//                 $("#listClientes").addClass('hidden');
//
//             }
//
//             }).fail(function() {
//                  Push.create("Rendiapps",{
//                     body:"Error al editar el cliente.",
//                     icon:"/apps/plantilla/img/icon.png",
//                     timeout:4000,
//                     });
//                     document.getElementById("btnGuardar").disabled = false;
//             });
// }
//
//
// function Limpiar(){
//     $("#InputCuenta").val('');
//     $("#InputRazon").val('');
//     $("#InputRfc").val('');
//     $("#InputContacto").val('');
//     $("#InputCorreo").val('');
//     $("#InputTelefono").val('');
//     $("#InputDireccion").val('');
// }
