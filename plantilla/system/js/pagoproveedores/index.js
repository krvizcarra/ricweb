
$("#btn-facturaNueva").on("click",function(e){
  var url = $("#base_url").val()+'PagoProveedores/Apps/frmFacturas';
  $.get(url,{},function(data){
    bootbox.confirm({
      'title': 'Agregar Factura',
      'message':$(data),
      'size': 'large',
      'buttons': {
          cancel: {
            label: '<span class="glyphicon glyphicon-remove" style="margin-right:5%;"></span> Cancelar',
            className: 'btn btn-default btn-sm'
          },
          confirm: {
              label: '<span class="glyphicon glyphicon-floppy-saved" style="margin-right:5%;"></span> Guardar',
              className: 'btn-primary btn-click btn-sm'
          }
        },
      callback: function(result){
        if(result)
        {
           return false;
        }else {
          location.reload();
        }
      },
      onEscape: function() {
        location.reload();
      }
    })
  }).fail(function(e,x,error){
    $.toast({
      heading: 'Error',
      hideAfter: 4000,
      position: 'bottom-right',
      text: error,
      icon: 'error'
    });
  });
})



$("#btn-pagarFacturas").on('click',function(e){
  $.get($("#base_url").val()+'PagoProveedores/Apps/frmPagarFacturas',{},function(data){
    bootbox.confirm({
      'title': 'Pagar Facturas',
      'message':$(data),
      'size': 'large',
      'buttons': {
          cancel: {
            label: '<span class="glyphicon glyphicon-remove" style="margin-right:5%;"></span> Cancelar',
            className: 'btn btn-default btn-sm btn-cancelar'
          },
          confirm: {
              label: '<span class="glyphicon glyphicon-floppy-saved" style="margin-right:5%;"></span> Pagar',
              className: 'btn-primary btn-click btn-sm'
          }
        },
      callback: function(result){
        if(result)
        {
          $("#frmPagarFacturas").submit();
           return false;
        }
      }
    })
  }).fail(function(e,x,error){
    $.toast({
      heading: 'Error',
      hideAfter: 4000,
      position: 'bottom-right',
      text: error,
      icon: 'error'
    });
  });
});

tblFacturas = $('#tb-facturas').DataTable( {
      ajax: $("#base_url").val() + 'PagoProveedores/Apps/obtenerFacturas',
      columns: [
          { title: "Id",width: '5%'},
          { title: "Fecha",width: '10%'},
          { title: "Estacion",width: '20%'},
          { title: "Factura",width: '10%'},
          { title: "Vencimiento",width: '10%'},
          { title: "Monto",className: 'dt-right ',width: '10%'},
          { title: "Saldo",className: 'dt-right', width:'10%'},
          { title: "Estado",className: 'dt-center', width:'15%'},
          { title: "Opciones",className: 'dt-center', width:'10%'},
          { title: "dia",className: 'hidden'},
          { title: "mes",className: 'hidden'},
          { title: "anio",className: 'hidden'},

      ],
      order: [ [11, 'desc'] , [10, 'desc'],[9, 'desc'] ],
      responsive: true,
      bLengthChange: false,
      pageLength: 20,
      language: {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },

      dom: '<"html5buttons"B>lTfgitp',
      buttons: [
      ],
      footerCallback: function( tfoot, data, start, end, display ) {
        var api = this.api();
        // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
              return typeof i === 'string' ?
                  i.replace(/[\$,]/g, '')*1 :
                  typeof i === 'number' ?
                      i : 0;
          };

          total = api
                .column(6,{'filter':'applied'})
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                $( api.column(6).footer() ).html(
                      '<b>$ '+currency(total,2, [',', ",", '.'])+'</b>'
                  );
      }
  });



  $('#tb-facturas tbody').on( 'click', 'tr .aplicarFactura', function (){
    var row = $(this).closest("tr");
    var url = $("#base_url").val() + 'PagoProveedores/Apps/aplicarFactura';
    var datos = {folio: $(this).attr("folio")};
    swal({
        title: "Alerta",
        text: "¿Desea Aplicar Esta Factura?",
        content: "warning",
        closeOnConfirm: false,
        buttons: {
          cancel: "Cancelar",
          catch: {
            text: "Confirmar",
            // closeModal: false,
          }
        }
    }).then(name => {
      if (name !== null) {
        $.post(url,datos,function(data){
          tblFacturas.ajax.reload();
          if (data.error) {
            swal("Error!", " " + data.msg, "error");
          }else {
            swal("Exito!", " " + data.msg, "success");
          }
        },'json').fail(function(e,x,error){
          swal("Error!", " " + error, "error");
        });
      }
    });
  });

  $('#tb-facturas tbody').on( 'click', 'tr .verFactura', function (){
    var row = $(this).closest("tr");
    var url = $("#base_url").val() + 'PagoProveedores/Apps/verFactura';
    var datos = {folio: $(this).attr("folio")};
    $.get(url,datos,function(data){
      // console.log(data);
      bootbox.dialog({
        message: $(data),
        size:'large',
        buttons: {
            cancel: {
                label: '<span class="glyphicon glyphicon-remove" style="margin-right:5%;"></span> Cerrar',
                className: 'btn btn-primary btn-sm'
            },
        },

      })
    }).fail(function(e,x,error){
      swal("Error!", " " + error, "error");
    });
  });


    $('#tb-facturas tbody').on( 'click', 'tr .rechazarFactura', function (){

      // var row = $(this).closest("tr");
      var datos = {folio: $(this).attr("folio")};
      swal({
        text: '¿Desea Rechazar Esta Factura?',
        content: "input",
        closeOnClickOutside: false,
        closeOnEsc: false,
        buttons: {
          cancel: "Cancelar",
          confirm: {
                    text: "Rechazar factura",
                    closeModal: false,
                }
        },
      })
      .then(name => {
        if (!name) throw null;
          datos.comentario = name;
          return fetch($("#base_url").val() + 'PagoProveedores/AppsSS/rechazarFactura?folio='+datos.folio+'&comentario='+datos.comentario);
      })
      .then(results => {
        return results.json();
      })
      .then(json => {
        tblFacturas.ajax.reload();
        if (!json.error) {
          swal("Exito!"," "+json.msg,"success");
        }else {
          swal("Error!"," "+json.msg,"error");
        }

      })
      .catch(err => {
        if (err) {
          swal("Error!", " "+err, "error");
        } else {
          swal.stopLoading();
          swal.close();
        }
      });

    });
