  $(document).ready(function(){


     optionsLineChart = {
      tooltips: {

        mode: 'index',
        intersect: false,
        callbacks: {
          itemSort: function(a, b) { return b.datasetIndex - a.datasetIndex },
          label: function(tooltipItems, data) {

            return data.datasets[tooltipItems.datasetIndex].label +' : ' + currency(tooltipItems.yLabel,2, [',', ",", '.']);
        },
          // Use the footer callback to display the sum of the items showing in the tooltip
          footer: function(tooltipItems, data) {
            var sum = 0;

            tooltipItems.forEach(function(tooltipItem) {
              sum += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
            });
            return '            Total : ' + currency(sum,2, [',', ",", '.']);
          },
        },
        footerFontStyle: 'normal'
      },
      legend:{
          display:false
      },
      responsive: true,
      scales: {
        xAxes: [{
          stacked: true,
        }],
        yAxes: [{
          stacked: true
        }]
      }
    };
    ctx1 = document.getElementById("ventasGenerales").getContext('2d');
    ctx2 = document.getElementById("ventasCiudad").getContext('2d');
    barEjercicio = new Chart(ctx1, {type: 'bar',data: {},options: optionsLineChart	});
    barCiudad = new Chart(ctx2, {type: 'bar',data: {},options: optionsLineChart	});


    actualizarGraficaGeneral(new Date().getFullYear());
    actualizarGraficaCiudad(new Date().getFullYear(),13,'Culiacán');
});//document ready

$(".dropdown-menu .Year").on('click',function(e){
  $(".Year").each(function(i,item){
    $(this).attr('valor',0);
  })
  let year = $(this).text();
  $(this).attr('valor',year);

  let region = 0;
  let ciudad = "";
  $(".City").each(function(i,item){
    if ($(item).attr('valor') != 0) {
      region =$(item).attr('valor');
      ciudad = $(item).text();
    }
  })
  $('#ejercicio').html('<b>'+year+'</b>');

  actualizarGraficaGeneral(year);
  actualizarGraficaCiudad(year,region,ciudad);
});

$(".dropdown-menu .City").on('click',function(e){
  $(".City").each(function(i,item){
    $(this).attr('valor',0);
  })
  let region = $(this).attr('region');
  let cd = $(this).text();
  $(this).attr('valor',region);

  let year = 0;
  $(".Year").each(function(i,item){
    if ($(item).attr('valor') != 0) {
      year = $(item).attr('valor');
    }
  })
  $('#ciudad').html('<b>'+cd+'</b>');
  actualizarGraficaCiudad(year,region,cd);

});

    function actualizarGraficaGeneral(year) {
      let url = $('#base_url').val()+'Ventas/Apps/getVentasv2';
      let datos = { year:year,est:0};
        $.get(url,datos,function(data) {
          barEjercicio.destroy();
          barEjercicio = new Chart(ctx1, {type: 'bar',data: data,options: optionsLineChart	});
        },'json').fail(function(e,x,error){
          console.log(error);
        });
    }


    function actualizarGraficaCiudad(year,region,ciudad) {
      let url = $('#base_url').val()+'Ventas/Apps/getVentasCiudadv2';
      let datos = { year:year,ciudad:region};
        $.get(url,datos,function(data) {
          barCiudad.destroy();
          barCiudad = new Chart(ctx2, {type: 'bar',data: data,options: optionsLineChart	});
        },'json').fail(function(e,x,error){
          console.log(error);
        });
    }

    function currency(value, decimals, separators) {
            decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
            separators = separators || ['.', "'", ','];
            var number = (parseFloat(value) || 0).toFixed(decimals);
            if (number.length <= (4 + decimals))
                return number.replace('.', separators[separators.length - 1]);
            var parts = number.split(/[-.]/);
            value = parts[parts.length > 1 ? parts.length - 2 : 0];
            var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
                separators[separators.length - 1] + parts[parts.length - 1] : '');
            var start = value.length - 6;
            var idx = 0;
            while (start > -3) {
                result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
                    + separators[idx] + result;
                idx = (++idx) % 2;
                start -= 3;
            }
            return (parts.length == 3 ? '-' : '') + result;
        }
