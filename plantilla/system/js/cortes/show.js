
$("#regresar").click(function(e){
  window.location.href = $("#base_url").val()+"Cortes";
});

$(function(){
    tabledespa = $('#tb-despachadores').DataTable( {
          data: adata,
          columns: [
              { Folio: "#" , className: "removeTr"},
              { title: "Nombre" , className: "fecha"},
              { title: "Cargos" , className: ""},
              { title: "Pagos", className: "" },
              { title: "Posiciones De carga", className: "" },
              { title: "Opción",className: "dt-center"},
          ],

          responsive: true,
          bLengthChange: false,
          language: {
              "sProcessing":     "Procesando...",
              "sLengthMenu":     "Mostrar _MENU_ registros",
              "sZeroRecords":    "No se encontraron resultados",
              "sEmptyTable":     "Ningún dato disponible en esta tabla",
              "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix":    "",
              "sSearch":         "Buscar:",
              "sUrl":            "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "Último",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
          },

          dom: '<"html5buttons"B>lTfgitp',
          buttons: [
              { extend: 'copy'},
              {extend: 'excel', title: 'ExampleFile'},
              {extend: 'pdf', title: 'ExampleFile'},

              {extend: 'print',
               customize: function (win){
                      $(win.document.body).addClass('white-bg');
                      $(win.document.body).css('font-size', '10px');

                      $(win.document.body).find('table')
                              .addClass('compact')
                              .css('font-size', 'inherit');
              }
              }
          ]
      } );
});
  $("#btn-add").on('click',function(e){
    var url = $("#base_url").val();
    $.post( url + "Cortes/Despachadores/formDespachador" ,{corte: varcorte } ,function( data ) {
        var boot = bootbox.confirm(
          {
           message: $(data) ,
           title: "Agregar despachadores",
           size: 'large',
           buttons: {
             confirm: {
                 label: '<span class="glyphicon glyphicon-floppy-saved" style="margin-right:5%;"></span> Agregar',
                 className: 'btn-primary saveDespachador btn-sm'
             },
             cancel: {
               label: '<span class="glyphicon glyphicon-remove" style="margin-right:5%;"></span> Cancelar',
               className: 'btn btn-default btn-sm'
             }
           },
           callback: function(result){
             if(result)
             {
               $("#frmDespachador").submit();
               return false;
              }
          }
         });
         boot.find('.modal-footer').css({'margin-top':'0%'});
    },"text").fail(function(xhr, status, error) {
        console.log(error);
        $.toast({
          heading: 'Error',
          hideAfter: 4000,
          position: 'bottom-right',
          text: error,
          icon: 'error'
        });
      });
  });



$(document).on('click','.deletePosicion',function(e){
  var url = $("#base_url").val();
  var folio = $(this).attr('valor');
  var column = $(this).parents('tr');
  var datos = {folio: folio ,corte: $("#corteD").val()};

  bootbox.confirm({
      title: "¿Desea eliminar este despachador?",
      message: "<div style='text-align:center'><img src='"+url+"plantilla/img/remove-icon-png.png'></img></div>",
      buttons: {
          confirm: {
              label: 'Confirmar',
              className: 'btn-primary btn-sm'
          },
          cancel: {
              label: 'Cancelar',
              className: 'btn-default btn-sm'
          }
      },
      callback: function (result) {
        if (result) {
          var url = $("#base_url").val();
          $.post( url + "Cortes/Cortes/delete" ,datos,function( data ) {
            //console.log(data);
            if (data.error) {
              $.toast({
                heading: 'Error',
                hideAfter: 4000,
                position: 'bottom-right',
                text: data.msg,
                icon: 'error'
              });
            }else {
              var tables = $('.dataTable').DataTable();
              var allData = tables.tables();
              allData.row(column).remove().draw( false );
              // column.remove();
              $.toast({
                heading: 'Alerta',
                hideAfter: 4000,
                position: 'bottom-right',
                text: data.msg,
                icon: 'success'
              });
              $("#txtliquidado").val("$" + parseFloat(data.liquidado).toFixed(2));
              $("#txtimporte").val("$"+ parseFloat(data.importe).toFixed(2));
            }
          },'json');
        }
      }
  });

  });

  $(document).on('keyup','.float',function(e){
    if (this.value === '') {
      $(this).css({'border-style': 'solid', 'border-color': 'red'})
      $("#span"+this.name).text("Campo requerido");
      $("#span"+this.name).css({'color':'red','font-size':'80%'})
    }else{
      if (this.name.indexOf("precio") >= 0 && this.value <= 0 ) {
        $(this).css({'border-style': 'solid', 'border-color': 'red'})
        $("#span"+this.name).text("Precio debe ser mayor a 0");
        $("#span"+this.name).css({'color':'red','font-size':'80%'})
      }else{
         $(this).css({'border-style': 'solid', 'border-color': 'green'})
         $("#span"+this.name).text("");
      }
    }
  })
