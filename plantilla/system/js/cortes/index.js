$("#btn-reportes").on('click',function(e){
  var url = $("#base_url").val();

  bootbox.confirm({
    title: 'Tipo de reporte',
    message : $(reporte()),
    buttons: {
        confirm: {
            label: '<span class="glyphicon glyphicon-floppy-saved" ></span> Confirmar',
            className: 'btn-sm btn-primary btn-sm'
        },
        cancel: {
            label: '<span class="glyphicon glyphicon-remove" style="margin-right:5%;"></span> Cancelar',
            className: 'btn-sm btn-default btn-sm'
        }
    },
    callback: function (result) {
        if (result) {
          var fecha = $("#fechaInicial").val();
          var tiporeporte = $("#tiporeporte").val();
          if (tiporeporte !== null) {
            if (fecha !== '') {
              window.open(url + 'Cortes/Cortes/reporteGeneral?fechaInicial='+fecha,'_blank');
            }else {
              $.toast({
                heading: 'Error',
                hideAfter: 4000,
                position: 'bottom-right',
                text: "Selecciona fecha",
                icon: 'error'
              });
            }
          }else {
            $.toast({
              heading: 'Error',
              hideAfter: 4000,
              position: 'bottom-right',
              text: "Selecciona tipo de reporte",
              icon: 'error'
            });
          }
          return false;
        }
    }
  }).find('.modal-footer').css({'margin-top':'5%'});
})

// $('.toggle-estado').bootstrapToggle({
//       on: 'Abierto',
//       off: 'Cerrado'
// });
$(document).on('change',".toggle-estado",function(e){
    var check = $(this);
    var url = $("#base_url").val();
    var row = $(this).closest("tr");
    folio = row.find(".removeTr").text();
    var toggle = check.data('bs.toggle');
  //  alert(check.prop('checked'));

    modalFecha = bootbox.confirm({
        title: "Cambiar Estado",
        message: "¿Desea cambiar el estado del corte?",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar',
                className: 'btn-sm btn-default'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar',
                className: 'btn-sm btn-primary'
            }
        },
        callback: function (result) {
          var estado = 1;
          if (!check.is(':checked')) {
            estado = 2;
          }
          if (!result) {
            if (estado === 1) {
              toggle.off(true);
            }else {
              toggle.on(true);
            }
          }else {
              $.post(url + "Cortes/Cortes/cambiarEstado",{folio: folio,estado: estado},function(data){
                if (!data.error) {
                  console.log(data);
                  if (estado === 1) {
                    row.find('#lblmodificar').removeAttr('style');
                    row.find('#lblmodificar').addClass('label-primary');
                    row.find('#lblfecha').removeAttr('style');
                    row.find('#lblfecha').addClass('label-warning');
                    row.find('.puntero').removeAttr('style');
                    row.find('.puntero').removeAttr('disabled');
                    row.find(".puntero").css({'margin-left':'1%'});

                  }else {
                    row.find('#lblmodificar').removeClass('label-primary');
                    row.find('#lblmodificar').css({'color':'#fff','background-color':'#74BF8F'});
                    row.find('#lblfecha').removeClass('label-warning');
                    row.find('#lblfecha').css({'color':'#fff','background-color':'#F4BF8F'});
                    row.find(".puntero").css({'pointer-events':'none','cursor':'default'});
                    row.find('.puntero').attr('disabled',"disabled");
                  }
                  $.toast({
                      heading: 'Alerta',
                      hideAfter: 4000,
                      position: 'bottom-right',
                      text: data.msg,
                      icon: 'success'
                    });
                }else {
                  if (estado === 1) {
                    toggle.off(true);
                  }else {
                    toggle.on(true);
                  }
                  $.toast({
                    heading: 'Error',
                    hideAfter: 4000,
                    position: 'bottom-right',
                    text: data.msg,
                    icon: 'error'
                  });
                }
              },'json').fail(function (jqXHR, textStatus, errorThrown) {
                if (estado === 1) {
                  toggle.off(true);
                }else {
                  toggle.on(true);
                }
                $.toast({
                  heading: 'Error',
                  hideAfter: 4000,
                  position: 'bottom-right',
                  text: errorThrown,
                  icon: 'error'
                });
              });
          }
        }
    });

});

$(function(){
    tablecortes = $('#tb-cortes').DataTable( {
          data: adata,
          columns: [
              { Folio: "#" , className: "removeTr"},
              { title: "Fecha" , className: "fecha"},
              { title: "Importe" , className: ""},
              { title: "Liquidado", className: "" },
              { title: "Horario", className: "turno" },
              { title: "Estado", className: "dt-center" },
              { title: "Opción",className: "dt-center"},
          ],
          order : [[ 1, "desc" ],[4,"desc"]],
          responsive: true,
          fnDrawCallback: function() {
            $('.toggle-estado').bootstrapToggle({
                  on: 'Abierto',
                  off: 'Cerrado',

            });
          },
          bLengthChange: false,
          language: {
              "sProcessing":     "Procesando...",
              "sLengthMenu":     "Mostrar _MENU_ registros",
              "sZeroRecords":    "No se encontraron resultados",
              "sEmptyTable":     "Ningún dato disponible en esta tabla",
              "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix":    "",
              "sSearch":         "Buscar:",
              "sUrl":            "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "Último",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
          },

          dom: '<"html5buttons"B>lTfgitp',
          buttons: [
              { extend: 'copy'},
              {extend: 'excel', title: 'ExampleFile'},
              {extend: 'pdf', title: 'ExampleFile'},

              {extend: 'print',
               customize: function (win){
                      $(win.document.body).addClass('white-bg');
                      $(win.document.body).css('font-size', '10px');

                      $(win.document.body).find('table')
                              .addClass('compact')
                              .css('font-size', 'inherit');
              }
              }
          ]
      } );

      $("#Input1").on('change',function(e){
        var url = $("#base_url").val() + 'Cortes/Cortes/cambiarEstacionG';
        var datos = {folio: $(this).val(),
                     razon: $("#Input1 option:selected").attr('razon'),
                     rfc: $("#Input1 option:selected").attr('rfc')};
          $.post(url,datos,function(data){
              location.reload();
          }).fail(function(e,x,error){
            swal("Error!", error, "error");
          });
      })
});

$(document).on('click',".btn-show",function(e){
  location.href = $(this).attr('url');
});

$(document).on('click',".btn-print",function(e){
  let fecha = $(this).attr('valor');
  let turno = $(this).attr('turno');
  let url = $("#base_url").val();
  window.open(url + 'Cortes/Cortes/reporteGeneralPorCorte?fechaInicial='+fecha+'&turno='+turno,'_blank');
});

$(document).on('click',".btn-printdesp",function(e){
  let folio = $(this).attr('valor');
  let fecha = $(this).closest("tr").find('.fecha').text();
  let turno = $(this).closest("tr").find('.turno').text();

  let url = $("#base_url").val();
  $.get(url + 'Cortes/Cortes/frmReporteDespachadores',{'folio':folio},function(data){
    console.log(data);
      if (!data.error) {
        bootbox.confirm(
        {
          message : $(reporteDesp(data.msg,folio,fecha,turno)),
          title : "Ventas por despachador",
          buttons: {
              cancel: {
                label: '<span class="glyphicon glyphicon-remove"></span> Cancelar',
                className: 'btn btn-default btn-sm'
              },
              confirm: {
                  label: '<span class="glyphicon glyphicon-floppy-saved"></span> Confirmar',
                  className: 'btn-primary btn-sm'
              }
            },
            callback: function(result){
              if(result)
              {
                 $("#frmPrintDesp").submit();
                 return false;
               }
           }
        }).find('.modal-footer').css({'margin-top':'5%'});
      }else {
        swal('Error'," "+data.msg,'error');
      }
  },'json').fail(function(e,x,error){
    swal('Error'," "+error,'error');
  });
});

$(document).on("click", ".btn-edit", function() {
  var valor = $(this).attr("valor");
  var row = $(this).closest("tr");
  var fecha = row.find(".fecha").text();
  var folio = row.find(".removeTr").text();
  modalFecha = bootbox.confirm(
    {
      message : $(form(folio,fecha)),
      title : "Fecha del corte",
      buttons: {
          cancel: {
            label: '<span class="glyphicon glyphicon-remove"></span> Cancelar',
            className: 'btn btn-default btn-sm'
          },
          confirm: {
              label: '<span class="glyphicon glyphicon-floppy-saved"></span> Agregar',
              className: 'btn-primary btn-sm'
          }
        },
        callback: function(result){
          if(result)
          {
             updateFecha(folio);
             return false;
           }
       }
    });
    modalFecha.find('.modal-footer').css({'margin-top':'5%'});
});



  $("#btn-add").on('click',function(e){
    modalFecha = bootbox.confirm(
      {
        closeButton: false,
        message : $(form()),
        title : "Fecha del corte",
        buttons: {
            cancel: {
              label: '<span class="glyphicon glyphicon-remove"></span> Cancelar',
              className: 'btn btn-default btn-sm'
            },
            confirm: {
                label: '<span class="glyphicon glyphicon-floppy-saved"></span> Agregar',
                className: 'btn-primary btn-sm'
            }
          },
          callback: function(result){
            if(result)
            {
              var fecha = $("#txtFecha").val();
              if (fecha !== '') {
                sendFecha();
              }else {
                $.toast({
                  heading: 'Error',
                  hideAfter: 4000,
                  position: 'bottom-right',
                  text: "Seleccionar fecha",
                  icon: 'error'
                })
              }
               return false;
             }
         }
      });
      modalFecha.find('.modal-footer').css({'margin-top':'5%'});
  });



  function sendFecha()
  {
    var url = $("#base_url").val();
    var fecha = $("#txtFecha").val();
    var turno = $("#txtTurno").val();
    var modal = bootbox.dialog({
    closeButton: false,
    message: $(crearProgressBar())
    });
    $.post( url + "Cortes/Cortes/create" ,{fecha: fecha,turno: turno},function( data ) {
        console.log(data);
        modal.modal('hide');
        if (data.error == true)
        {
          $.toast({
            heading: 'Error',
            hideAfter: 4000,
            position: 'bottom-right',
            text: data.msg,
            icon: 'error'
          })
        }
        else
        {
          modalFecha.modal('hide');
          if (data.msg.estado_id === "1") {
            est = '<td class="dt-center"><input type="checkbox" checked data-toggle="toggle" data-on="Abierto" data-off="Cerrado" data-size="mini" class="toggle-estado " data-onstyle="primary" data-offstyle="warning"></td>';

            b1 = '<button data-toggle="tooltip" title="Modificar Corte" type="button" url="'+url+'Cortes/show?folio='+data.msg.id+'" style="margin-right:1%" class="btn btn-primary btn-xs btn-show puntero" name="button"><span class="glyphicon glyphicon-edit"></span></button>'+
            '<button data-toggle="tooltip" title="Modificar fecha" type="button" valor="'+data.msg.id+'" style="margin-right:1%" class="btn btn-warning btn-xs btn-edit puntero" name="button"><span class="glyphicon glyphicon-calendar"></span></button>'+
            '<button data-toggle="tooltip" title="Imprimir corte" type="button" turno="'+data.msg.turno+'" valor="'+data.msg.fecha+'" style="margin-right:1%" class="btn btn-info btn-xs btn-print puntero" name="button"><span class="glyphicon glyphicon-print"></span></button>'+
            '<button data-toggle="tooltip" title="Reporte despachador" type="button" valor="'+data.msg.id+'" style="margin-right:1%" class="btn btn-success btn-xs btn-printdesp puntero" name="button"><span class="glyphicon glyphicon-user"></span></button>';
          }else {
            est = '<td class="dt-center"><input type="checkbox" data-toggle="toggle" data-on="Abierto" data-off="Cerrado" data-size="mini" class="toggle-estado " data-onstyle="primary" data-offstyle="warning"></td>';

            b1 = '<button data-toggle="tooltip" disabled style="pointer-events: none;cursor: default;margin-left:1%;" title="Modificar Corte" type="button" url="'+url+'Cortes/show?folio='+data.msg.id+'" style="margin-right:1%" class="btn btn-primary btn-xs btn-show puntero" name="button"><span class="glyphicon glyphicon-edit"></span></button>'+
            '<button data-toggle="tooltip" disabled style="pointer-events: none;cursor: default;margin-left:1%;margin-right:1%" title="Modificar fecha" type="button" valor="'+data.msg.id+'" class="btn btn-warning btn-xs btn-edit puntero" name="button"><span class="glyphicon glyphicon-calendar"></span></button>'+
            '<button data-toggle="tooltip" title="Imprimir corte" type="button" turno="'+data.msg.turno+'" valor="'+data.msg.fecha+'" style="margin-right:1%" class="btn btn-info btn-xs btn-print puntero" name="button"><span class="glyphicon glyphicon-print"></span></button>'+
            '<button data-toggle="tooltip" title="Reporte despachador" type="button" valor="'+data.msg.id+'" style="margin-right:1%" class="btn btn-success btn-xs btn-printdesp puntero" name="button"><span class="glyphicon glyphicon-user"></span></button>';
          }

          tablecortes.row.add( [
             data.msg.id, data.msg.fecha,"$"+data.msg.importe,"$"+data.msg.liquidado,
             data.msg.horarioinicial+" - "+data.msg.horariofin,
             est,b1
          ]).draw().node();

          $(".toggle-estado").bootstrapToggle()
          $.toast({
            heading: 'Alerta',
            hideAfter: 4000,
            position: 'bottom-right',
            text: "Corte registrado",
            icon: 'success'
          })
        }
    },"json").fail(function(xhr, status, error) {
        console.log(error);
        modal.modal('hide');
        $.toast({
          heading: 'Error',
          hideAfter: 4000,
          position: 'bottom-right',
          text: xhr,
          icon: 'error'
        })
      });
  }

  function updateFecha(folio)
  {
    var url = $("#base_url").val();
    var fecha = $("#txtFecha").val();
    var modal = bootbox.dialog({
    closeButton: false,
    message: $(crearProgressBar())
    });
    $.post( url + "Cortes/Cortes/actualizarCorte" ,{fecha: fecha,folio: folio},function( data ) {
        console.log(data);
        modal.modal('hide');
        if (data.error)
        {
          $.toast({
            heading: 'Error',
            hideAfter: 4000,
            position: 'bottom-right',
            text: data.msg,
            icon: 'error'
          });
        }
        else
        {
          modalFecha.modal('hide');
          $.toast({
            heading: 'Alerta',
            hideAfter: 4000,
            position: 'bottom-right',
            text: data.msg,
            icon: 'success'
          });
          setTimeout(function(){location.reload()},2000)
        }
    },"json").fail(function(xhr, status, error) {
        console.log(error);
        modal.modal('hide');
        $.toast({
          heading: 'Error',
          hideAfter: 4000,
          position: 'bottom-right',
          text: xhr,
          icon: 'error'
        });
      });
  }

  function crearProgressBar(color = '')
  {
    var html = '<div class="progress" style="margin-top:5%"> <div class="progress-bar progress-bar-striped '+color+' active" '+
    'role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">'+
    'Cargando </div></div>'
    return html;
  }

  function form(folio,fecha = "")
  {
    if (fecha !== "") {
      var html =
      '<div class="col-lg-5 col-md-5 col-sm-5">'+
            '<label for="txtFecha">Fecha</label>'+
      '</div>'+
      '<div class="col-lg-7 col-md-7 col-sm-7">'+
            '<input type="date" id="txtFecha" class="form-control" value="'+fecha+'">'+
      '</div>';
    }else {
      var html =
      '<div class="col-lg-12 col-md-12 col-sm-12">'+
          '<div class="col-lg-7 col-md-7 col-sm-7">'+
              '<div class="col-lg-3 col-md-3 col-sm-3">'+
                '<label for="txtFecha">Fecha</label>'+
              '</div>'+
              '<div class="col-lg-9 col-md-9 col-sm-9">'+
                  '<input type="date" id="txtFecha" class="form-control" value="">'+
              '</div>'+
           '</div>'+
           '<div class="col-lg-5 col-md-5 col-sm-5">'+
              '<div class="col-lg-3 col-md-3 col-sm-3">'+
                  '<label for="turno">Turno</label>'+
              '</div>'+
              '<div class="col-lg-9 col-md-9 col-sm-9">'+
                '<select class="form-control" id="txtTurno">'+
                  '<option value="1">'+1+'</option>'+
                  '<option value="2">'+2+'</option>'+
                  '<option value="3">'+3+'</option>'+
                  '<option value="4">'+4+'</option>'+
                '</select>'+
              '</div>'+
           '</div>'+
        '</div>';
    }

    return html;
  }

  function reporte() {
    return '<div class="">'+
      '<div class="col-lg-6 col-md-6 col-sm-6 form-group">'+
        '<select class="form-control" name="tiporeporte" id="tiporeporte">'+
          '<option selected disabled>Tipo de reporte</option>'+
          '<option value="1">Reporte General</option>'+
        '</select>'+
      '</div>'+
      '<div class="col-lg-6 col-md-6 col-sm-6 form-group">'+
        '<div class="input-group date" id="dateinicial">'+
          '<span class="input-group-addon">'+
              '<i class="glyphicon glyphicon-calendar"></i>'+
          '</span>'+
          '<input type="date" name="fechaInicial" id="fechaInicial" class="form-control" placeholder="Fecha" />'+
        '</div>'+
      '</div>'+
    '</div>';
  }

  function reporteDesp(despachadores,folio,fecha,turno) {
    let html = '<div class=""><form id="frmPrintDesp">'+
                  '<div class="col-lg-4 col-md-4 col-sm-4 form-group">'+
                    '<label for="despachador"> Despachadores </label>'+
                  '</div>'+
                  '<div class="input-group col-lg-8 col-md-8 col-sm-8" >'+
                    '<span class="input-group-addon">'+
                        '<i class="glyphicon glyphicon-user"></i>'+
                    '</span>'+
                    '<input type="hidden" id="corte" value="'+folio+'"/>'+
                    '<input type="hidden" id="fecha" value="'+fecha+'"/>'+
                    '<input type="hidden" id="turno" value="'+turno+'"/>'+
                    '<select name="despachador" id="despachador" class="form-control" placeholder="Despachadores" >'+
                    '<option selected disabled>Seleccionar despachador</option>';
                    $.each(despachadores, function(i,item){
                      html += '<option value="'+item.id+'">'+item.nombrecompleto+'</option>'
                    })
                    html += '</select>';
                '</div>'+
              '</form></div>';
      return html;
  }

  $(document).on("submit","#frmPrintDesp",function(e){
    e.preventDefault();
    let despachador = {
      corte: $("#frmPrintDesp #corte").val(),
      despachador: $("#frmPrintDesp #despachador").val(),
      despachadorN: $("#frmPrintDesp #despachador option:selected").text(),
      fecha: $("#frmPrintDesp #fecha").val(),
      turno: $("#frmPrintDesp #turno").val(),
    }
    let url = $("#base_url").val() + 'Cortes/Cortes/reporteDespachador?corte='+despachador.corte+'&despachador='+
    despachador.despachador+'&nombre='+despachador.despachadorN+'&fecha='+despachador.fecha+'&turno='+despachador.turno;

    if (despachador.despachador != null) {
      window.open(url ,'_blank');
    }
  });
