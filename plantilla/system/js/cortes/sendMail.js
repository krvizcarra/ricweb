$( function(){
     var mails = '<?php echo JSON_encode($this->session->userdata('Correos')['mails']);?>';
     var availableTags = new Array();
     $.each($.parseJSON(mails),function(i, item){
       availableTags.push(item.from);
     });
    $( "#destino" ).autocomplete({
      source: availableTags
    });

    $("#formMail").on('submit',function(e){
      var bot = bootbox.dialog({
      closeButton: false,
      message: $(crearProgressBar())
      });
      e.preventDefault();
      var url = $("#base_url").val();
      var form = new FormData(this);
      $.ajax({
            async:true,
            data: form,
            contentType: false,
            cache: false,
            processData:false,
            type: "post",
            dataType: "json",
            url: url + "Correos/enviarMail",
        })
         .done(function( data, textStatus, jqXHR ) {
             bot.modal('hide');
             console.log(data);
             if (data.error){
               alertify.error(data.msg);
             }else {
               alertify.success(data.msg);
               setTimeout(function(){location.reload()},2000);
             }
         })
         .fail(function( jqXHR, textStatus, errorThrown ) {
             bot.modal('hide');
             console.log(errorThrown);
             alertify.error(errorThrown);
        });
    });
  });

  function crearProgressBar()
  {
    var html = '<div class="progress" style="margin-top:5%"> <div class="progress-bar progress-bar-striped active" '+
    'role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">'+
    'Enviando </div></div>'
    return html;
  }
