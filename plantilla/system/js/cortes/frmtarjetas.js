$('.float').keypress(function(event) {
  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
    event.preventDefault();
  }
});


  var count = 0;
  $("#btn-add-card").on('click',function(e){
    $("#frmAddCard").bootstrapValidator('validate');
    var valid= $("#frmAddCard").data('bootstrapValidator').isValid();

    if (valid) {
        var montoRest = parseFloat($("#montofaltante").attr('rest'));
        var url = $("#base_url").val();

        datos = {
          monto: $("#monto").val(),
          aid: $("#aid").val(),
          autorizacion: $("#autorizacion").val(),
          operacion: $("#operacion").val(),
          tipopago: $("#tipopago").val(),
          tipopagotexto: $("#tipopago option:selected").text()
        }

        // alert(montoRest+" "+sumColums());
        // if (montoRest >= sumColums()) {
        var tp = $("#tipopago").val();
        $('#frmAddCard').data('bootstrapValidator').resetForm(true);
        $("#tipopago").val(tp);
        $("#monto").focus();
          $('#tb-tarjetas tr:first').
          after('<tr id="'+count+'">'+
                  '<td><input type="text" class="monto form-control montotd" style="width:110%" readonly name="tarjetas['+count+'][monto]" value="'+datos.monto+'"></td>'+
                  '<td><input type="text" class="form-control" style="width:110%" readonly name="tarjetas['+count+'][autorizacion]" value="'+datos.autorizacion+'"></td>'+
                  '<td><input type="text" class="form-control" style="width:110%" readonly name="tarjetas['+count+'][operacion]" value="'+datos.operacion+'"></td>'+
                  '<td><input type="text" class="form-control" style="width:110%;" readonly name="tarjetas['+count+'][aid]" value="'+datos.aid+'"></td>'+
                  '<td>'+
                    '<input type="hidden" name="tarjetas['+count+'][tipopago]" value="'+datos.tipopago+'">'+
                    '<input type="text" class="form-control" style="width:150%;margin:0px;padding:0px;font-size:90%" readonly name="tarjetas['+count+'][tipopagotexto]" value="'+datos.tipopagotexto+'">'+
                  '</td>'+
                  '<td style="text-align:right"><a class="btn btn-link" onclick="eliminarFila('+count+')" ><span class="glyphicon glyphicon-remove"></span></a></td>'+
                '</tr>');
                count++;
                $("#total").val("$"+numberWithCommas(parseFloat(sumColums()).toFixed(2)));

        // }else {
        //   alertify.error("Monto excede el adeudo")
        // }
    }

  });

  function eliminarFila(index)
  {
      $("#"+index).remove();
      $("#total").val("$"+numberWithCommas(parseFloat(sumColums()).toFixed(2)));
  }


 $(function(e){
   $("#cortefrm").val($("#corte").val());
   $("#poscargadespachadorfrm").val($("#poscargadespachador").val());
   $("#parcialfrm").val($("#parcial").val());
 });


  $('.btn-click').on('click',function(e){
    var rowCount = $('#tb-tarjetas tr').length;
    // alert(rowCount);
    if (rowCount > 2) {
      $("#frmTarjetas").submit();
    }else {
      $.toast({
        heading: 'Error',
        hideAfter: 4000,
        position: 'bottom-right',
        text: "Agregar al menos una tarjeta",
        icon: 'error'
      });
    }
  });
  $(".btn-click").attr("data-loading-text","<i class='fa fa-spinner fa-spin '></i> Registrando")

  $("#frmTarjetas").on('submit',function(e){
    e.preventDefault();
    var url = $("#base_url").val();
    var form = $('#frmTarjetas').serialize();

    $(".btn-click").button('loading');
    $(".btn-cancel").attr("disabled","disabled");

      // $.post( url + "Pagos/post" , form ,function( data ) {
      $.post( url + "Cortes/Pagos/pagoTarjeta" , form ,function( data ) {
        $(".btn-click").button('reset');
        $(".btn-cancel").removeAttr("disabled");
        if (data.error) {
          $.toast({
            heading: 'Error',
            hideAfter: 4000,
            position: 'bottom-right',
            text: data.msg,
            icon: 'error'
          });
        }else {
          bootbox.hideAll();
          $.toast({
            heading: 'Alerta',
            hideAfter: 4000,
            position: 'bottom-right',
            text: "Pago registrado",
            icon: 'success'
          });
          window.setTimeout(function(){ location.reload()}, 2000);
        }
      },"json").fail(function(xhr, status, error) {
        $(".btn-click").button('reset');
        $(".btn-cancel").removeAttr("disabled");
          $.toast({
            heading: 'Error',
            hideAfter: 4000,
            position: 'bottom-right',
            text: error,
            icon: 'error'
          });
      });
  });

  // function sumColums()
  // {
  //   var monto = $("#monto").val().replace(/,/g , "");
  //   $(".monto").each(function(i,item){
  //     monto = parseFloat(monto) + parseFloat(item.value.replace(/,/g , ""));
  //   });
  //   return monto;
  // }

  function sumColums()
  {
    var monto = 0;
    $(".montotd").each(function(i,item){
      monto = parseFloat(monto) + parseFloat(item.value.replace(/,/g , ""));
    });
    if (monto.length == 0) {
      monto = 0;
    }
    return monto;
  }

  $(document).ready(function() {
      $('#frmAddCard').bootstrapValidator({
        // framework: 'bootstrap',
        // excluded: ':disabled',
        feedbackIcons: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
              monto: {
                  validators: {
                      greaterThan: {
                              value: 0.01,
                              message: 'Monto debe ser mayor a 0'
                          },
                          notEmpty: {
                            message: 'Ingrese una cantidad'
                          }
                    }
              },
              aid: {
                  validators: {
                          notEmpty: {
                            message: 'Campo requerido'
                          }
                  }
              },
              autorizacion: {
                  validators: {
                    notEmpty: {
                      message: 'Campo requerido'
                    }
                  }
              },
              operacion: {
                  validators: {
                    notEmpty: {
                      message: 'Campo requerido'
                    }
                  }
              },
              tipopago: {
                  validators: {
                    notEmpty: {
                      message: 'Seleccione una opcion'
                    }
                  }
              }
            }
          });
  });
