
var count = 0;
$("#btn-add-vale").on('click',function(e){
  $("#frmValesAdd").bootstrapValidator('validate');
  var valid = $("#frmValesAdd").data('bootstrapValidator').isValid();
  if (valid) {
    var datos = {
      monto: $("#monto").val(),
      codigo: $("#codigo").val(),
      tipopago: $("#tipopago").val(),
      tipopagotexto: $("#tipopago option:selected").text()
    };
    $('#frmValesAdd').data('bootstrapValidator').resetForm(true);

    $('#tb-vales tr:first').
    after('<tr id="'+count+'">'+
            '<td><input type="text" class="monto form-control"  readonly name="vales['+count+'][monto]" value="'+datos.monto+'"></td>'+
            '<td><input type="text" class="form-control" readonly name="vales['+count+'][codigo]" value="'+datos.codigo+'"></td>'+
            '<td>'+
              '<input type="hidden" name="vales['+count+'][tipopago]" value="'+datos.tipopago+'">'+
              '<input type="text" class="form-control" readonly value="'+datos.tipopagotexto+'">'+
            '</td>'+
            '<td style="text-align:right"><a class="btn btn-link" onclick="eliminarFila('+count+')" ><span class="glyphicon glyphicon-remove"></span></a></td>'+
          '</tr>');
          count++;
  }
});

function eliminarFila(index)
{
    $("#"+index).remove();
}

$('.btn-click').on('click',function(e){
  var rowCount = $('#tb-vales tr').length;
  if (rowCount > 2) {
    $("#frmVales").submit();
  }else {
    $.toast({
      heading: 'Error',
      hideAfter: 4000,
      position: 'bottom-right',
      text: "Agregar al menos un vale",
      icon: 'error'
    });
  }
});


$("#frmVales").on('submit',function(e){
  e.preventDefault();
  var url = $("#base_url").val();
  var form = $('#frmVales').serialize();
  $.post( url + "Cortes/Pagos/pagoVales" , form ,function( data ) {
    console.log(data);
    if (data.error) {
      $.toast({
        heading: 'Error',
        hideAfter: 4000,
        position: 'bottom-right',
        text: data.msg,
        icon: 'error'
      });
    }else {
      bootbox.hideAll();
      $.toast({
        heading: 'Alerta',
        hideAfter: 4000,
        position: 'bottom-right',
        text: "Pago registrado",
        icon: 'success'
      });
      window.setTimeout(function(){ location.reload()}, 2000);
    }
  },"json").fail(function(xhr, status, error) {
      $.toast({
        heading: 'Error',
        hideAfter: 4000,
        position: 'bottom-right',
        text: error,
        icon: 'error'
      });
  });
});
$('.float').keypress(function(event) {
  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
    event.preventDefault();
  }
});
$(document).ready(function() {
  $("#cortefrm").val($("#corte").val());
  $("#poscargadespachadorfrm").val($("#poscargadespachador").val());
  $("#parcialfrm").val($("#parcial").val());

    $('#frmValesAdd').bootstrapValidator({
      feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            monto: {
                validators: {
                    greaterThan: {
                            value: 0.01,
                            message: 'Monto debe ser mayor a 0'
                        },
                        notEmpty: {
                          message: 'Ingrese una cantidad'
                        }
                  }
            },
            codigo: {
                validators: {
                        notEmpty: {
                          message: 'Campo requerido'
                        }
                  }
            },
            tipopago: {
                validators: {
                        notEmpty: {
                          message: 'Campo requerido'
                        }
                  }
            }
          }
    });
});
