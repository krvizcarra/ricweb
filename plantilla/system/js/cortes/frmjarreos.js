$("#cortefrm").val($("#corte").val());
$("#poscargadespachadorfrm").val($("#poscargadespachador").val());
$("#parcialfrm").val($("#parcial").val());

$('.btn-click').on('click',function(e){
  $("#frmJarreos").submit();
});

$("#frmJarreos").on('submit',function(e){
  e.preventDefault();
  $("#frmJarreos").bootstrapValidator('validate');
  var valid= $("#frmJarreos").data('bootstrapValidator').isValid();
  if (valid) {
    var url = $("#base_url").val();
    var form = $('form').serialize()
    $.post( url + "Cortes/Pagos/pagoJarreos" , form ,function( data ) {
      console.log(data);
      if (data.error) {
        $.toast({
          heading: 'Error',
          hideAfter: 4000,
          position: 'bottom-right',
          text: data.msg,
          icon: 'error'
        });
      }else {
        bootbox.hideAll();
        $.toast({
          heading: 'Alerta',
          hideAfter: 4000,
          position: 'bottom-right',
          text: "Pago registrado",
          icon: 'success'
        });
        window.setTimeout(function(){ location.reload()}, 2000);
      }
    },"json").fail(function(xhr, status, error) {
        console.log(error);
        $.toast({
          heading: 'Error',
          hideAfter: 4000,
          position: 'bottom-right',
          text: error,
          icon: 'error'
        });
    });
  }
});

$('.float').keypress(function(event) {
  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
    event.preventDefault();
  }
});

$(document).ready(function() {
    $('#frmJarreos').bootstrapValidator({
      framework: 'bootstrap',
      excluded: ':disabled',
      feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          monto:{
            validators:{
              greaterThan: {
                      value: 0.01,
                      message: 'Monto debe ser mayor a 0'
                  },
                notEmpty: {
                  message: 'Campo requerido'
                }
            }
          },
          descripcion:{
            validators:{
                notEmpty: {
                  message: 'Campo requerido'
                }
            }
          },
          posicion:{
            validators:{
                notEmpty: {
                  message: 'Campo requerido'
                }
            }
          }
        }
    });
});
