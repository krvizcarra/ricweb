
  $("#table-pagos tbody").on('click','tr .editPago',function(e){
    if ($(this).hasClass("changing")) {
                var $parentRow = $(this).parents('tr').eq(0);
                $(this).removeClass("changing");
                $parentRow.removeClass("changing");
                $(this).html('<a class="editPago" data-toggle="tooltip" title="Editar"><span class="glyphicon glyphicon-edit"></span></a>');
                $parentRow.find('.editable').attr("contenteditable","false");
            } else {
                var $parentRow = $(this).parents('tr').eq(0);
                var row = $(this).closest("tr");
                monto = row.find(".monto").text();
                aut = row.find(".aut").text();
                oper = row.find(".oper").text();
                aid = row.find(".aid").text();
                var replacemonto = monto.replace(/,/g , "");
                replacemonto = replacemonto.replace('$' , "");
                row.find(".monto").text(replacemonto)
                $(this).addClass("changing");
                $parentRow.addClass("changing");
                $(this).html('<a class="green" data-toggle="tooltip" title="Confirmar">Confirmar<span class="glyphicon glyphicon-check"></span></a>');
                $parentRow.find('.editable').attr("contenteditable", "true");
            }
  });

  $("#table-pagos tbody").on('click','tr .deletePago',function(e){
    var row = $(this).closest("tr");
    var folio = row.find(".folio").text();
    var column = $(this).parents('tr');
    bootbox.confirm({
        message: "¿Desea eliminar este producto?",
        buttons: {
            confirm: {
                label: 'Confirmar',
                className: 'btn-primary btn-sm'
            },
            cancel: {
                label: 'Cancelar',
                className: 'btn-default btn-sm'
            }
        },
        callback: function (result) {
          if (result) {
            var url = $("#base_url").val();
            $.post( url + "Cortes/Pagos/deletePago",{folio: folio,corte: $("#corte").val()}, function( data ) {
              console.log(data);
              if (data.error) {
                $.toast({
                  heading: 'Error',
                  hideAfter: 4000,
                  position: 'bottom-right',
                  text: data.msg,
                  icon: 'error'
                });
              }else {
                column.remove();
                $.toast({
                  heading: 'Alerta',
                  hideAfter: 4000,
                  position: 'bottom-right',
                  text: data.msg,
                  icon: 'success'
                });
              }
            },'json');
          }
        }
    });
  });

$("#table-pagos tbody").on('click','tr .green',function(e){
  var row = $(this).closest("tr");
  var folio = row.find(".folio").text();
  var nvomonto = row.find(".monto");
  var nvaaut = row.find(".aut");
  var nvaoper = row.find(".oper");
  var nvoaid = row.find(".aid");

  var replacemonto = nvomonto.text().replace(/,/g , "");
  replacemonto = replacemonto.replace('$' , "");

  var datos = {
    folio: folio,
    corte: $("#corte").val(),
    monto: replacemonto,
    autorizacion: nvaaut.text(),
    operacion: nvaoper.text(),
    aid: nvoaid.text(),
    tarjeta: true,
    poscargadespachador: $("#poscargadespachador").val()
  }
  monto = monto.replace(/,/g , "");
  monto = monto.replace('$' , "");
  if ((replacemonto > 0 && nvaaut.text() > 0 && nvaoper.text() > 0 &&
        nvoaid.text() > 0) && (replacemonto !== monto || nvaaut.text() !== aut ||
      nvaoper.text() !== oper || nvoaid.text() !== aid)) {
      bootbox.confirm({
          message: "¿Desea editar este producto?",
          buttons: {
              confirm: {
                  label: 'Confirmar',
                  className: 'btn-primary btn-sm'
              },
              cancel: {
                  label: 'Cancelar',
                  className: 'btn-default btn-sm'
              }
          },
          callback: function (result) {
            if (result) {
                var url = $("#base_url").val();
                $.post( url + "Cortes/Pagos/editPago",datos, function( data ) {
                console.log(data);
                if (data.error) {
                    nvomonto.text(monto);
                    $.toast({
                      heading: 'Error',
                      hideAfter: 4000,
                      position: 'bottom-right',
                      text: data.msg,
                      icon: 'error'
                    });
                }else {
                    nvomonto.text("$"+numberWithCommas(replacemonto.toFixed(2)));
                    $.toast({
                      heading: 'Alerta',
                      hideAfter: 4000,
                      position: 'bottom-right',
                      text: data.msg,
                      icon: 'success'
                    });
                }
              },'json').fail(function(xhr, status, error) {
                    console.log(error);
                    $.toast({
                      heading: 'Error',
                      hideAfter: 4000,
                      position: 'bottom-right',
                      text: error,
                      icon: 'error'
                    });
               });
              }else {
                nvomonto.text("$"+numberWithCommas(monto));
                nvaaut.text(aut);
                nvaoper.text(oper);
                nvoaid.text(aid);
              }
            }
          });
      }else {
        nvomonto.text("$"+numberWithCommas(monto));
        nvaaut.text(aut);
        nvaoper.text(oper);
        nvoaid.text(aid);
      }
});

$('.float').keypress(function(event) {
  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
    event.preventDefault();
  }
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
