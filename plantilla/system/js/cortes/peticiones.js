var table;
var inbox;
var outbox;
var spambox;
var draftbox;
var trashbox;
var boxo;
var boxd;
var boxs;
var boxt;
var mails;
var unseen = 0;


$(function() {

  init();

    $(".marcar").click(function(e){
      if ($("#formMail").length > 0) {
        $("#importancia").attr('value',$(this).attr("importancia"));
        $("#lblImport").text($(this).attr("imp"));
        $("#divImport").removeAttr('hidden');
        $("#divColor").css({'background': $(this).attr("color")});
      }
    });

    $("#nvoMail").on('click',function(){
      var url = $("#base_url").val();
        $.get( url + "Correos/verForm" ,function( data ) {
          $("#mailBody").empty();
          //$("#Escond").empty();
          $("#mailBody").removeAttr('hidden');
          $("#Escond").attr("hidden",true);
          $("#mailBody").append(data);
        });
    });
      $("#tlCorreos").on('click','.leerM',function(e){
        var a = $(this).attr("value");
        $.each(mails, function( index, value ){
          if (value.uid == a) {
            if (value.seen == 0) {
              value.seen = 1;
              unseen = unseen - 1;
              $("#noMsg").text(unseen);
            }
            return false;
          }
        });


            var bot = bootbox.dialog({
            closeButton: false,
            message: $(crearProgressBar())
            });
             var url = $("#base_url").val();
                $.ajax({
                    type: 'POST',
                    url: url + "Correos/leerMail",
                    dataType: "text",
                    data: {
                        'mail': $(this).attr("value"),
                        'asunto': $(this).attr("asunto"),
                        'fecha': $(this).attr("fecha"),
                        'remitente': $(this).attr("remitente"),
                        'box': $("#box").val()
                        },
                    success: function(data){
                      bot.modal('hide');
                      //console.log(data);
                      $("#mailBody").removeAttr('hidden');
                      $("#Escond").attr("hidden",true);
                      $("#mailBody").append(data);
                    }
                }).fail(function( jqXHR, textStatus, errorThrown ) {
                    bot.modal('hide');
                    console.log(errorThrown);
                    alertify.error(errorThrown);
               });
        });

        $(".deleteAllMail").click(function(e){

          var myCheckboxes = new Array();
          $("input:checked").each(function() {
             myCheckboxes.push($(this).val());
          });

          if (myCheckboxes.length > 0) {
                bootbox.confirm({
                  message: "¿ Desea eliminar "+myCheckboxes.length+" archivos permanentemente ?",
                  buttons: {
                  confirm: {
                      label: 'Confirmar',
                      className: 'btn-danger'
                  },
                  cancel: {
                      label: 'Cancelar',
                      className: 'btn-default'
                      }
                  },
                  callback: function (result) {
                    if (result) {
                      deleteAllMail(myCheckboxes);
                      /*--Eliminar mail leido----*/
                      for (var i = 0; i < myCheckboxes.length; i++) {
                        mails.splice(i,1);
                      }
                      /*--------------------*/
                    }
                  }
                });
          }else {
            bootbox.alert({
            message: "Debe seleccionar al menos un correo",
            size: 'small'
            });
          }
        });

       $("#btnCheckAll").click(function(e){
          if ($("#iCheckAll").hasClass("fa-square-o")) {
            $("#iCheckAll").removeClass("fa-square-o");
            $("#iCheckAll").addClass("fa-check-square-o");
            $('.chMail').prop('checked', true);
          }else {
            $("#iCheckAll").removeClass("fa-check-square-o");
            $("#iCheckAll").addClass("fa-square-o");
            $('.chMail').prop('checked', false);
          }
        });

        $(".reenviarGral").click(function(e){
          var myCheckboxes = new Array();
          $("input:checked").each(function() {
             myCheckboxes.push($(this).val());
          });
          if (myCheckboxes.length === 1) {
            //trizon
            var bot = bootbox.dialog({
            closeButton: false,
            message: $(crearProgressBar())
            });
            var url = $("#base_url").val();
             $.post( url + "Correos/leerMailGral",{mail :myCheckboxes[0], box : $("#box").val()} ,function( data ) {
               bot.modal('hide');
               $("#mailBody").removeAttr('hidden');
               $("#Escond").attr("hidden",true);
               $("#mailBody").append(data.msg);
               $('<input>').attr({
                  type: 'hidden',
                  name: 'divBoby',
                  value: $("#divbody").html()
              }).appendTo('form');
              },'json')
              .fail(function( jqXHR, textStatus, errorThrown ) {
                  bot.modal('hide');
                  console.log(errorThrown);
                  alertify.error(errorThrown);
             });
          }else {
            bootbox.alert({
            message: "seleccionar solo un correo",
            size: 'small'
            });
          }
        });

        $(".responseGral").click(function(e){
          var url = $("#base_url").val();
          var myCheckboxes = new Array();
          $("input:checked").each(function() {
             myCheckboxes.push($(this).val());
          });
          if (myCheckboxes.length === 1) {
            $.each(mails, function(i, item) {
                if (myCheckboxes[0] == item.uid) {
                  $.get( url + "Correos/verForm" ,function( data ) {
                      $("#mailBody").removeAttr('hidden');
                      $("#Escond").attr("hidden",true);
                      $("#mailBody").append(data);
                      $("#destino").val(item.from);
                      $("#asunto").val(item.subject);
                      $("#asunto").attr('readonly',true);
                      $('<input>').attr({
                         type: 'hidden',
                         name: 'answered',
                         value: 1
                     }).appendTo('form');

                     $('<input>').attr({
                        type: 'hidden',
                        name: 'date',
                        value: item.date
                    }).appendTo('form');

                  });
                }
            });
          }else {
            bootbox.alert({
            message: "seleccionar solo un correo",
            size: 'small'
            });
          }
        });



  $("#bandejaSalida").on('click',function(e){


      var modal = loading();
      if (outbox !== undefined)
      {
          modal.modal('hide');
          $("#tituloMails").text(boxo);
          $("#mailBody").empty();
          $("#Escond").removeAttr("hidden");
          $("#mailBody").attr("hidden",true);
          $("#box").val(boxo);
          limpiarTable();
          llenarTabla(outbox);
          dataTableM();
      }else {
            var url = $("#base_url").val();
            if ($("#Escond").is(':visible') && outbox === undefined) {
              $.post( url + "Correos/outbox" ,function( data ) {
                outbox = data.result;
                boxo = data.box;
                modal.modal('hide');
                $("#tituloMails").text(boxo);
                $("#box").val(boxo);
                limpiarTable();
                llenarTabla(outbox);
                dataTableM();

                },"json").fail(function(xhr, status, error) {
                    modal.modal('hide');
                    alertify.error("error : "+error);
                  });
            }else if (outbox === undefined) {
              $.post( url + "Correos/outbox" ,function( data ) {
                  outbox = data.result;
                  boxo = data.box;
                  modal.modal('hide');
                  $("#tituloMails").text(boxo);
                  $("#box").val(boxo);
                  $("#mailBody").empty();
                  $("#Escond").removeAttr("hidden");
                  $("#mailBody").attr("hidden",true);
                  limpiarTable();
                  llenarTabla(outbox);
                  dataTableM();
              },"json").fail(function(xhr, status, error) {
                  modal.modal('hide');
                  alertify.error("error : "+error);
                });
            }
            else
            {
              modal.modal('hide');
              $("#tituloMails").text(boxo);
              $("#mailBody").empty();
              $("#Escond").removeAttr("hidden");
              $("#mailBody").attr("hidden",true);
              $("#box").val(boxo);
              limpiarTable();
              llenarTabla(outbox);
              dataTableM();
            }
      }
  });

  $("#borradores").on('click',function(e){
      var modal = loading();
      if (draftbox !== undefined)
      {
          modal.modal('hide');
          $("#tituloMails").text(boxd);
          $("#mailBody").empty();
          $("#Escond").removeAttr("hidden");
          $("#mailBody").attr("hidden",true);
          $("#box").val(boxd);
          limpiarTable();
          llenarTabla(draftbox);
          dataTableM();
      }else {
            var url = $("#base_url").val();
            if ($("#Escond").is(':visible') && draftbox === undefined) {
              $.post( url + "Correos/draftbox" ,function( data ) {
                draftbox = data.result;
                boxd = data.box;
                modal.modal('hide');
                $("#tituloMails").text(boxd);
                $("#box").val(boxd);
                limpiarTable();
                llenarTabla(draftbox);
                dataTableM();
                },"json").fail(function(xhr, status, error) {
                    modal.modal('hide');
                    alertify.error("error : "+error);
                  });
            }else if (draftbox === undefined) {
              $.post( url + "Correos/draftbox" ,function( data ) {
                  draftbox = data.result;
                  boxd = data.box;
                  modal.modal('hide');
                  $("#tituloMails").text(boxd);
                  $("#box").val(boxd);
                  $("#mailBody").empty();
                  $("#Escond").removeAttr("hidden");
                  $("#mailBody").attr("hidden",true);
                  limpiarTable();
                  llenarTabla(draftbox);
                  dataTableM();
              },"json").fail(function(xhr, status, error) {
                  modal.modal('hide');
                  alertify.error("error : "+error);
                });
            }
            else
            {
              modal.modal('hide');
              $("#tituloMails").text(boxd);
              $("#mailBody").empty();
              $("#Escond").removeAttr("hidden");
              $("#mailBody").attr("hidden",true);
              $("#box").val(boxd);
              limpiarTable();
              llenarTabla(draftbox);
              dataTableM();
            }
      }
  });

  $("#nodeseado").on('click',function(e){
      var modal = loading();
      if (spambox !== undefined)
      {
          modal.modal('hide');
          $("#tituloMails").text(boxs);
          $("#mailBody").empty();
          $("#Escond").removeAttr("hidden");
          $("#mailBody").attr("hidden",true);
          $("#box").val(boxs);
          limpiarTable();
          llenarTabla(spambox);
          dataTableM();
      }else {
            var url = $("#base_url").val();
            if ($("#Escond").is(':visible') && spambox === undefined) {
              $.post( url + "Correos/spambox" ,function( data ) {
                spambox = data.result;
                boxs = data.box;
                modal.modal('hide');
                $("#tituloMails").text(boxs);
                $("#box").val(boxs);
                limpiarTable();
                llenarTabla(spambox);
                dataTableM();
                },"json").fail(function(xhr, status, error) {
                    modal.modal('hide');
                    alertify.error("error : "+error);
                  });
            }else if (spambox === undefined) {
              $.post( url + "Correos/spambox" ,function( data ) {
                  spambox = data.result;
                  boxs = data.box;
                  modal.modal('hide');
                  $("#tituloMails").text(boxs);
                  $("#box").val(boxs);
                  $("#mailBody").empty();
                  $("#Escond").removeAttr("hidden");
                  $("#mailBody").attr("hidden",true);
                  limpiarTable();
                  llenarTabla(spambox);
                  dataTableM();
              },"json").fail(function(xhr, status, error) {
                  modal.modal('hide');
                  alertify.error("error : "+error);
                });
            }
            else
            {
              modal.modal('hide');
              $("#tituloMails").text(boxs);
              $("#mailBody").empty();
              $("#Escond").removeAttr("hidden");
              $("#mailBody").attr("hidden",true);
              $("#box").val(boxs);
              limpiarTable();
              llenarTabla(spambox);
              dataTableM();
            }
      }
  });

  $("#basurero").on('click',function(e){
      var modal = loading();
      if (trashbox !== undefined)
      {
          modal.modal('hide');
          $("#tituloMails").text(boxt);
          $("#mailBody").empty();
          $("#Escond").removeAttr("hidden");
          $("#mailBody").attr("hidden",true);
          $("#box").val(boxt);
          limpiarTable();
          llenarTabla(trashbox);
          dataTableM();
      }else {
            var url = $("#base_url").val();
            if ($("#Escond").is(':visible') && trashbox === undefined) {
              $.post( url + "Correos/trashbox" ,function( data ) {
                modal.modal('hide');
                trashbox = data.result;
                boxt = data.box;
                $("#tituloMails").text(boxt);
                $("#box").val(boxt);
                limpiarTable();
                llenarTabla(trashbox);
                dataTableM();
              },"json").fail(function(xhr, status, error) {
                  modal.modal('hide');
                  alertify.error("error : "+error);
                });
            }else if (trashbox === undefined) {
                $.post( url + "Correos/trashbox" ,function( data ) {
                    trashbox = data.result;
                    boxt = data.box;
                    modal.modal('hide');
                    $("#tituloMails").text(boxt);
                    $("#box").val(boxt);
                    $("#mailBody").empty();
                    $("#Escond").removeAttr("hidden");
                    $("#mailBody").attr("hidden",true);
                    limpiarTable();
                    llenarTabla(trashbox);
                    dataTableM();
                },"json").fail(function(xhr, status, error) {
                    modal.modal('hide');
                    alertify.error("error : "+error);
                  });
            }
            else
            {
              modal.modal('hide');
              $("#tituloMails").text(boxt);
              $("#mailBody").empty();
              $("#Escond").removeAttr("hidden");
              $("#mailBody").attr("hidden",true);
              $("#box").val(boxt);
              limpiarTable();
              llenarTabla(trashbox);
              dataTableM();
            }
      }
  });


  $("#inbox").on('click',function(e){
        var modal = loading();
          $("#tituloMails").text("inbox");
          $("#mailBody").empty();
          $("#Escond").removeAttr("hidden");
          $("#mailBody").attr("hidden",true);
          $("#box").val('inbox');
          limpiarTable();
          llenarTabla(mails);
          dataTableM();
        modal.modal('hide');
  });

});

function llenarTabla(emails)
{
  $('.removeTr').closest('tr').remove();
    $.each(emails, function( index, value ){
      var readed = "";
      try {
        readed = (value.seen === 1) ? '<td class="removeTr mailbox-subject ">'+decodeUtf8(value.subject)+'</td>' : '<td class="removeTr mailbox-subject "><b>'+decodeUtf8(value.subject) +'</b></td>';
      } catch (e) {
        readed = (value.seen === 1) ? '<td class="removeTr mailbox-subject ">'+value.subject+'</td>' : '<td class="removeTr mailbox-subject "><b>'+value.subject +'</b></td>';
      }

      $('#tlCorreos tr:first').after(
        '<tr>'+
          '<td><input class="chMail removeTr" type="checkbox" id="deletes[]" name="deletes[]" value="'+value.uid+'" ></td>'+
          '<td class="mailbox-name" class="test"><a value="'+value.uid+'" asunto="'+value.subject+'" fecha="'+value.date+'" remitente="'+value.from+'" class="leerM">'+decodeUtf8(value.from)+'</a></td>'+
          readed+
          '<td class="mailbox-attachment"></td>'+
          '<td class="mailbox-date">'+value.date+'</td>'+
        '</tr>')
    });
}

function decodeUtf8(text)
{
  return decodeURIComponent(escape(text));
}

function encodeUtf8(text)
{
  return unescape(encodeURIComponent(text));
}


function limpiarTable()
{
  try {
    var tables = $.fn.dataTable.fnTables(true);
    $(tables).each(function () {
        $(this).dataTable().fnDestroy();
    })
  } catch (e) {
    //console.log(e);
  }
}

function dataTableM()
{
  try {
    $.fn.dataTable.ext.errMode = 'none';
    var table = $('#tlCorreos').DataTable({
    language: {
          "zeroRecords": "No existe ningún registro",
          "info": "",
          "infoEmpty": "Ningún regitros disponibles",
          "sSearch": "Buscar",
          "oPaginate": {
            "sFirst":    	"Primer",
            "sPrevious": 	"Anterior",
            "sNext":     	"Siguiente",
            "sLast":     	"Final"
          },
          infoFiltered: "",
        },
        "bLengthChange" : false, //thought this line could hide the LengthMenu
    "bInfo":false,
      });
      return table;
  } catch (e) {
      //console.log(e);
  }

}




function crearProgressBar(color = '')
{
  var html = '<div class="progress" style="margin-top:5%"> <div class="progress-bar progress-bar-striped '+color+' active" '+
  'role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">'+
  'Cargando </div></div>'
  return html;
}

function deleteAllMail(myCheckboxes)
{
  var bot = bootbox.dialog({
  closeButton: false,
  message: $(crearProgressBar('progress-bar-danger'))
  });
   var url = $("#base_url").val();
      $.ajax({
          type: 'POST',
          url: url + "Correos/eliminarMailM",
          dataType: "json",
          data: {
              'folios': myCheckboxes,
              'box': $("#box").val()
              },
          success: function(data){
            //console.log(data);
            bot.modal('hide');
            if (data.error) {
              alertify.error(data.msg);
            }else {
              alertify.log(data.msg);
            //  setTimeout(function(){location.reload()},2000);
            }
          }
      }).fail(function( jqXHR, textStatus, errorThrown ) {
          bot.modal('hide');
          console.log(errorThrown);
          alertify.error(errorThrown);
     });
}

function loading()
{
    var bot = bootbox.dialog({
      closeButton: false,
      message: $('<div class="loader" style="margin: 0 auto;"></div>'),
      size: 'small'
     });
    bot.find('.modal-content').css({'text-align': 'center','background-color': 'rgba(0, 0, 0, 0)'})
  return bot;
}

function init()
{
  var modal = loading();
  var url = $("#base_url").val();
  var respuesta = null;
  $.post( url + "Correos/getInbox" ,function( data ) {
      modal.modal('hide');
      mails = data.data;
      unseen = data.no;
      respuesta = data.data;
      $("#noMsg").text(unseen);
      //console.log("init : ");
      llenarTabla(data.data);
      dataTableM();
  },"json").fail(function(xhr, status, error) {
      modal.modal('hide');
      console.log(xhr);
      console.log(status);
      console.log(error);
      alertify.error("error : "+xhr);
    });
    return;
}
