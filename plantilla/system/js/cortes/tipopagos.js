$("#regresar").click(function(e){
  window.location.href = $("#base_url").val() + "Cortes/Cortes/show?folio="+$("#corte").val();
});

$("#wrapper").toggleClass("toggled");
var peti ;
$(".add-data").on('click',function(e){
  var url = $("#base_url").val();
  var datos = {poscargadespachador: $("#poscargadespachador").val(),corte: $("#corte").val(),tipo: $(this).attr("valor")};
  console.log(datos);
  var controlador = url + $(this).attr("controlador");
  var titulo = $(this).text();
  $.get(controlador,datos,function(data){
        peti = bootbox.confirm(
        {
          message: $(data),
          title: titulo.toUpperCase(),
          buttons: {
            confirm: {
                label: '<span class="glyphicon glyphicon-floppy-saved" style="margin-right:5%;"></span> Guardar',
                className: 'btn-primary btn-click btn-sm'
            },
            cancel: {
              label: '<span class="glyphicon glyphicon-remove" style="margin-right:5%;"></span> Cancelar',
              className: 'btn btn-default btn-cancel btn-sm'
            }
          },
          callback: function(result){
            if(result)
            {
               return false;
             }else {
               location.reload();
             }
         }
       }).attr("id","modalGs");
    });

 });



 $(document).on('click','.desgolse',function(e){
   var url = $("#base_url").val();
   var datos = {poscargadespachador: $("#poscargadespachador").val(),corte: $("#corte").val(),tipo: $(this).attr("valor"),parcial: $("#parcial").val()};

   $.post(url + "Cortes/Cargos/desgloses",datos,function(data){
     console.log(data);
     if (!data.error) {
       bootbox.alert({
          message: $(data.msg),
          size: "large",
          onEscape: function() {location.reload();},
          buttons: {
             ok: {
               label: '<span class="glyphicon glyphicon-remove"></span> Cerrar',
               className: 'btn btn-primary btn-sm'
             }
           },
           callback: function(result){location.reload()}
        });
     }else {
       $.toast({
         heading: 'Error',
         hideAfter: 4000,
         position: 'bottom-right',
         text: data.msg,
         icon: 'error'
       });
     }
   },"json").fail(function(xhr, status, error) {
        console.log(error);
        $.toast({
          heading: 'Error',
          hideAfter: 4000,
          position: 'bottom-right',
          text: error,
          icon: 'error'
        });
    });
 });

 $(document).on('click','.desgolsePagos',function(e){
   var url = $("#base_url").val();

   var datos = {poscargadespachador: $("#poscargadespachador").val(),corte: $("#corte").val(),tipo: $(this).attr("valor"),parcial: $("#parcial").val()};

   $.post(url + "Cortes/Pagos/desgloses",datos,function(data){
     console.log(data);
     if (!data.error) {
       bootbox.alert({
         title: 'Desglose',
         message: $(data.msg),
         size: "large",
         onEscape: function() {location.reload();},
         buttons: {
            ok: {
              label: '<span class="glyphicon glyphicon-remove"></span> Cerrar',
              className: 'btn btn-primary btn-sm'
            }
          },
          callback: function(result){location.reload()}
       });
     }else {
       $.toast({
         heading: 'Error',
         hideAfter: 4000,
         position: 'bottom-right',
         text: data.msg,
         icon: 'error'
       });
     }
   },"json").fail(function(xhr, status, error) {
        console.log(error);
        $.toast({
          heading: 'Error',
          hideAfter: 4000,
          position: 'bottom-right',
          text: error,
          icon: 'error'
        });
    });
 });

$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});
