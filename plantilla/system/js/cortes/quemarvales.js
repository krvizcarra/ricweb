var count = 0;
$("#btn-add-vale").on('click',function(e){
  $("#monto").trigger('input');
  $("#codigo").trigger('input');
  $("#fecha").trigger('input');
  $("#tipopago").trigger('input');
  $("#tipopagotexto").trigger('input');

  $("#tipovaletexto").val('');
  $("#frmValesAdd").bootstrapValidator('validate');
  var valid = $("#frmValesAdd").data('bootstrapValidator').isValid();
  if (valid) {
    var datos = {
      monto: $("#monto").val(),
      codigo: $("#codigo").val(),
      tipopago: $("#tipopago").val(),
      tipopagotexto: $("#tipopagotexto").val(),
      cliente : $("#cliente").val()
    };
    $('#frmValesAdd').data('bootstrapValidator').resetForm(true);
    $("#monto").attr('readonly',true);

        if (checkifexist(datos.codigo)) {
          $('#tb-vales tr:first').
          after('<tr id="'+count+'">'+
                  '<td style="text-align:center">'+
                    '<input type="hidden" name="vales['+count+'][tipopago]" value="'+datos.tipopago+'">'+
                    datos.tipopagotexto+
                  '</td>'+
                  '<td style="text-align:center"><input type="hidden" class="form-control floata codigo" readonly name="vales['+count+'][codigo]" value="'+datos.codigo+'">'+datos.codigo+'</td>'+
                  '<td style="text-align:center"><input type="hidden" class="monto form-control montotd" readonly name="vales['+count+'][monto]" value="'+parseFloat(datos.monto).toFixed(2)+'">'+parseFloat(datos.monto).toFixed(2)+'</td>'+
                  '<td style="text-align:center">'+datos.cliente+'</td>'+
                  '<td style="text-align:center"><a class="btn btn-link" onclick="eliminarFila('+count+')" ><span class="glyphicon glyphicon-remove"></span></a></td>'+
                '</tr>');
                count++;
              $("#total").val("$"+numberWithCommas(parseFloat(sumColums()).toFixed(2)));
        }else {
          $.toast({
            heading: 'Error',
            hideAfter: 4000,
            position: 'bottom-right',
            text: 'Este vale ya se agregó',
            icon: 'error'
          });
    }
  }
});
$("#codigo").on('select',function (e) {
  $(this).val(this.value);
});

function eliminarFila(index)
{
    $("#"+index).remove();
    $("#total").val("$"+numberWithCommas(parseFloat(sumColums()).toFixed(2)));
}

function eliminarFilas(codigo) {
    $("#tb-vales tbody tr").each(function (i,index) {
      var vale = $(index).closest('tr').find(".codigo").attr('value');
      if (codigo == vale) {
        $(index).remove();
      }
    });
}

$("#frmVales").on('submit',function(e){
  e.preventDefault();
  var url = $("#base_url").val();
  var form = $('#frmVales').serialize();
  var contador = 0;
  $.post( url + "Cortes/Pagos/marcarValesRed" , form ,function( data ) {
    console.log(data);
    if (data.error) {
      $.toast({
        heading: 'Error',
        hideAfter: 4000,
        position: 'bottom-right',
        text: data.msg,
        icon: 'error'
      });
    }else {
      var error = false;
      $.each(data.msg,function(i,item){
        if (item.result == -1) {
          error = true;
          $.toast({
            heading: 'Error',
            hideAfter: false,
            position: 'bottom-right',
            text: "Vale "+item.codigo+" ya se ingreso en la estacion "+item.alias,
            showHideTransition: 'plain',
            icon: 'error'
          });
        }else if (item.result == -2) {
          error = true;
          $.toast({
            heading: 'Error',
            hideAfter: false,
            position: 'bottom-right',
            text: "Vale "+item.codigo+" no se puede puede registrar.",
            showHideTransition: 'plain',
            icon:'error'
          });
        }
        else {
          eliminarFilas(item.codigo);
          $.toast({
            heading: 'Alerta',
            hideAfter: 4000,
            position: 'bottom-right',
            text: "Vale "+item.codigo+" Registrado.",
            icon: 'success'
          });
          contador++;
        }
      });
      if (!error) {
        bootbox.hideAll();
        window.setTimeout(function(){ location.reload();  }, 3000);
      }
      //
    }
  },"json").fail(function(xhr, status, error) {
      console.log(error);
      $.toast({
        heading: 'Error',
        hideAfter: 4000,
        position: 'bottom-right',
        text: error,
        icon: 'error'
      });

  });
});

$('.float').keypress(function(event) {
  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
    event.preventDefault();
  }
});

$(document).ready(function() {
  var ctrlDown = false,
      ctrlKey = 17,
      cmdKey = 91,
      vKey = 86,
      cKey = 67;

  $(document).keydown(function(e) {
      if (e.keyCode == ctrlKey || e.keyCode == cmdKey) ctrlDown = true;
  }).keyup(function(e) {
      if (e.keyCode == ctrlKey || e.keyCode == cmdKey) ctrlDown = false;
  });

  $(".no-copy-paste").keydown(function(e) {
      if (ctrlDown && (e.keyCode == vKey || e.keyCode == cKey)) return false;
  });


  $("#cortefrm").val($("#corte").val());
  $("#poscargadespachadorfrm").val($("#poscargadespachador").val());
  $("#parcialfrm").val($("#parcial").val());



    $('#frmValesAdd').bootstrapValidator({
      feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            tipopagotexto: {
                validators: {
                        notEmpty: {
                          message: 'Ingrese una cantidad 1'
                        }
                  }
            },
            monto: {
                validators: {
                    greaterThan: {
                            value: 1,
                            message: 'Monto debe ser mayor a 0'
                        },
                        notEmpty: {
                          message: 'Ingrese una cantidad'
                        }
                  }
            },
            codigo: {
                validators: {
                        notEmpty: {
                          message: 'Campo requerido'
                        },
                        callback: {
                          message: 'El vale ingresado tiene formato no valido',
                          callback: function (value, validator, $field) {
                              if (!value.match(/^[0-9]+$/)) {
                                  return false;
                              }else {
                                return true;
                              }
                          }
                      },
                  }
            },
            tipopago: {
                validators: {
                        notEmpty: {
                          message: 'Campo requerido'
                        }
                  }
            },
            fecha: {
                validators: {
                        notEmpty: {
                          message: 'Campo requerido'
                        }
                  }
            }
          }
    });
});

function sumColums()
{
  var monto = 0;
  $(".montotd").each(function(i,item){
    monto = parseFloat(monto) + parseFloat(item.value.replace(/,/g , ""));
  });
  if (monto.length == 0) {
    monto = 0;
  }
  return monto;
}

var delay = (function(){
   var timer = 0;
   return function(callback, ms){
      clearTimeout (timer);
      timer = setTimeout(callback, ms);
   };
})();

$("#codigo").on("input", function() {
   delay(function(){
     var url = $("#base_url").val();
     let size = $("#codigo").val().length;
     // alert(size)
     if (size == 12 || size == 14) {
       if ($("#codigo").val() != '' ) {
         $.post(url + "Cortes/Cargos/validarTarjeta",{longitud: $("#codigo").val()},function(data){
           console.log(data);
           if (!data.error) {
             if (data.msg.monto == null) {
               $("#monto").removeAttr('readonly');
               $("#monto").val(data.msg.monto);
               $("#tipopago").val(data.msg.id);
               $("#tipopagotexto").val(data.msg.nombre);
               $("#monto").val('');
               $("#monto").focus();
             }else {
               $("#monto").val(data.msg.monto);
               $("#tipopago").val(data.msg.id);
               $("#tipopagotexto").val(data.msg.nombre);
               $("#cliente").val(data.msg.cliente);
               $("#btn-add-vale").click();
               $("#codigo").focus();
             }

           }else {
             $("#codigo").val('');
             $.toast({
               heading: 'Error',
               hideAfter: 4000,
               position: 'bottom-right',
               text: data.msg,
               icon: 'error'
             });
           }
         },'json').fail(function(xhr, status, error) {
             console.log(error);
             $.toast({
               heading: 'Error',
               hideAfter: 4000,
               position: 'bottom-right',
               text: error,
               icon: 'error'
             });
         });
       }
     }else {
       if (size > 0) {
         $("#codigo").val("");
         $("#codigo").focus();
         $.toast({
           heading: 'Error',
           hideAfter: 4000,
           position: 'bottom-right',
           text: "Formato invalido.",
           icon: 'error'
         });
       }

     }

   }, 400 );
});

 function checkifexist(valor) {
   var response = true;
   $(".floata").each(function(i , item){
    //  console.log(item.value+" - "+valor);
     if (item.value === valor) {
       response = false;
     }
   });
  return response;
 }

 $('.btn-click').on('click',function(e){
   var rowCount = $('#tb-vales tr').length;
   if (rowCount > 2) {
     $("#frmVales").submit();
   }else {
     $.toast({
       heading: 'Error',
       hideAfter: 5000,
       position: 'bottom-right',
       text: "Agregar al menos un vale",
       icon: 'error',
       showHideTransition: 'plain',
     });
   }
 });
