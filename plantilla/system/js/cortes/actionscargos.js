var cantidad = 0;
var precio = 0;

  $("#table-cargos tbody").on('click','tr .editCargo',function(e){
    if ($(this).hasClass("changing")) {
                var $parentRow = $(this).parents('tr').eq(0);
                $(this).removeClass("changing");
                $parentRow.removeClass("changing");
                $(this).html('<a class="editCargo" data-toggle="tooltip" title="Editar"><span class="glyphicon glyphicon-edit"></span></a>');
                $parentRow.find('.editable').attr("contenteditable","false");
            } else {
                var $parentRow = $(this).parents('tr').eq(0);
                var row = $(this).closest("tr");
                cantidad = row.find(".cantidad").text();
                precio = row.find(".precio").text();

                $(this).addClass("changing");
                $parentRow.addClass("changing");
                $(this).html('<a class="green" data-toggle="tooltip" title="Confirmar">Confirmar<span class="glyphicon glyphicon-check"></span></a>');
                $parentRow.find('.editable').attr("contenteditable", "true");
            }
  });

  $("#table-cargos tbody").on('click','tr .deleteCargo',function(e){
    var row = $(this).closest("tr");
    var folio = row.find(".folio").text();
    var column = $(this).parents('tr');
    bootbox.confirm({
        message: "¿Desea eliminar este producto?",
        buttons: {
            confirm: {
                label: 'Confirmar',
                className: 'btn-primary btn-sm'
            },
            cancel: {
                label: 'Cancelar',
                className: 'btn-default btn-sm'
            }
        },
        callback: function (result) {
          if (result) {
            var url = $("#base_url").val();
            $.post( url + "Cortes/Cargos/deleteCargo",{folio: folio,corte: $("#corte").val()}, function( data ) {
              if (data.error) {
                $.toast({
                  heading: 'Error',
                  hideAfter: 4000,
                  position: 'bottom-right',
                  text: data.msg,
                  icon: 'error'
                });
              }else {
                column.remove();
                $.toast({
                  heading: 'Alerta',
                  hideAfter: 4000,
                  position: 'bottom-right',
                  text: data.msg,
                  icon: 'success'
                });
              }
            },'json');
          }
        }
    });
  });

$("#table-cargos tbody").on('click','tr .green',function(e){
  var row = $(this).closest("tr");
  var folio = row.find(".folio").text();
  var nvacantidad = row.find(".cantidad");
  var monto = row.find(".monto");
  var nvoprecio = row.find(".precio");
  var origen = row.find(".origen");
  var replacemonto = nvacantidad.text().replace(/,/g , "");
  replacemonto = replacemonto.replace('$' , "");

  if (origen.text() == 0) {
      if (nvacantidad.text() !== cantidad && nvacantidad.text() > 0) {
          bootbox.confirm({
              message: "¿Desea editar este producto?",
              buttons: {
                  confirm: {
                      label: 'Confirmar',
                      className: 'btn-primary btn-sm'
                  },
                  cancel: {
                      label: 'Cancelar',
                      className: 'btn-default btn-sm'
                  }
              },
              callback: function (result) {
                if (result) {

                  var url = $("#base_url").val();
                  // alert("-> "+$("#poscargadespachador").val());
                    $.post( url + "Cortes/Cargos/editCargo",{folio: folio,corte: $("#corte").val(),cantidad: replacemonto,origen: 0,poscargadespachador: $("#poscargadespachador").val()}, function( data ) {
                      console.log(data);
                      if (data.error) {
                        $.toast({
                          heading: 'Error',
                          hideAfter: 4000,
                          position: 'bottom-right',
                          text: data.msg,
                          icon: 'error'
                        });
                        nvacantidad.text(cantidad);
                      }else {
                        monto.text("$"+data.msg.monto);
                        nvacantidad.text(data.msg.cantidad);
                        $.toast({
                          heading: 'Alerta',
                          hideAfter: 4000,
                          position: 'bottom-right',
                          text: "Cargo modificado",
                          icon: 'success'
                        });
                      }
                    },'json').fail(function(xhr, status, error) {
                        console.log(error);
                        $.toast({
                          heading: 'Error',
                          hideAfter: 4000,
                          position: 'bottom-right',
                          text: error,
                          icon: 'error'
                        });
                    });
                  }else {
                    nvacantidad.text(cantidad);
                  }

                }
          });
      }else {
        nvacantidad.text(cantidad);
      }
    }else {
      //alert(nvacantidad.text()+" "+cantidad+" "+nvoprecio.text()+" "+precio)
      if ((nvacantidad.text() != cantidad || nvoprecio.text() != precio) &&
         (nvacantidad.text() > 0 && nvoprecio.text() > 0)){

        bootbox.confirm({
            message: "¿Desea editar este producto?",
            buttons: {
                confirm: {
                    label: 'Confirmar',
                    className: 'btn-primary btn-sm'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-default btn-sm'
                }
            },
            callback: function (result) {
              if (result) {
                  var url = $("#base_url").val();
                    $.post( url + "Cargos/editCargo",{folio: folio,corte: $("#corte").val(),cantidad: nvacantidad.text(),origen: 1,precio: nvoprecio.text(),poscargadespachador: $("#poscargadespachador").val()}, function( data ) {
                      console.log(data);
                      if (data.error) {
                        $.toast({
                          heading: 'Error',
                          hideAfter: 4000,
                          position: 'bottom-right',
                          text: data.msg,
                          icon: 'error'
                        });
                      }else {
                        monto.text(data.msg.monto);
                        nvacantidad.text(data.msg.cantidad);
                        $.toast({
                          heading: 'Alerta',
                          hideAfter: 4000,
                          position: 'bottom-right',
                          text: "Cargo modificado",
                          icon: 'success'
                        });
                      }
                    },'json').fail(function(xhr, status, error) {
                        console.log(error);
                        $.toast({
                          heading: 'Error',
                          hideAfter: 4000,
                          position: 'bottom-right',
                          text: error,
                          icon: 'error'
                        });
                    });
                }else {
                  nvoprecio.text(precio);
                  nvacantidad.text(cantidad);
                }
              }
        });
      }
      else {
        nvoprecio.text(precio);
        nvacantidad.text(cantidad);
      }
    }
});

$('.float').keypress(function(event) {
  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
    event.preventDefault();
  }
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
