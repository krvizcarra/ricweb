$("#cortefrm").val($("#corte").val());
$("#poscargadespachadorfrm").val($("#poscargadespachador").val());
$("#parcialfrm").val($("#parcial").val());

$('.btn-click').on('click',function(e){
  $("#frmCheques").submit();
});

var count = 0;
$("#btn-add-cheque").on('click',function(e){
    var condicion = true;
    $("#frmAddCheq").bootstrapValidator('validate');
    var valid= $("#frmAddCheq").data('bootstrapValidator').isValid();

    var montoRest = parseFloat($("#montofaltante").attr('rest'));
    datos = {
      monto: $("#monto").val(),
      descripcion: $("#descripcion").val()
    }

    if (valid) {
      // if (montoRest >= sumColums()) {
        $('#tb-cheques tr:first').after(
          '<tr id="'+count+'">'+
            '<td><input type="text" class="monto form-control" style="" readonly name="cheques['+count+'][monto]" value="'+datos.monto+'"></td>'+
            '<td><input type="text" style="" class="form-control" readonly name="cheques['+count+'][descripcion]" value="'+datos.descripcion+'"></td>'+
            '<td "><a class="btn btn-link" style="border:none;" onclick="eliminarFila('+count+')" ><span class="glyphicon glyphicon-remove"></span></a></td>'+
          '</tr>')
          $('#frmAddCheq').data('bootstrapValidator').resetForm(true);
          count++;
    }
});

function eliminarFila(index)
{
  $("#"+index).remove();
}

$(".btn-click").attr("data-loading-text","<i class='fa fa-spinner fa-spin '></i> Registrando")

$("#frmCheques").on('submit',function(e){
  e.preventDefault();
  var url = $("#base_url").val();
  var form = $('form').serialize()
  var rowCount = $('#tb-cheques tr').length;

  if (rowCount > 2) {
    $(".btn-click").button('loading');
    $(".btn-cancel").attr("disabled","disabled");

    $.post( url + "Cortes/Pagos/pagoCheques" , form ,function( data ) {
      $(".btn-click").button('reset');
      $(".btn-cancel").removeAttr("disabled");
      if (data.error) {
        $.toast({
          heading: 'Error',
          hideAfter: 4000,
          position: 'bottom-right',
          text: data.msg,
          icon: 'error'
        });
      }else {
        bootbox.hideAll();
        $.toast({
          heading: 'Alerta',
          hideAfter: 4000,
          position: 'bottom-right',
          text: "Pago registrado",
          icon: 'success'
        });
        window.setTimeout(function(){ location.reload()}, 2000);
      }
    },"json").fail(function(xhr, status, error) {
        $(".btn-click").button('reset');
        $(".btn-cancel").removeAttr("disabled");
        $.toast({
          heading: 'Error',
          hideAfter: 4000,
          position: 'bottom-right',
          text: error,
          icon: 'error'
        });
    });
  }else {
    $.toast({
      heading: 'Error',
      hideAfter: 4000,
      position: 'bottom-right',
      text: "Agregar al menos un cheque",
      icon: 'error'
    });
  }
});
$('.float').keypress(function(event) {
  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
    event.preventDefault();
  }
});

function sumColums()
{
  var monto = $("#monto").val().replace(/,/g , "");
  $(".monto").each(function(i,item){
    monto = parseFloat(monto) + parseFloat(item.value.replace(/,/g , ""));
  });
  return monto;
}

$(document).ready(function() {
    $('#frmAddCheq').bootstrapValidator({
      framework: 'bootstrap',
      excluded: ':disabled',
      feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          monto:{
            validators:{
              greaterThan: {
                      value: 0.01,
                      message: 'Monto debe ser mayor a 0'
                  },
                notEmpty: {
                  message: 'Campo requerido'
                }
            }
          },
          descripcion:{
            validators:{
              stringLength: {
                      max: 500,
                      message: 'Maximo 500 caracteres'
                  },
                notEmpty: {
                  message: 'Campo requerido'
                }
            }
          }
        }
    });
});
