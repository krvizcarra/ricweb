
$('.single').parents('.bootbox').removeAttr('tabindex');
$(".single").select2({
  placeholder: "Selecciona",
  allowClear: true
});


$("#posicion").on('change',function(e){
  var datos = {posicion: $(this).val() ,corte: $("#corte").val()};
  var url = $("#base_url").val() + 'Cortes/Cargos/frmGasolinaAjax';
   if ($("#despachador").val() === null) {
     $.toast({
       heading: 'Error',
       hideAfter: 4000,
       position: 'bottom-right',
       text: "Seleccionar despachador",
       icon: 'error'
     });
   }else {
     $.post(url,datos,function(data){
       console.log(data);
       if (!data.error) {
         $.each(data.msg,function(i,item) {
           if (item.tipocargo == 1) {
             $('#tbodypos').append(
               '<tr class="'+item.posicion_carga+'">'+
                 '<input type="hidden" class="form-control" readonly name="productos['+item.posicion_carga+'][despachador]" value="'+$("#despachador").val()+'">'+
                 '<td>'+$("#despachador option:selected").text()+'</td>'+
                 '<input type="hidden" class="existe" value="'+item.posicion_carga+"magna"+'"/>'+
                 '<input type="hidden" class="form-control" readonly name="productos['+item.posicion_carga+'][corte]" value="'+$("#corte").val()+'"/>'+
                 '<input type="hidden" class="form-control" readonly name="productos['+item.posicion_carga+'][posicion]" value="'+item.posicionid+'">'+
                 '<td>'+item.posicion_carga+'</td>'+
                //  '<td>'+exhibidores+'</td>'+
                 '<td>'+item.cargo+'</td>'+
                 '<td>'+ item.litrosfin+'<input type="hidden" class="form-control" readonly name="productos['+item.posicion_carga+'][ltinimagna]" value="'+ item.litrosfin+'"></td>'+
                 '<td>'+ item.precios +'<input type="hidden" class="form-control cantidad" readonly name="productos['+item.posicion_carga+'][preciomagna]" value="'+ item.precios +'"></td>'+
                 '<td style="text-align:center"><a class="form-control"  style="border:none" onclick="eliminarFila('+item.posicion_carga+')" ><span class="glyphicon glyphicon-remove"></span></a></td>'+
               '</tr>'
             );
           }else if (item.tipocargo == 2) {
             $('#tbodypos').append(
               '<tr class="'+item.posicion_carga+'">'+
                 '<input type="hidden" class="form-control" readonly name="productos['+item.posicion_carga+'][despachador]" value="'+$("#despachador").val()+'">'+
                 '<td>'+$("#despachador option:selected").text()+'</td>'+
                 '<input type="hidden" class="existe" value="'+item.posicion_carga+"magna"+'"/>'+
                 '<input type="hidden" class="form-control" readonly name="productos['+item.posicion_carga+'][corte]" value="'+$("#corte").val()+'"/>'+
                 '<input type="hidden" class="form-control" readonly name="productos['+item.posicion_carga+'][posicion]" value="'+item.posicionid+'">'+
                 '<td>'+item.posicion_carga+'</td>'+
                //  '<td>'+exhibidores+'</td>'+
                 '<td>'+item.cargo+'</td>'+
                 '<td>'+ item.litrosfin+'<input type="hidden" class="form-control" readonly name="productos['+item.posicion_carga+'][ltinipremium]" value="'+ item.litrosfin+'"></td>'+
                 '<td>'+ item.precios +'<input type="hidden" class="form-control cantidad" readonly name="productos['+item.posicion_carga+'][preciopremium]" value="'+ item.precios +'"></td>'+
                 '<td style="text-align:center"><a class="form-control"  style="border:none" onclick="eliminarFila('+item.posicion_carga+')" ><span class="glyphicon glyphicon-remove"></span></a></td>'+
               '</tr>'
             );
           }else if (item.tipocargo == 3) {
             $('#tbodypos').append(
               '<tr class="'+item.posicion_carga+'">'+
                 '<input type="hidden" class="form-control" readonly name="productos['+item.posicion_carga+'][despachador]" value="'+$("#despachador").val()+'">'+
                 '<td>'+$("#despachador option:selected").text()+'</td>'+
                 '<input type="hidden" class="existe" value="'+item.posicion_carga+"magna"+'"/>'+
                 '<input type="hidden" class="form-control" readonly name="productos['+item.posicion_carga+'][corte]" value="'+$("#corte").val()+'"/>'+
                 '<input type="hidden" class="form-control" readonly name="productos['+item.posicion_carga+'][posicion]" value="'+item.posicionid+'">'+
                 '<td>'+item.posicion_carga+'</td>'+
                //  '<td>'+exhibidores+'</td>'+
                 '<td>'+item.cargo+'</td>'+
                 '<td>'+ item.litrosfin+'<input type="hidden" class="form-control" readonly name="productos['+item.posicion_carga+'][ltinidiesel]" value="'+ item.litrosfin+'"></td>'+
                 '<td>'+ item.precios +'<input type="hidden" class="form-control cantidad" readonly name="productos['+item.posicion_carga+'][preciodiesel]" value="'+ item.precios +'"></td>'+
                 '<td style="text-align:center"><a class="form-control"  style="border:none" onclick="eliminarFila('+item.posicion_carga+')" ><span class="glyphicon glyphicon-remove"></span></a></td>'+
               '</tr>'
             );
           }

         });
       }else {
         $.toast({
           heading: 'Error',
           hideAfter: 4000,
           position: 'bottom-right',
           text: data.msg,
           icon: 'error'
         });
       }
     },'json').fail(function(xhr, status, error) {
         console.log(error);
         $.toast({
           heading: 'Error',
           hideAfter: 4000,
           position: 'bottom-right',
           text: error,
           icon: 'error'
         });
     });
   }
});

$("#frmDespachador").on('submit',function(e){
    e.preventDefault();
    var rowCount = $('#tb-posiciones-add tr').length;
    if (rowCount > 2) {
      var url = $("#base_url").val();
      var form = $('form').serialize()
      // $.post( url + "Cortes/Cargos/post" , form ,function( data ) {
      $.post( url + "Cortes/Despachadores/registrarDespachadorPorCorte" , form ,function( data ) {
        console.log(data);
        if (data.error) {
          $.toast({
            heading: 'Error',
            hideAfter: 4000,
            position: 'bottom-right',
            text: data.msg,
            icon: 'error'
          });
        }else {
          $.each(data.msg, function(i,item){
            if (item.error) {
              $.toast({
                heading: 'Error',
                hideAfter: 4000,
                position: 'bottom-right',
                text: item.msg,
                icon: 'error'
              });
            }else {
              bootbox.hideAll();
              $.toast({
                heading: 'Alerta',
                hideAfter: 4000,
                position: 'bottom-right',
                text: "Posiciones de carga asignadas",
                icon: 'success'
              });
            }
            // alert(url +'Cargos/cargos'+" "+cortevar)
            if (data.datos.length > 0) {
              bootbox.hideAll();
              $("#tb-despachadores tbody tr").remove();
              $("#tb-despachadores tbody").append('<tr></tr>');
              var tables = $('.dataTable').DataTable();
              var allData = tables.tables();
              allData.clear().draw();
              $.each(data.datos,function(i,item){
                allData.row.add([
                   item.depachador_id, item.nombrecompleto,"$"+item.cargos,"$"+item.pagos,item.posiciones,
                   '<form action="'+url +'Cortes/Cargos/Parciales'+'" method="post">'+
                     '<input type="hidden" name="corte" id="corteD"  value="'+cortevar+'">'+
                     '<input type="hidden" name="nombre" value="'+item.nombrecompleto+'">'+
                     '<input type="hidden" name="pc" value="'+item.posiciones+'">'+
                     '<input type="hidden" name="despachador" value="'+item.depachador_id+'">'+
                     '<button onclick="$(this).closest(\'form\').submit()" data-toggle="tooltip" title="Modificar despachador" type="button" style="margin-right:1%" class="btn btn-primary btn-xs" name="button"><span class="glyphicon glyphicon-edit"></span></button>'+
                     '<button data-toggle="tooltip" title="Eliminar despachador" type="button" valor="'+item.depachador_id+'" style="margin-right:1%" class="btn btn-warning btn-xs deletePosicion" name="button"><span class="glyphicon glyphicon-trash"></span></button>'+
                   '</form>'
                ]).draw().node();
              });
            }
          });
        }
      },"json").fail(function(xhr, status, error) {
          console.log(error);
          $.toast({
            heading: 'Error',
            hideAfter: 4000,
            position: 'bottom-right',
            text: error,
            icon: 'error'
          });
      });
    }else {
      $.toast({
        heading: 'Error',
        hideAfter: 4000,
        position: 'bottom-right',
        text: "Ingrese a menos una posicion de carga",
        icon: 'error'
      });
    }
});


function eliminarFila(index)
{
    $("."+index).remove();
}
