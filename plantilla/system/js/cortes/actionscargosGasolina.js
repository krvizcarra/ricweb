var ltini = 0;
var ltfin = 0;
var precio = 0;

  $("#table-cargos tbody").on('click','tr .editCargo',function(e){
    if ($(this).hasClass("changing")) {
                var $parentRow = $(this).parents('tr').eq(0);
                $(this).removeClass("changing");
                $parentRow.removeClass("changing");
                $(this).html('<a class="editCargo" data-toggle="tooltip" title="Editar"><span class="glyphicon glyphicon-edit"></span></a>');
                $parentRow.find('.editable').attr("contenteditable","false");
            } else {
                var $parentRow = $(this).parents('tr').eq(0);
                var row = $(this).closest("tr");
                ltini = row.find(".ltini").text();
                ltfin = row.find(".ltfin").text();
                precio = row.find(".precio").text();
                $(this).addClass("changing");
                $parentRow.addClass("changing");
                $(this).html('<a class="green" data-toggle="tooltip" title="Confirmar">Confirmar<span class="glyphicon glyphicon-check"></span></a>');
                $parentRow.find('.editable').attr("contenteditable", "true");
            }
  });

  $("#table-cargos tbody").on('click','tr .deleteCargo',function(e){
    var row = $(this).closest("tr");
    var folio = row.find(".folio").text();
    var column = $(this).parents('tr');
    var nvocantidad = row.find(".cantidad");
    var nvoltfin = row.find(".ltfin");
    var monto = row.find(".monto");

    bootbox.confirm({
        message: "¿Desea eliminar este producto?",
        buttons: {
            confirm: {
                label: 'Confirmar',
                className: 'btn-primary btn-sm'
            },
            cancel: {
                label: 'Cancelar',
                className: 'btn-default btn-sm'
            }
        },
        callback: function (result) {
          if (result) {
            var url = $("#base_url").val();
            $.post( url + "Cortes/Cargos/deleteCargo",{folio: folio,corte: $("#corte").val(),origen: 1}, function( data ) {
              // console.log(data);
              if (data.error) {
                $.toast({
                  heading: 'Error',
                  hideAfter: 4000,
                  position: 'bottom-right',
                  text: data.msg,
                  icon: 'error'
                });
              }else {
                // column.remove();
                nvocantidad.text("$0.00");
                nvoltfin.text("$0.00");
                monto.text("$0.00");
                $.toast({
                  heading: 'Alerta',
                  hideAfter: 4000,
                  position: 'bottom-right',
                  text: data.msg,
                  icon: 'success'
                });
              }
            },'json');
          }
        }
    });
  });

$("#table-cargos tbody").on('click','tr .green',function(e){
  console.log("entre");

  var row = $(this).closest("tr");
  var folio = row.find(".folio").text();
  var nvocantidad = row.find(".cantidad");
  var nvoltfin = row.find(".ltfin");
  var nvoltini = row.find(".ltini");
  var monto = row.find(".monto");
  var nvoprecio = row.find(".precio");
  // var ltini = row.find(".ltini");

  // console.log(ltini.text() + " <=> " +nvoltfin.text());
      //alert(nvoltfin.text()+" "+ltfin+" "+nvoprecio.text()+" "+precio)

      // console.log(ltini+" <-> "+nvoltini.text());
      // console.log(ltfin+" <-> "+nvoltfin.text());
      // console.log(parseFloat(replaceAll(nvoltini.text(),',','')) +" <=> "+ parseFloat(replaceAll(nvoltfin.text(),',','')));

      if ( ( (nvoltfin.text() != ltfin) || (nvoltini.text() != ltini)) &&
          (parseFloat(replaceAll(nvoltfin.text(),',','')) > 0) &&
          (parseFloat(replaceAll(nvoltini.text(),',','')) <= parseFloat(replaceAll(nvoltfin.text(),',','')))
        )
         {

        var editdata = {
          folio: folio,
          corte: $("#corte").val(),
          ltfin: nvoltfin.text(),
          ltini: nvoltini.text(),
          origen: 1,precio: nvoprecio.text(),
          poscargadespachador: $("#poscargadespachador").val()
        }
        bootbox.confirm({
            message: "¿Desea editar este producto?",
            buttons: {
                confirm: {
                    label: 'Confirmar',
                    className: 'btn-primary btn-sm'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-default btn-sm'
                }
            },
            callback: function (result) {
              if (result) {
                  var url = $("#base_url").val();
                  //Cargos/editCargo
                    $.post( url + "Cortes/Cargos/editCargo",editdata, function( data ) {
                      console.log(data);
                      if (data.error) {
                        $.toast({
                          heading: 'Error',
                          hideAfter: 4000,
                          position: 'bottom-right',
                          text: data.msg,
                          icon: 'error'
                        });
                      }else {
                        monto.text("$" + data.msg.monto);
                        nvocantidad.text(data.msg.cantidad);
                        nvoltfin.text(nvoltfin.text());
                        nvoltini.text(nvoltini.text());

                        $.toast({
                          heading: 'Alerta',
                          hideAfter: 4000,
                          position: 'bottom-right',
                          text: "Lecturas modificadas",
                          icon: 'success'
                        });
                      }
                    },'json').fail(function(xhr, status, error) {
                        console.log(error);
                        $.toast({
                          heading: 'Error',
                          hideAfter: 4000,
                          position: 'bottom-right',
                          text: error,
                          icon: 'error'
                        });
                    });
                }else {
                  nvoprecio.text(precio);
                  nvoltfin.text(ltfin);
                  nvoltini.text(ltini);
                }
              }
        });
      }
      else {
        $.toast({
          heading: 'Error',
          hideAfter: 4000,
          position: 'bottom-right',
          text: "Datos incorrectos",
          icon: 'error'
        });
        // nvoprecio.text(precio);
        // nvoltfin.text(ltfin);
        // nvoltini.text(ltini);
      }

});

$('.float').keypress(function(event) {
  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
    event.preventDefault();
  }
});

function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}
