
$(document).ready(function() {
    $("#txtProductos").select2({ width: '100%'});
    $('#txtProductos').parents('.bootbox').removeAttr('tabindex');
});

$("#cortefrm").val($("#corte").val());
$("#poscargadespachadorfrm").val($("#poscargadespachador").val());

$("#txtProductos").on('change',function(e){
  var text = $("#txtProductos option:selected").text();
  var val = $("#txtProductos option:selected").val();
  var monto = $("#txtProductos option:selected").attr('monto');

  var select = '<select class="form-control" name="productos['+val+'][exhibidor]">';
  $.each(exhibidores,function(i,item){
    select += '<option value="'+item.id+'">'+item.nombre+'</option>';
  });
  select += '</select>';

  if($("#"+val).length === 0)
  {
    $('#tb-productos tr:first').after(
      '<tr id="'+val+'">'+
      '<td ><input type="hidden" class="form-control" name="productos['+val+'][producto]" value="'+val+'" >'+text+'</td>'+
      '<td ><input type="number" class="form-control cantidad" name="productos['+val+'][cantidad]" value="1"></td>'+
      '<td ><input type="text" class="form-control" readonly name="productos['+val+'][monto]" value="'+monto+'"></td>'+
      '<td style="width: 150px">'+select+'</td>'+
      '<td style="text-align:center"><a class="form-control"  style="border:none" onclick="eliminarFila('+val+')" ><span class="glyphicon glyphicon-remove"></span></a></td>'+
      '</tr>'
    )
  }
});

$(document).on("keyup", ".cantidad" ,function() {
  if($(this).val() == 0)
  {
    $(this).val(1);
  }
});

function eliminarFila(index)
{
    $("#"+index).remove();
}
$(".btn-click").on('click',function(e){
  $("#frmAceites").submit();
});
$(".btn-click").attr("data-loading-text","<i class='fa fa-spinner fa-spin '></i> Registrando")

$("#frmAceites").on('submit',function(e){
  e.preventDefault();
  $(".btn-click").button('loading');
  $(".btn-cancel").attr("disabled","disabled");

  var url = $("#base_url").val();
  var form = $('form').serialize()
  $.post( url + "Cortes/Cargos/cargoAceites" , form ,function( data ) {
    $(".btn-click").button('reset');
    $(".btn-cancel").removeAttr("disabled");
    if (data.error) {
      $.toast({
        heading: 'Error',
        hideAfter: 4000,
        position: 'bottom-right',
        text: data.msgerr,
        icon: 'error'
      });
    }else {
      bootbox.hideAll();
      $.toast({
        heading: 'Alerta',
        hideAfter: 4000,
        position: 'bottom-right',
        text: "Cargo registrado",
        icon: 'success'
      });
      window.setTimeout(function(){ location.reload()}, 2000);
    }
  },"json").fail(function(xhr, status, error) {
      $(".btn-click").button('reset');
      $(".btn-cancel").removeAttr("disabled");
      $.toast({
        heading: 'Error',
        hideAfter: 4000,
        position: 'bottom-right',
        text: error,
        icon: 'error'
      });
  });
});
