$("#cortefrm").val($("#corte").val());
$("#poscargadespachadorfrm").val($("#poscargadespachador").val());
$(".btn-click").attr('disabled','disabled');
$('.float').keypress(function(event) {
  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
    event.preventDefault();
  }
});
$(".btn-click").on('click',function(e){

  $("#frmAddGas").bootstrapValidator('validate');
  // $(".litrosfinales").input('trigger');
  // $(".lecsys").input('trigger');
  var condicion2= $("#frmAddGas").data('bootstrapValidator').isValid();
  if (condicion2) {
        var url = $("#base_url").val();
        var form = $('#frmAddGas').serialize();
            var modal = bootbox.dialog({
            closeButton: false,
            message: $(crearProgressBar())
            });

            $.post( url + "Cortes/Cargos/cargoGasolina" ,form,function( data ) {
                console.log(data);
                modal.modal('hide');
                if (data.error == true)
                {
                  console.log(data.msg);
                  $.toast({
                    heading: 'Error',
                    hideAfter: 4000,
                    position: 'bottom-right',
                    text: data.msg,
                    icon: 'error'
                  });
                }
                else
                {
                  bootbox.hideAll();
                  $.toast({
                    heading: 'Alerta',
                    hideAfter: 4000,
                    position: 'bottom-right',
                    text: "Cargo registrado",
                    icon: 'success'
                  });
                 window.setTimeout(function(){ location.reload()}, 2000);
                }
            },"json").fail(function(xhr, status, error) {
                console.log(error);
                modal.modal('hide');
                $.toast({
                  heading: 'Error',
                  hideAfter: 4000,
                  position: 'bottom-right',
                  text: error,
                  icon: 'error'
                });
              });
    }else {
      console.log("no valido");
    }
});

function crearProgressBar(color = '')
{
  var html = '<div class="progress" style="margin-top:5%"> <div class="progress-bar progress-bar-striped '+color+' active" '+
  'role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">'+
  'Cargando </div></div>'
  return html;
}



$(document).ready(function() {
  magna = {} ;
  premium = {} ;
  diesel = {} ;
  lecmagna = {} ;
  lecpremium = {} ;
  lecdiesel = {} ;

  $.each(folios,function(i,item){

    $("#ltfinmagna"+item.posicion).on('keyup',function(e) {
      var ini = document.getElementById('productos['+item.posicion+'][ltinimagna]').value;
      ini = ini.replace(/,/g , "")
      var result = (parseFloat($(this).val()) - parseFloat(ini));
      $("#cantidadlitrosmagna"+item.posicion).val(numberWithCommas(result.toFixed(2)));
    });

    $("#ltfinpremium"+item.posicion).on('keyup',function(e) {
      var ini = document.getElementById('productos['+item.posicion+'][ltinipremium]').value;
      ini = ini.replace(/,/g , "")
      var result = (parseFloat($(this).val()) - parseFloat(ini));
      $("#cantidadlitrospremium"+item.posicion).val(numberWithCommas(result.toFixed(2)));
    });

    $("#ltfindiesel"+item.posicion).on('keyup',function(e) {
      var ini = document.getElementById('productos['+item.posicion+'][ltinidiesel]').value;
      ini = ini.replace(/,/g , "")
      var result = (parseFloat($(this).val()) - parseFloat(ini));
      $("#cantidadlitrosdiesel"+item.posicion).val(numberWithCommas(result.toFixed(2)));
    });

    if (item.requerido == 1) {
      lecmagna['productos['+item.posicion+'][lecturamagna]'] = {
           validators:{
             notEmpty: {
               message: 'Consultar lecturas por sistema'
             },
             callback: {
                 message: 'lecturas no cuadran',
                 callback: function (value, validator, $field) {
                     var ini = document.getElementById('productos['+item.posicion+'][ltinimagna]').value;
                     var fin = document.getElementsByName('productos['+item.posicion+'][ltfinmagna]')[0].value;
                     ini = ini.replace(/,/g , '');
                     fin = fin.replace(/,/g , '');
                     value = value.replace(/,/g , '');
                     var cantidad = (parseFloat(fin) - parseFloat(ini).toFixed(2)).toFixed(2) ;
                     var diferiencia = (cantidad - parseFloat(value).toFixed(2)).toFixed(2) ;
                     if (diferiencia > 1 || diferiencia < -1) {
                        $('.alert').show();
                        return false;
                      }else {
                        $('.alert').css({'display':'none'});
                        return true;
                      }
                 }
             }
          }
        };

      lecpremium['productos['+item.posicion+'][lecturapremium]'] = {
           validators:{
             notEmpty: {
               message: 'Consultar lecturas por sistema'
             },
             callback: {
                 message: 'lecturas no cuadran',
                 callback: function (value, validator, $field) {
                     var ini = document.getElementById('productos['+item.posicion+'][ltinipremium]').value;
                     var fin = document.getElementsByName('productos['+item.posicion+'][ltfinpremium]')[0].value;
                     ini = ini.replace(/,/g , '');
                     fin = fin.replace(/,/g , '');
                     value = value.replace(/,/g , '');
                     var cantidad = (parseFloat(fin) - parseFloat(ini).toFixed(2)).toFixed(2) ;
                     var diferiencia = (cantidad - parseFloat(value).toFixed(2)).toFixed(2) ;
                     if (diferiencia > 1 || diferiencia < -1) {
                       //  $('.alert').show();
                        return false;
                      }else {
                       //  $('.alert').css({'display':'none'});
                        return true;
                      }
                 }
             }
          }
        };

       lecdiesel['productos['+item.posicion+'][lecturadiesel]'] = {
            validators:{
              notEmpty: {
                message: 'Consultar lecturas por sistema'
              },
               callback: {
                   message: 'lecturas no cuadran',
                   callback: function (value, validator, $field) {
                       var ini = document.getElementById('productos['+item.posicion+'][ltinidiesel]').value;
                       var fin = document.getElementsByName('productos['+item.posicion+'][ltfindiesel]')[0].value;
                       ini = ini.replace(/,/g , '');
                       fin = fin.replace(/,/g , '');
                       value = value.replace(/,/g , '');
                       var cantidad = (parseFloat(fin) - parseFloat(ini).toFixed(2)).toFixed(2) ;
                       var diferiencia = (cantidad - parseFloat(value).toFixed(2)).toFixed(2) ;
                        if (diferiencia > 1 || diferiencia < -1) {
                          $('.alert').show();
                          return false;
                        }else {
                          $('.alert').css({'display':'none'});
                          return true;
                        }
                   }
               }
           }
         };
    }
     magna['productos['+item.posicion+'][ltfinmagna]'] = {
        validators:{
          notEmpty: {
            message: 'Campo requerido'
          },
          callback: {
              message: 'Debe ser mayor o igual a inicio litros',
              callback: function (value, validator, $field) {
                  var ini = document.getElementById('productos['+item.posicion+'][ltinimagna]').value;
                  ini = ini.replace(/,/g , '');
                  value = value.replace(/,/g , '');
                  if (parseFloat(ini) > parseFloat(value)) {
                        return false;
                  }else {
                    return true;
                  }
              }
          }
       }
     };

      premium['productos['+item.posicion+'][ltfinpremium]'] = {
        validators:{
          notEmpty: {
            message: 'Campo requerido'
          },
          callback: {
              message: 'Debe ser mayor o igual a inicio litros',
              callback: function (value, validator, $field) {
                  var ini = document.getElementById('productos['+item.posicion+'][ltinipremium]').value;
                  ini = ini.replace(/,/g , '');
                  value = value.replace(/,/g , '');
                  if (parseFloat(ini) > parseFloat(value)) {
                        return false;
                  }else {
                    return true;
                  }
              }
          }
        }
      };
      diesel['productos['+item.posicion+'][ltfindiesel]'] = {
        validators:{
          notEmpty: {
            message: 'Campo requerido'
          },
          callback: {
              message: 'Debe ser mayor o igual a inicio litros',
              callback: function (value, validator, $field) {
                  var ini = document.getElementById('productos['+item.posicion+'][ltinidiesel]').value;
                  ini = ini.replace(/,/g , '');
                  value = value.replace(/,/g , '');
                  if (parseFloat(ini) > parseFloat(value)) {
                        return false;
                  }else {
                    return true;
                  }
              }
          }
        }
      };
  });

  campos = {};
  $.extend(campos,lecmagna,lecpremium,lecdiesel,magna, premium,diesel);

  $('#frmAddGas').bootstrapValidator({
    framework: 'bootstrap',
    excluded: ':disabled',
    feedbackIcons: {
          // valid: 'glyphicon glyphicon-ok',
          // invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: campos
  });

});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/*$(".EntTab").on("keypress", function(e) {

       if (e.keyCode == 13) {
           var inps = $("input, select"); //add select too
           for (var x = 0; x < inps.length; x++) {
               if (inps[x] == this) {
                   while ((inps[x]).name == (inps[x + 1]).name) {
                   x++;
                   }
                   if ((x + 1) < inps.length) $(inps[x + 1]).focus();
               }
           }   e.preventDefault();
       }
   });

$('.float').keypress(function(event) {
  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
    event.preventDefault();
  }
});*/
