$(document).ready(function(){
  $('#tb-cargos').DataTable({
                pageLength: 10,
                data: adatacargos,
                columns: [
                    { title: "Id" , className: "folio"},
                    { title: "Nombre" , className: "nombre"},
                    { title: "Cuenta" , className: "cuenta"},
                    { title: "Segmento" , className: "segmento"},
                    { title: "Opcines" , className: ""},

                ],
                responsive: true,
                bLengthChange: false,
                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                columnDefs: [ {
                    "targets": -1,
                    // "data": null,
                    "sClass": "text-center",

                } ],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

            $('#tb-abonos').DataTable({
                          pageLength: 10,
                          data: adataabonos,
                          columns: [
                              { title: "Id" , className: "folio"},
                              { title: "Nombre" , className: "nombre"},
                              { title: "Cuenta" , className: "cuenta"},
                              { title: "Segmento" , className: "segmento"},
                              { title: "Opcines" , className: ""},

                          ],
                          responsive: true,
                          bLengthChange: false,
                          language: {
                              "sProcessing":     "Procesando...",
                              "sLengthMenu":     "Mostrar _MENU_ registros",
                              "sZeroRecords":    "No se encontraron resultados",
                              "sEmptyTable":     "Ningún dato disponible en esta tabla",
                              "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                              "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                              "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                              "sInfoPostFix":    "",
                              "sSearch":         "Buscar:",
                              "sUrl":            "",
                              "sInfoThousands":  ",",
                              "sLoadingRecords": "Cargando...",
                              "oPaginate": {
                                  "sFirst":    "Primero",
                                  "sLast":     "Último",
                                  "sNext":     "Siguiente",
                                  "sPrevious": "Anterior"
                              },
                              "oAria": {
                                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                              }
                          },
                          columnDefs: [ {
                              "targets": -1,
                              // "data": null,
                              "sClass": "text-center",

                          } ],
                          dom: '<"html5buttons"B>lTfgitp',
                          buttons: [
                              { extend: 'copy'},
                              {extend: 'excel', title: 'ExampleFile'},
                              {extend: 'pdf', title: 'ExampleFile'},

                              {extend: 'print',
                               customize: function (win){
                                      $(win.document.body).addClass('white-bg');
                                      $(win.document.body).css('font-size', '10px');

                                      $(win.document.body).find('table')
                                              .addClass('compact')
                                              .css('font-size', 'inherit');
                              }
                              }
                          ]

                      });

                      $('#formCuenta').bootstrapValidator({
                               message: 'Campo requerido',
                                  feedbackIcons: {
                                      // valid: 'glyphicon glyphicon-ok',
                                      // invalid: 'glyphicon glyphicon-remove',
                                      validating: 'glyphicon glyphicon-refresh'
                              },
                              // This option will not ignore invisible fields which belong to inactive panels
                              //excluded: ':disabled',
                              fields: {
                                  InputCuenta: {
                                      validators: {
                                          notEmpty: {
                                              message: 'Cuenta requerido'
                                          }
                                      }
                                  },
                                  InputNombre: {
                                      validators: {
                                          notEmpty: {
                                              message: 'Nombre requerido'
                                          }
                                      }
                                  }
                              }
                          });
  });

  $("#btn-agregarcargo").on('click',function(e){
    $("#myModal").modal('toggle');
    $('#modal-title').text('Agregar cuenta');
    $('#formCuenta')[0].reset();
    $('#InputId').val(0);
    $('#InputTipo').val(2);
    // $('#formCuenta').data('bootstrapValidator').resetForm();

  });

  $("#btn-agregarabono").on('click',function(e){
    $("#myModal").modal('toggle');
    $('#modal-title').text('Agregar cuenta');
    $('#formCuenta')[0].reset();
    $('#InputId').val(0);
    $('#InputTipo').val(1);
    // $('#formCuenta').data('bootstrapValidator').resetForm();

  });

  $("#btnGuardar").on('click',function(e){
    e.preventDefault();
    $("#formCuenta").bootstrapValidator('validate');
    var valid= $("#formCuenta").data('bootstrapValidator').isValid();
    if (valid) {
      var url = $("#base_url").val();
      var form = $('#formCuenta').serialize();
      $.post( "guardarCuenta", form ,function( data ) {
        console.log(data);
        if (data.error) {
          $.toast({
            heading: 'Error',
            hideAfter: 4000,
            position: 'bottom-right',
            text: data.msg,
            icon: 'error'
          });
        }else {
          $.toast({
            heading: 'Alerta',
            hideAfter: 4000,
            position: 'bottom-right',
            text: data.msg ,
            icon: 'success'
          });
          row.find(".nombre").text(data.datos.InputNombre.toUpperCase());
          row.find(".cuenta").text(data.datos.InputCuenta);
          row.find(".segmento").text(data.datos.InputSegmento);
          $('#myModal').modal('toggle');
        }
      },"json").fail(function(xhr, status, error) {
          console.log(error);
          $.toast({
            heading: 'Error',
            hideAfter: 4000,
            position: 'bottom-right',
            text: error,
            icon: 'error'
          });
      });
    }
  });
  row ="";
  $(document).on('click',".btn-edit",function(e){
      $('#modal-title').text('Editar cuenta');      
      row = $(this).closest("tr");
      let id = row.find(".folio").text();
      let nombre = row.find(".nombre").text();
      let cuenta = row.find(".cuenta").text();
      let segmento = row.find(".segmento").text();

      $('#InputTipo').val($(this).attr('tipo'));
      $('#InputId').val(id);
      $('#InputNombre').val(nombre);
      $('#InputCuenta').val(cuenta);
      $('#InputSegmento').val(segmento);
      $("#myModal").modal('toggle');
  });
