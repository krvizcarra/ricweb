$(function(){
  $('.modal').on('shown.bs.modal', function () {
    $('.fecha1').datetimepicker({
      format:'YYYY-MM-DD'
    });
  });

    tablepolizas = $('#tb-polizas').DataTable( {
          data: adata,
          columns: [
              { title: "Id" , className: "folio"},
              { title: "Codigo" , className: "codigo"},
              { title: "Fecha" , className: "fecha"},
              { title: "Importe" , className: ""},
              { title: "Liquidado", className: "" },
              { title: "Estado", className: "dt-center" },
              { title: "Opción",className: "dt-center"},
          ],
          order : [ 0, "desc" ],
          responsive: true,
          fnDrawCallback: function() {
            $('.toggle-estado').bootstrapToggle({
                  on: 'Cuadrada',
                  off: 'Abierta',

            });
          },
          bLengthChange: false,
          language: {
              "sProcessing":     "Procesando...",
              "sLengthMenu":     "Mostrar _MENU_ registros",
              "sZeroRecords":    "No se encontraron resultados",
              "sEmptyTable":     "Ningún dato disponible en esta tabla",
              "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix":    "",
              "sSearch":         "Buscar:",
              "sUrl":            "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "Último",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
          },

          dom: '<"html5buttons"B>lTfgitp',
          buttons: [
              { extend: 'copy'},
              {extend: 'excel', title: 'ExampleFile'},
              {extend: 'pdf', title: 'ExampleFile'},

              {extend: 'print',
               customize: function (win){
                      $(win.document.body).addClass('white-bg');
                      $(win.document.body).css('font-size', '10px');

                      $(win.document.body).find('table')
                              .addClass('compact')
                              .css('font-size', 'inherit');
              }
              }
          ]
      } );

  $(document).on('click',".btn-edit",function(e){
    var row = $(this).closest("tr");
    var folio = row.find(".folio").text();
    var fecha = row.find(".fecha").text();
    var codigo = row.find(".codigo").text();
    location.href = $('#base_url').val() + 'Polizas/Apps/modificarpoliza?folio='+folio+'&fecha='+fecha+'&codigo='+codigo;
   });

  $(document).on('click',".btn-conf",function(e){
    $("#modaledit").modal('toggle');
    var row = $(this).closest("tr");
    var fecha = row.find(".fecha").text();
    var codigo = row.find(".codigo").text();
    var folio = row.find(".folio").text();
    $("#fecha").val(fecha);
    $("#codigo").val(codigo);
    $("#folio").val(folio);

  });

  $(document).on('click',".btn-print",function(e){
    var row = $(this).closest("tr");
    var folio = row.find(".folio").text();
    // alert(folio);
    var url = $("#base_url").val();
    window.open(url + 'Polizas/Apps/imprimirPoliza?id='+folio,'_blank');
    // var fecha = row.find(".fecha").text();
    // var codigo = row.find(".codigo").text();
    // location.href = $('#base_url').val() + 'Polizas/Apps/modificarpoliza?folio='+folio+'&fecha='+fecha+'&codigo='+codigo;
   });


   $(document).on('click',".btn-descargar",function(e){
     var row = $(this).closest("tr");
     var folio = row.find(".folio").text();
     var url = $("#base_url").val();
     window.open(url + 'Polizas/Apps/descargarPolizaTxt?id='+folio,'_blank');

    });

  $("#frmPolizaEdit").on('submit',function(e) {
    e.preventDefault();
    var form = $(this);
    var url = form.attr('action');
    var datos = form.serialize();
    var id = $("#folio").val();

    $.post(url,datos,function(data){
      if (data.error) {
        $.toast({
          heading: 'Error', hideAfter: 4000,
          position: 'bottom-right', text: data.msg,
          icon: 'error'  });
      }else {
        $("#modaledit").modal('toggle');
        $.toast({
          heading: 'Alerta', hideAfter: 4000,
          position: 'bottom-right', text: data.msg,
          icon: 'success'  });

          $("td").each(function(){
            if ($(this).text() == id) {
                var row = $(this).closest("tr");
                row.find(".codigo").text($("#codigo").val());
                row.find(".fecha").text($("#fecha").val());
              }
           });
      }
    },'json').fail(function(e,x,error){
      $.toast({
        heading: 'Error', hideAfter: 4000,
        position: 'bottom-right', text: error,
        icon: 'error'  });
    });

  });

  $(document).on('click',".btn-delete",function(e){
    var row = $(this).closest("tr");
    var datos = {folio: row.find(".folio").text()};
    var column = $(this).parents('tr');
    bootbox.confirm({
      message: "¿Desea eliminar la poliza "+row.find(".codigo").text()+ " ?",
      buttons: {
          confirm: {
              label: '<i class="fa fa-save"></i> Eliminar',
              className: 'btn-primary'
          },
          cancel: {
              label: '<i class="fa fa-remove"></i> Cancelar',
              className: 'btn-default'
          }
      },
      callback: function (result) {
          if (result) {
            var url = $('#base_url').val() + 'Polizas/Apps/eliminarPoliza';
            $.post(url,datos,function(data){
              if (data.error) {
                $.toast({
                  heading: 'Error', hideAfter: 4000,
                  position: 'bottom-right', text: data.msg,
                  icon: 'error'  });
              }else {
                tablepolizas.row(column).remove().draw( false );
                $.toast({
                  heading: 'Alerta', hideAfter: 4000,
                  position: 'bottom-right', text: data.msg,
                  icon: 'success'  });
              }
            },'json').fail(function(e,x,error){
              $.toast({
                heading: 'Error', hideAfter: 4000,
                position: 'bottom-right', text: error,
                icon: 'error'  });
            });
          }
      }
    });
  });

  $("#btn-new").on('click',function(e){
    $("#modalnew").modal('toggle');
  });

  $("#frmPoliza").on('submit',function(e){
    e.preventDefault();
    var form = $(this);
    var url = form.attr('action');
    var datos = form.serialize();

    $.post(url,datos,function(data){
      console.log(data);
      if (data.error) {
        $.toast({
          heading: 'Error', hideAfter: 4000,
          position: 'bottom-right', text: data.msg,
          icon: 'error'  });
      }else {
        $("#modalnew").modal('toggle');
        tablepolizas.row.add([
           data.msg.id, $("#codigon").val() , $("#fechan").val(),
           "$0.00","$0.00",
           '<input type="checkbox"  class="toggle-estado" data-size="mini" data-onstyle="primary" data-offstyle="warning">',
           '<button type="button" class="btn btn-xs btn-primary btn-edit"><span class="glyphicon glyphicon-pencil"></span></button>'+
           '<button type="button" class="btn btn-xs btn-warning btn-conf" style="margin-left:2%;"><span class="glyphicon glyphicon-wrench"></span></button>'+
           '<button type="button" class="btn btn-xs btn-danger btn-delete" style="margin-left:2%;"><span class="glyphicon glyphicon-trash"></span></button>'
        ]).draw().node();


        $(".toggle-estado").bootstrapToggle()

        $.toast({
          heading: 'Alerta', hideAfter: 4000,
          position: 'bottom-right', text: "Poliza registrada.",
          icon: 'success'  });
      }
    },'json').fail(function(e,x,error){
      $.toast({
        heading: 'Error', hideAfter: 4000,
        position: 'bottom-right', text: error,
        icon: 'error'  });
    });
  });

  $(document).on('change',".toggle-estado",function(e){
      var check = $(this);
      var url = $("#base_url").val();
      var row = $(this).closest("tr");
      folio = row.find(".folio").text();
      var toggle = check.data('bs.toggle');

      modalFecha = bootbox.confirm({
          title: "Cambiar Estado",
          message: "¿Desea cambiar de estado la póliza?",
          buttons: {
              cancel: {
                  label: '<i class="fa fa-times"></i> Cancelar',
                  className: 'btn-sm btn-default'
              },
              confirm: {
                  label: '<i class="fa fa-save"></i> Confirmar',
                  className: 'btn-sm btn-primary'
              }
          },
          callback: function (result) {
            var estado = 2;
            if (!check.is(':checked')) {
              estado = 1;
            }
            if (!result) {
              if (estado === 2) {
                toggle.off(true);
              }else {
                toggle.on(true);
              }
            }else {
                $.post(url + "Polizas/Apps/cambiarEstado",{folio: folio,estado: estado},function(data){
                  if (!data.error) {
                    $.toast({
                        heading: 'Alerta',
                        hideAfter: 4000,
                        position: 'bottom-right',
                        text: data.msg,
                        icon: 'success'
                      });
                  }else {
                    if (estado === 2) {
                      toggle.off(true);
                    }else {
                      toggle.on(true);
                    }
                    $.toast({
                      heading: 'Error',
                      hideAfter: 4000,
                      position: 'bottom-right',
                      text: data.msg,
                      icon: 'error'
                    });
                  }
                },'json').fail(function (jqXHR, textStatus, errorThrown) {
                  if (estado === 2) {
                    toggle.off(true);
                  }else {
                    toggle.on(true);
                  }
                  $.toast({
                    heading: 'Error',
                    hideAfter: 4000,
                    position: 'bottom-right',
                    text: errorThrown,
                    icon: 'error'
                  });
                });
            }
          }
      });
  })
});
