$(function(){
  $('.modal').on('shown.bs.modal', function () {

    $('.fecha1').datetimepicker({
      format:'DD-MM-YYYY'
    });
    $('.fechaIni').datetimepicker({
      format:'DD-MM-YYYY HH:mm'
    });
    $('.fechaFin').datetimepicker({
      format:'DD-MM-YYYY HH:mm'
    });
  });

  $('.modal').on('hidden.bs.modal', function () {
    $('#frmPoliza')[0].reset();
  });

    tablepolizas = $('#tb-polizas').DataTable( {
          ajax: $("#base_url").val()+'Polizas/Apps/obtenerPolizasRed',
          columns: [
              { title: "Id" , className: "folio"},
              { title: "Fecha" , className: "fecha"},
              { title: "Codigo" , className: "codigo"},
              { title: "Fecha Inicial" , className: "fechaini"},
              { title: "Fecha Final", className: "fechafin" },
              { title: "Usuario", className: "dt-center" },
              { title: "Opción",className: "dt-center"},
          ],
          order : [ 0, "desc" ],
          responsive: true,
          fnDrawCallback: function() {
            $('.toggle-estado').bootstrapToggle({
                  on: 'Cuadrada',
                  off: 'Abierta',

            });
          },
          bLengthChange: false,
          language: {
              "sProcessing":     "Procesando...",
              "sLengthMenu":     "Mostrar _MENU_ registros",
              "sZeroRecords":    "No se encontraron resultados",
              "sEmptyTable":     "Ningún dato disponible en esta tabla",
              "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix":    "",
              "sSearch":         "Buscar:",
              "sUrl":            "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "Último",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
          },

          dom: '<"html5buttons"B>lTfgitp',
          buttons: [
              { extend: 'copy'},
              {extend: 'excel', title: 'ExampleFile'},
              {extend: 'pdf', title: 'ExampleFile'},

              {extend: 'print',
               customize: function (win){
                      $(win.document.body).addClass('white-bg');
                      $(win.document.body).css('font-size', '10px');

                      $(win.document.body).find('table')
                              .addClass('compact')
                              .css('font-size', 'inherit');
              }
              }
          ]
      } );
      $(document).on('click',".btnImprimir",function(e){
        var row = $(this).closest("tr");
        var fecha = row.find(".fecha").text();
        var codigo = row.find(".codigo").text();
        var fechaini = row.find(".fechaini").text();
        var fechafin = row.find(".fechafin").text();
        var url = $("#base_url").val();
        window.open(url + 'Polizas/Apps/imprimirPolizaRed?fecha='+fecha+'&codigo='+codigo+'&fechainicial='+fechaini+'&fechafinal='+fechafin,'_blank');
       });

       $(document).on('click',".btnDescargar",function(e){
         var row = $(this).closest("tr");
         var fecha = row.find(".fecha").text();
         var codigo = row.find(".codigo").text();
         var fechaini = row.find(".fechaini").text();
         var fechafin = row.find(".fechafin").text();
         console.log('Error');
         // var folio = row.find(".folio").text();
         // var folio = row.find(".folio").text();
         // alert(folio);
         var url = $("#base_url").val();
         window.open(url + 'Polizas/Apps/descargarPolizaTxtRed?fecha='+fecha+'&codigo='+codigo+'&fechainicial='+fechaini+'&fechafinal='+fechafin,'_blank');
         // var fecha = row.find(".fecha").text();
         // var codigo = row.find(".codigo").text();
         // location.href = $('#base_url').val() + 'Polizas/Apps/modificarpoliza?folio='+folio+'&fecha='+fecha+'&codigo='+codigo;
        });
});

  $('#tb-polizas tbody').on( 'click', 'tr .btnEliminar', function (){
    var row = $(this).closest("tr");
    var url = $("#base_url").val() + 'Polizas/Apps/eliminarPolizaRed';
    var datos = {
          folio: row.find(".folio").text()
        };
    var fecha = row.find(".fecha").text();
    swal({
        text: "¿Desea eliminar la póliza del día "+fecha+" ?",
        content: "warning",
        closeOnClickOutside: false,
        buttons: {
          cancel: "Cancelar",
          catch: {
            text: "Confirmar",
          }
        }
    }).then(name => {
      if (name !== null) {
         $.post(url,datos,function(data){
          tablepolizas.ajax.reload();
          if (data.error) {
            swal("Error!", " " + data.msg, "error");
          }else {
            swal("Exito!", " " + data.msg, "success");
          }
        },'json').fail(function(e,x,error){
          swal("Error!", " " + error, "error");
        });
     }
    });
  })


  $("#btn-new").on('click',function(e){
    $("#modalnew").modal('toggle');
  });

  $("#frmPoliza").on('submit',function(e){
    e.preventDefault();
    var form = $(this);
    var url = form.attr('action');
    var datos = form.serialize();

    $.post(url,datos,function(data){
      if (data.error) {
        $.toast({
          heading: 'Error', hideAfter: 4000,
          position: 'bottom-right', text: data.msg,
          icon: 'error'  });
      }else {
          tablepolizas.ajax.reload();
          $("#modalnew").modal('toggle');
          $.toast({
            heading: 'Alerta', hideAfter: 4000,
            position: 'bottom-right', text: "Poliza registrada correctamente.",
            icon: 'success'  });
      }
    },'json').fail(function(e,x,error){
      $.toast({
        heading: 'Error', hideAfter: 4000,
        position: 'bottom-right', text: error,
        icon: 'error'  });
    });
  });
