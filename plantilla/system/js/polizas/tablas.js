$(".deletePago").on('click',function(e){
  var row = $(this).parents('tr');
  var url = $("#base_url").val() + 'Polizas/Apps/eliminarPago';
  var datos = {folio: row.find(".folio").text(),
               cuenta: row.find(".cuenta").text(),
               poliza: $("#poliza").val()
              };
  var confirm = bootbox.confirm({
    message: "¿Desea eliminar el pago "+datos.folio+".?",
    buttons: {
        confirm: {
            label: '<span class="glyphicon glyphicon-floppy-save"></span> Confirmar',
            className: 'btn-primary btn-sm'
        },
        cancel: {
            label: '<span class="glyphicon glyphicon-remove"></span> Cancelar',
            className: 'btn-default btn-sm'
        }
    },
    callback: function (result) {
        if (result) {
          $.post(url,datos,function(data){
            if (data.error) {
              $.toast({
                heading: 'Error',
                hideAfter: 4000,
                position: 'bottom-right',
                text: data.msg,
                icon: 'error'
              });
            }else {
              row.remove();
              $.toast({
                heading: 'Alerta',
                hideAfter: 4000,
                position: 'bottom-right',
                text: data.msg,
                icon: 'success'
              });
              tblPolizas.ajax.reload();
            }
          },'json').fail(function(e,x,error){
              console.log(error);
          });
        }
    }
  });
});

$(".deleteCargo").on('click',function(e){
  var row = $(this).parents('tr');
  var url = $("#base_url").val() + 'Polizas/Apps/eliminarCargo';
  var datos = {folio: row.find(".folio").text(),poliza: $("#poliza").val()};
  var confirm = bootbox.confirm({
    message: "¿Desea eliminar el cargo "+datos.folio+".?",
    buttons: {
        confirm: {
            label: '<span class="glyphicon glyphicon-floppy-save"></span> Confirmar',
            className: 'btn-primary btn-sm'
        },
        cancel: {
            label: '<span class="glyphicon glyphicon-remove"></span> Cancelar',
            className: 'btn-default btn-sm'
        }
    },
    callback: function (result) {
        if (result) {
          $.post(url,datos,function(data){
            if (data.error) {
              $.toast({
                heading: 'Error',
                hideAfter: 4000,
                position: 'bottom-right',
                text: data.msg,
                icon: 'error'
              });
            }else {
              row.remove();
              $.toast({
                heading: 'Alerta',
                hideAfter: 4000,
                position: 'bottom-right',
                text: data.msg,
                icon: 'success'
              });
              tblPolizas.ajax.reload();
            }
          },'json').fail(function(e,x,error){
              console.log(error);
          });
        }
    }
  });
});
