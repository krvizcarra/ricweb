$(document).ready(function () {
    $('.fileinput').fileinput();
    $('#tblCategorias').DataTable({
        pageLength: 10,
        ajax: $("#base_url").val()+'Sistema/obtenerCategorias',
        responsive: true,
        bLengthChange: false,
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        columnDefs: [ { "targets": 5, "sClass": "text-center" },
                      { "targets": 0, "sClass": "id" },
                      { "targets": 1, "sClass": "nombre" },
                      { "targets": 2, "sClass": "descripcion" },
                      { "targets": 3, "sClass": "url hidden" },
                      { "targets": 4, "sClass": "imagen hidden" },
  
       ],
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            { extend: 'copy'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},
  
            {extend: 'print',
             customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
  
                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
            }
            }
        ]
    });//DataTable
  
    var fila;
    $("#tblCategorias tbody").on('click','tr .editar',function(e) {
        $('#lb-titulo').text('Modificar Categoría');
        fila = $(this).closest('tr');
        var id = fila.find(".id").text();
        var nombre = fila.find(".nombre").text();
        var descripcion = fila.find(".descripcion").text();        
        var imagen = fila.find(".imagen").text();
  
        $('#id').val(id);
        $('#nombre').val(nombre);
        $('#descripcion').val(descripcion);        
        $('#filename').val(imagen);
        $('#file').attr('required', false);
        $('#preview').text('');
        $('#preview').append('<img src="'+imagen+'" />');
        //$('#.fileinput-new').text('');
    });

    $("#tblCategorias tbody").on('click','tr .eliminar',function(e){
        var row = $(this);
        var opc = row.attr('opcion');
        var id = row.attr('folio');
        var datos = {id: id,opc: opc};
      swal({
          title: "Desbloquear Pre-Póliza",
          text: "Al desbloquear esta hoja el encargado podrá cambiar los archivos anexados en las formas de pago.",
          buttons: {
            cancel: "Cancelar",
            catch: {
              text: "Confirmar",
            }
          },
          closeOnConfirm: false
      }).then(name => {
          if (name == "catch") {
            var url = $("#base_url").val() + 'HojaSAT/Apps/editarHoja'
            $.post(url,datos,function(data){
              tblhojasat.ajax.reload();
              if (data.error) {
                swal("Error!", data.msg, "error");
              }else {
                swal("Exito!", data.msg, "success");
              }
            },'json').fail(function(e,x,error){
              swal("Error!", error, "error");
            });
          }
      });
    });

    
    $('#file').change(function() {
      var fileReader = new FileReader(),
              files = this.files,
              file;
  
      if (!files.length) {
          return;
      }
  
      file = files[0];
  
      if (/^image\/\w+$/.test(file.type)) {
          fileReader.readAsDataURL(file);
          fileReader.onload = function () {
              $('#filename').val(this.result);
              $('#preview').text('');
              $('#preview').append('<img src="'+this.result+'" />');
  
          };
      } else {
        $.toast({
          heading: 'Error',
          hideAfter: 4000,
          position: 'bottom-right',
          text: 'Formato de imagen incorrecto',
          icon: 'error'
        });
      }
      $('.fileinput-new').css({'display':'block'});
  
    });
  
    $("#form-categorias").submit(function(e){
      e.preventDefault();
      var base_url = $('#base_url').val();
      $.ajax({
          url: base_url+'Home/Apps/guardarCategorias',
          type: 'POST',
          data: $(this).serialize(),
          cache:false,
          dataType: 'json',
          success: function(data, textStatus, jqXHR) {
              if(!data.error) {
                  $('#tblCategorias').DataTable().ajax.reload();
                  $('#Modal').modal('hide');
                  $('form#form-categorias :input').each(function (i,item) {
                    $(this).val('');
                  })
                  $.toast({
                    heading: 'Exíto',
                    hideAfter: 5000,
                    position: 'bottom-right',
                    text: 'Información guardada correctamente',
                    icon: 'success'
                  });
  
              }else {
                  $.toast({
                    heading: 'Error',
                    hideAfter: 5000,
                    position: 'bottom-right',
                    text: data.msg,
                    icon: 'error'
                  });
              }
              // console.log(data);
          },
          error: function(jqXHR, textStatus, errorThrown) {
              // Handle errors here
              console.log('Error: '+jqXHR);
          }
      });//ajax
    });
  
    $('#btnNuevo').click(function(event) {
      event.preventBubble=true;
      $('#lb-titulo').text('Nueva Categoría');
      $('form#form-categorias :input').each(function (i,item) {
        $(this).val('');
      });
      $('#preview').text('');
      $('#id').val(0);
      $('#file').attr('required', true);
    });
  
  
  }); //.ready
  